/*-----------------------------------------------------------------------------
 * CPSWPrivate.h            Header file
 * Copyright                acontis technologies GmbH, Weingarten, Germany
 * Response                 Kai Olbrich
 * Description              Register Description for Texas Instruments CPSW
 *---------------------------------------------------------------------------*/

#ifndef INC_CPSWPRIVATE
#define INC_CPSWPRIVATE

//
// Common defines
//

// If COPY_FRAME is defined (in LinkOsPlatform.h), the Master stack will operate on
// cached buffers only and this LL will do one additionally memcpy() on RX and TX.
// Background:
//   The DMA memory on ARM / Win-CE is mapped uncached, because cache coherence
//   must be done in software which involves writes to the System Control Coprocessor (CP15).
//   That writes are privileged operations!
// If COPY_FRAME is set, the CPU consumption is aprox. 17% better on CE (with perfmess512.xml ENI)

// Use dedicated (fast) SoC's SRAM for the DMA descriptors.
#define USE_CPPI_RAM 

// If defined, the address lookup engine of the switch fabric is effectively disabled.
// _All_ packets (including packets with dst-addr != station-addr) received on P1 or P2,
// are directed to the host port P0.
// This mode is similar to the promiscuous mode.
#undef ALE_BYPASS

// Enable tracing (debug prints)?
#undef TRACE

#define CPSW_RXDESCNT          128                    // Receive descriptor buffer count (!!must be power of 2, max 256!!)
#if defined COPY_FRAME
#  define CPSW_RXDMABUFCNT     CPSW_RXDESCNT          // Receive DMA buffer count. Use one DMA buffer per dtor (1:1 mapping)
#  define CPSW_RXCACHEDBUFCNT  (CPSW_RXDESCNT * 2)    // Receive cached buffer count (!!must be gt CPSW_RXDESCNT, must be power of 2!!)
#else
#  define CPSW_RXDMABUFCNT     (CPSW_RXDESCNT * 2)    // Receive DMA buffer count (!!must be gt CPSW_RXDESCNT, must be power of 2!!)
#endif
#define CPSW_RXBUFLEN          2048                   // RX buffer length (!!should be power of 2!!)

#define CPSW_TXDESCNT          128                    // Transmit descriptor buffer count (!!must be power of 2, max 256!!)
#define CPSW_TXDMABUFCNT     CPSW_TXDESCNT          // Transmit DMA buffer count (!!must be power of 2!!)
#if defined COPY_FRAME
#  define CPSW_TXCACHEDBUFCNT  CPSW_TXDESCNT          // Transmit cached buffer count (!!must be power of 2!!)
#endif
#define CPSW_TXBUFLEN          2048                   // TX buffer length (!!should be power of 2!!)



#define MAC_ADDR_LEN           6

#define CPSW_DMA_ALIGNMENT     16                     // DMA buffer alignment (fixed, don't change!)

#define BUFFER_MAGIC_TX        0xDEADBEEF
#define BUFFER_MAGIC_RX        0xBEEFBABE

#define CPSW_SOCDMA_LENGTH     KB(8)
#define CPSW_PORTDMA_LENGTH    (CPSW_SOCDMA_LENGTH / CPSW_SLAVECNT)
#define CPSW_IO_LENGTH         KB(32) // e.g: 0x4A10_0000 - 0x4A10_7FFF

#define CPDMA_MAX_CHANNELS     8

#define PORT_0_MASK            0x1
#define PORT_1_MASK            0x2
#define PORT_2_MASK            0x4
#define HOST_PORT_MASK         PORT_0_MASK
#define HOST_PORT_NO           0

#define ALE_ENTRY_WORDS        3

#define CPSW_ID_IDENT          0x19              // 0x19 Unique identifier for CPSW.       

#define KB(x) ((x) * 1024)
#define MB(x) ((x) * (1024 * 1024))

/* Mac Control register bit fields */
#define CPMAC_MACCONTROL_RXCMFEN_MASK           (0x1 << 24)
#define CPMAC_MACCONTROL_RXCSFEN_MASK           (0x1 << 23)
#define CPMAC_MACCONTROL_RXCEFEN_MASK           (0x1 << 22)
#define CPMAC_MACCONTROL_EXTEN_MASK             (0x1 << 18)
#define CPMAC_MACCONTROL_GIGFORCE_MASK          (0x1 << 17)
#define CPMAC_MACCONTROL_IFCTLB_MASK            (0x1 << 16)
#define CPMAC_MACCONTROL_IFCTLA_MASK            (0x1 << 15)
#define CPMAC_MACCONTROL_CMDIDLE_MASK           (0x1 << 11)
#define CPMAC_MACCONTROL_TXSHORTGAPEN_MASK      (0x1 << 10)
#define CPMAC_MACCONTROL_GIGABITEN_MASK         (0x1 << 7)
#define CPMAC_MACCONTROL_TXPACEEN_MASK          (0x1 << 6)
#define CPMAC_MACCONTROL_MIIEN_MASK             (0x1 << 5)
#define CPMAC_MACCONTROL_TXFLOWEN_MASK          (0x1 << 4)
#define CPMAC_MACCONTROL_RXFLOWEN_MASK          (0x1 << 3)
#define CPMAC_MACCONTROL_LOOPBKEN_MASK          (0x1 << 1)
#define CPMAC_MACCONTROL_FULLDUPLEXEN_MASK      (0x1)

/* DMA Control register bit fields */
#define CPDMA_DMACONTROL_TXPTYPE_MASK           (0x1)

/* TX Control register bit fields */
#define CPDMA_TXCONTROL_TXEN                    (0x1)

/* RX Control register bit fields */
#define CPDMA_RXCONTROL_RXEN                    (0x1)

/* Port_VLAN register fields */
#define CPSW3G_PORTVLAN_PRI_SHIFT               13
#define CPSW3G_PORTVLAN_PRI_MASK                (0x7 << 13)
#define CPSW3G_PORTVLAN_CFI_SHIFT               12
#define CPSW3G_PORTVLAN_CFI_MASK                (0x1 << 12)
#define CPSW3G_PORTVLAN_VID_SHIFT               0
#define CPSW3G_PORTVLAN_VID_MASK                (0xfff << 0)

/* ALE Control register bit fields */
#define ALE_PORTCTL_PSTATE_DISABLED             0
#define ALE_PORTCTL_PSTATE_BLOCKED              1
#define ALE_PORTCTL_PSTATE_LEARN                2
#define ALE_PORTCTL_PSTATE_FORWARD              3
#define ALE_PORTCTL_TABLEWRITE                  (0x1u << 31)

#define ALE_ENTRY_VLAN                           0x20
#define ALE_ENTRY_VLANUCAST                      0x30
#define ALE_ENTRY_UCAST                          0x10
#define ALE_ENTRY_MCAST                          0xD0
#define ALE_ENTRY_NUM_WORDS                      3
#define ALE_VLAN_ENTRY_MEMBER_LIST               0
#define ALE_VLAN_ENTRY_FRC_UNTAG_EGR             3
#define ALE_VLAN_ENTRY_ID_BIT0_BIT7              6
#define ALE_VLAN_ENTRY_TYPE_ID_BIT8_BIT11        7
#define ALE_VLANUCAST_ENTRY_ID_BIT0_BIT7         6
#define ALE_VLANUCAST_ENTRY_TYPE_ID_BIT8_BIT11   7

#define ALE_VLANUCAST_ENTRY_TYPE_PORTNO          8
#define ALE_VLANUCAST_ENTRY_PORT_SHIFT           2

#define ALE_UCAST_ENTRY_TYPE                     7
#define ALE_UCAST_ENTRY_DLR_PORT_BLK_SEC         8
#define ALE_UCAST_ENTRY_PORT_SHIFT               2
#define ALE_MCAST_ENTRY_TYPE_FWD_STATE           7
#define ALE_MCAST_ENTRY_PORTMASK_SUP             8
#define ALE_MCAST_ENTRY_PORTMASK_SHIFT           2

// Slave / Port
#define CPSW3G_PORTTXFIFO_INSEL_SHIFT           16


/* Macro definition for MDIO */
#define MDIO_GO			0x80000000
#define MDIO_READ       0x00000000
#define MDIO_WRITE		0x40000000
#define MDIO_ENABLE		0x40000000
#define MDIO_ACK        0x20000000



//
// MII interface (PHY access) register map
//

#define MII_CR         0x00      // Control (CR)
#define MII_SR         0x01      // Status (SR)
#define MII_PHYID1     0x02      // PHY ID 1
#define MII_PHYID2     0x03      // PHY ID 2
#define MII_ANA        0x04      // AN advertisement (ANA)
#define MII_ANLPBPA    0x05      // AN Link Partner Base Page Ability Register (ANLPBPA)
#define MII_ANEX       0x06      // AN Expansion Register (ANEX)
#define MII_CTRL1000   0x09      // 1000BASE-T control register
#define MII_STAT1000   0x0A      // 1000BASE-T status register
#define MII_TBICON     0x11      // TBI control (TBICON). This register is eTSEC specific!
#define MII_ESTATUS    0x0F      // Extended status (1000-BASE-X)

// MII_CR (0x00)
#define MII_CR_PHYRST         0x8000    // PHY reset.
#define MII_CR_SPEED0         0x2000    // 100Mbps (Always 0)
#define MII_CR_ANENABLE       0x1000    // Auto-negotiation enable.
#define MII_CR_ANRESTART      0x0200    // Restart auto-negotiation.
#define MII_CR_FULLDPX        0x0100    // Duplex mode.
#define MII_CR_SPEED1         0x0040    // 1Gps (Always 1)
#define MII_CR_DEFAULTMSK     (MII_CR_SPEED0 | MII_CR_ANENABLE | MII_CR_FULLDPX | MII_CR_SPEED1)

// MII_SR (0x01)
#define MII_SR_ERCAP          0x0001    // Ext-reg capability
#define MII_SR_LSTATUS        0x0004    // Link status
#define MII_SR_ANEGCAPABLE    0x0008    // Able to do auto-negotiation
#define MII_SR_RFAULT         0x0010    // Remote fault detected
#define MII_SR_ANDONE         0x0020    // Auto-negotiation complete
#define MII_SR_ESTATEN        0x0100    // Extended Status in R15
#define MII_SR_10HALF         0x0800    // Can do 10mbps, half-duplex
#define MII_SR_10FULL         0x1000    // Can do 10mbps, full-duplex
#define MII_SR_100HALF        0x2000    // Can do 100mbps, half-duplex
#define MII_SR_100FULL        0x4000    // Can do 100mbps, full-duplex
#define MII_SR_LINKOKVAL      (MII_SR_ANDONE | MII_SR_LSTATUS)

// MII_ANA (0x04)
#define MII_ANA_FULLDPX       0x0020    // Full-duplex capability. 
#define MII_ANA_1000XPAUSE    0x0080    // 1000BASE-X pause
#define MII_ANA_ASYMPAUSE     0x0100    // 1000BASE-X asym pause
#define MII_ANA_MSK           (MII_ANA_ASYMPAUSE | MII_ANA_1000XPAUSE | MII_ANA_FULLDPX)

// MII_ANLPBPA (0x05)
#define MII_ANLPBPA_100FDX    0x0100    // 100BASE-TX FDX capable
#define MII_ANLPBPA_100HDX    0x0080    // 100BASE-TX HDX capable
#define MII_ANLPBPA_10FDX     0x0040    // 10BASE-TX HDX capable

// MII_ANEX (0x06)
#define MII_ANEX_NPABLE       0x0004    // Next page able.
#define MII_ANEX_PGRCVD       0x0002    // Page received.
#define MII_ANEX_PGMSK        (MII_ANEX_NPABLE | MII_ANEX_PGRCVD)

// MII_STAT1000 (0x0A)
#define MII_STAT1000_1000HDX  0x0400    // 1000BASE-T HDX capable
#define MII_STAT1000_1000FDX  0x0800    // 1000BASE-T FDX capable

// MII_ESTATUS (0x0F)
#define MII_ESTATUS_1000HDX   0x1000    // 1000BASE-T HDX capable
#define MII_ESTATUS_1000FDX   0x2000    // 1000BASE-T FDX capable
  
#define CPSW3G_SOFT_RESET_BIT              (1 << 0)


//
// ALE
//

#define CPSW3G_ALECONTROL_ENABLEALE        (1u << 31)
#define CPSW3G_ALECONTROL_CLEARTABLE       (1u << 30)
#define CPSW3G_ALECONTROL_VLANAWARE        (1 << 2)
#define CPSW3G_ALECONTROL_ALEBYPASS        (1 << 4)
#define CPSW3G_ALECONTROL_LEARNNOVID       (1 << 7)


//
// CPSW DMA descriptor
//

#define CPDMA_DESC_SOP        (1u << 31)
#define CPDMA_DESC_EOP        (1 << 30)
#define CPDMA_DESC_OWNER      (1 << 29)
#define CPDMA_DESC_EOQ        (1 << 28)
#define CPDMA_DESC_TDOWNCMPLT (1 << 27)
#define CPDMA_DESC_PORTEN     (1 << 20)
#define CPDMA_DESC_PORTSHIFT  16

#define CPDMA_BUFLEN_MASK     0x7FF // 2047
#define CPDMA_RXBUFLEN        ((CPSW_RXBUFLEN > CPDMA_BUFLEN_MASK) ? CPDMA_BUFLEN_MASK : CPSW_RXBUFLEN)

//
// SoC clock register
//

#define SOC_PRCM_REGS                        (0x44E00000)

// Clock Control
#define CM_PER_CPGMAC0_CLKCTRL   (0x14)
#define CM_PER_CPGMAC0_CLKCTRL_MODULEMODE_ENABLE   (0x2u)
#define CM_PER_CPGMAC0_CLKCTRL_IDLEST   (0x00030000u)
#define CM_PER_CPSW_CLKSTCTRL   (0x144)
#define CM_PER_CPSW_CLKSTCTRL_CLKTRCTRL_SW_WKUP   (0x2u)
#define CM_PER_CPSW_CLKSTCTRL_CLKACTIVITY_CPSW_125MHZ_GCLK   (0x00000010u)

// Control Module
#define CTLM_MACID0_LO  0x630
#define CTLM_MACID0_HI  0x634
#define CTLM_MACID1_LO  0x638
#define CTLM_MACID1_HI  0x63C

// Control Module for AM57xx
#define CTRL_CORE_MAC_ID_SW_0    0x514
#define CTRL_CORE_MAC_ID_SW_1    0x518
#define CTRL_CORE_MAC_ID_SW_2    0x51c
#define CTRL_CORE_MAC_ID_SW_3    0x520

/* Atheros 8031 ethernet phy DEVICE */
#define ATH8031_PHY_ID 0x004dd074

#define AT803X_DEBUG_ADDR     0x1D
#define AT803X_DEBUG_DATA     0x1E

//
// CPSW register map
//

// AM33xx: CPSW_SS
typedef struct _CPSW_REGS
{
   volatile EC_T_DWORD    CPSW_IdVer;			// 0x00 CPSW_3G ID Version Register
   volatile EC_T_DWORD    CPSW_Control;		// 0x04 CPSW_3G Switch Control Register
   volatile EC_T_DWORD    CPSW_Soft_Reset;	// 0x08 CPSW_3G Soft Reset Register
   volatile EC_T_DWORD    CPSW_Stat_Port_En;	// 0x0c CPSW_3G Statistics Port Enable Register
   volatile EC_T_DWORD    CPSW_Pritype;		// 0x10 CPSW_3G Transmit Priority Type Register

   volatile EC_T_DWORD    CPSW_Soft_Idle;		// 0x14 CPSW_3G Software Idle
   volatile EC_T_DWORD    CPSW_Thru_Rate;		// 0x18 CPSW_3G Throughput Rate
   volatile EC_T_DWORD    CPSW_Gap_Thresh;	// 0x1C CPSW_3G CPGMAC_SL Short Gap Threshold
   volatile EC_T_DWORD    CPSW_Tx_Start_WDS;	// 0x20 CPSW_3G Transmit Start Words
   volatile EC_T_DWORD    CPSW_Flow_Control;	// 0x24 CPSW_3G Flow Control
} CPSW_REGS, *PCPSW_REGS;

// AM387x: CPSW_3GSS, AM33xx: CPSW_WR 
typedef struct _CPSW_SS_REGS
{
  volatile EC_T_DWORD   IDVER;				// 0x00  Subsystem ID Version
  volatile EC_T_DWORD   Soft_Reset;			// 0x04  Subsystem Soft Rest
  volatile EC_T_DWORD   Control;			   // 0x08  Subsystem Control
  volatile EC_T_DWORD   Int_Control;		// 0x0C  Subsystem Interrupt Control
  volatile EC_T_DWORD   C0_Rx_Thresh_En;	// 0x10  Subsystem Core 0 Receive Threshold Int Enable
  volatile EC_T_DWORD   C0_Rx_En;	 		// 0x14  Subsystem Core 0 Receive Interrupt Enable
  volatile EC_T_DWORD   C0_Tx_En;			// 0x18  Subsystem Core 0 Transmit Interrupt Enable
  volatile EC_T_DWORD   C0_Misc_En; 		// 0x1C  Subsystem Core 0 Misc Interrupt Enable
} CPSW_SS_REGS, *PCPSW_SS_REGS;

typedef struct _CPSW_SLIVER_REGS
{
  volatile EC_T_DWORD   SL_IDVER;				// 0x00 CPGMAC_SL ID/Version
  volatile EC_T_DWORD   SL_MacControl;			// 0x04 CPGMAC_SL Mac Control
  volatile EC_T_DWORD   SL_MacStatus;			// 0x08 CPGMAC_SL Mac Status
  volatile EC_T_DWORD   SL_Soft_Reset;			// 0x0c CPGMAC_SL Soft Reset
  volatile EC_T_DWORD   SL_Rx_Maxlen;			// 0x10 CPGMAC_SL Receive Maximum Length 
  volatile EC_T_DWORD   SL_BoffTest;			// 0x14 CPGMAC_SL Backoff Test Register
  volatile EC_T_DWORD   SL_Rx_Pause;			// 0x18 CPGMAC_SL Receive Pause Timer Register
  volatile EC_T_DWORD   SL_Tx_Pause;	 		// 0x1c CPGMAC_SL Transmit Pause Timer Register
  volatile EC_T_DWORD   SL_EMControl;			// 0x20 CPGMAC_SL Emulation Control
  volatile EC_T_DWORD   SL_Rx_Pri_Map; 		// 0x24 CPGMAC_SL Rx Pkt Pri to Header Pri Mapping
} CPSW_SLIVER_REGS, *PCPSW_SLIVER_REGS;

typedef struct _CPSW_SLAVE_REGS
{
  volatile EC_T_DWORD   Max_Blks;			// CPSW_3G Port Maximum FIFO blocks Register
  volatile EC_T_DWORD   BLK_CNT;			   // CPSW_3G Port FIFO Block Usage Count (read only)
  volatile EC_T_DWORD   Tx_In_Ctl;			// CPSW_3G Port Transmit FIFO Control
  volatile EC_T_DWORD   Port_VLAN;			// CPSW_3G Port VLAN Register
  volatile EC_T_DWORD   Tx_Pri_Map;			// CPSW_3G Port Tx Header Pri to Switch Pri Mapping Register
  volatile EC_T_DWORD   TS_CTL;				// CPSW_3G Port Time Sync Control Register
  volatile EC_T_DWORD   TS_SEQ_LTYPE;		// CPSW_3G Port Time Sync LTYPE (and SEQ_ID_OFFSET)
  volatile EC_T_DWORD   TS_VLAN;			   // CPSW_3G Port Time Sync VLAN2 and VLAN2 Register
  volatile EC_T_DWORD   SL_SA_LO;	 		// CPSW_3G CPGMAC_SL Source Address Low Register
  volatile EC_T_DWORD   SL_SA_HI;  			// CPSW_3G CPGMAC_SL Source Address High Register
  volatile EC_T_DWORD   Send_Percent;		// CPSW_3G Port Transmit Queue Send Percentages
} CPSW_SLAVE_REGS, *PCPSW_SLAVE_REGS;

typedef struct _CPSW_HOST_REGS
{
   volatile EC_T_DWORD    P0_Max_blks;	   // CPSW_3G Port 0 Maximum FIFO blocks Register
   volatile EC_T_DWORD    P0_BLK_CNT;			// CPSW_3G Port 0 FIFO Block Usage Count (read only)
   volatile EC_T_DWORD    P0_Tx_In_Ctl;		// CPSW_3G Port 0 Transmit FIFO Control
   volatile EC_T_DWORD    P0_Port_VLAN;		// CPSW_3G Port 0 VLAN Register
   volatile EC_T_DWORD    P0_Tx_Pri_Map;		// CPSW_3G Port 0 Tx Header Pri to Switch Pri Mapping Register
   volatile EC_T_DWORD    CPDMA_Tx_Pri_Map;	// CPSW_3G CPDMA TX (Port 0 Rx) Pkt Pri to Header Pri Mapping
   volatile EC_T_DWORD    CPDMA_Rx_Ch_Map;	// CPSW_3G CPDMA RX (Port 0 Tx) switch Pri to DMA channel Pri Mapping
} CPSW_HOST_REGS, *PCPSW_HOST_REGS;

typedef struct _CPSW_CPDMA_REGS
{
   volatile EC_T_DWORD    Tx_Idver;			   // 0x00 CPDMA_REGS TX Identification and Version
   volatile EC_T_DWORD    Tx_Control;			// 0x04 CPDMA_REGS Transmit Control Register
   volatile EC_T_DWORD    Tx_Teardown;		   // 0x08 CPDMA_REGS TX Teardown Register
   volatile EC_T_DWORD    Rsvd2;				   // 0x0c
   volatile EC_T_DWORD    Rx_Idver;			   // 0x10 CPDMA_REGS RX Identification and Version
   volatile EC_T_DWORD    Rx_Control;			// 0x14 CPDMA_REGS RX Control Register
   volatile EC_T_DWORD    Rx_Teardown;		   // 0x18 CPDMA_REGS RX Teardown Register
   volatile EC_T_DWORD    CPDMA_Soft_Reset;	// 0x1c CPDMA_REGS CPDMA Soft Reset Register
   volatile EC_T_DWORD    DMAControl;			// 0x20 CPDMA_REGS CPDMA CPDMA Control Register
   volatile EC_T_DWORD    DMAStatus;			// 0x24 CPDMA_REGS CPDMA CPDMA Status Register 
   volatile EC_T_DWORD    RX_Buffer_Offset;	// 0x28 CPDMA_REGS CPDMA Receive Buffer Offset  Register
   volatile EC_T_DWORD    EMControl;			   // 0x2c CPDMA_REGS CPDMA Emulation Control Register
   volatile EC_T_DWORD    TX_PriN_Rate[8];	   // 0x30-4C CPDMA_REGS Transmit (ingress) Priority N=0,...,7 Rate
   volatile EC_T_DWORD    Rsvd3[12];			   // 0x50-0x7C  

   volatile EC_T_DWORD    Tx_IntStat_Raw;		// 0x80 CPDMA_INT Transmit Interrupt Status (Unmasked) Register
   volatile EC_T_DWORD    Tx_IntStat_Masked;   // 0x84 CPDMA_INT Transmit Interrupt Status (Masked) Register
   volatile EC_T_DWORD    Tx_IntMask_Set;		// 0x88 CPDMA_INT Transmit Interrupt Mask Set Register
   volatile EC_T_DWORD    Tx_IntMask_Clear;	   // 0x8c CPDMA_INT Transmit Interrupt Mask Clear Register
   volatile EC_T_DWORD    CPDMA_In_Vector;		// 0x90 CPDMA_INT Input Vector Register (Read Only)
   volatile EC_T_DWORD    CPDMA_EOI_Vector; 	// 0x94 CPDMA_INT End of Interrupt Vector Register 
   volatile EC_T_DWORD    Rsvd4[2];			   // 0x98-0x9C  

   volatile EC_T_DWORD    Rx_IntStat_Raw;		// 0xa0 CPDMA_INT RX Interrupt Status (Unmasked) Register
   volatile EC_T_DWORD    Rx_IntStat_Masked;	// 0xa4 CPDMA_INT RX Interrupt Status (Masked) Register
   volatile EC_T_DWORD    Rx_IntMask_Set;		// 0xa8 CPDMA_INT RX Interrupt Mask Set Register
   volatile EC_T_DWORD    Rx_IntMask_Clear;	   // 0xac CPDMA_INT RX Interrupt Mask Clear Register
   volatile EC_T_DWORD    DMA_IntStat_Raw;		// 0xb0 CPDMA_INT DMA Interrupt Status (Unmasked) Register
   volatile EC_T_DWORD    DMA_IntStat_Masked;	// 0xb4 CPDMA_INT DMA Interrupt Status (Masked) Register
   volatile EC_T_DWORD    DMA_IntMask_Set;		// 0xb8 CPDMA_INT DMA Interrupt Mask Set Register
   volatile EC_T_DWORD    DMA_IntMask_Clear;	// 0xbc CPDMA_INT DMA Interrupt Mask Clear Register
   volatile EC_T_DWORD    RX_PendThresh[8];	   // 0xc0-0xdc CPDMA_INT Rx Threshold Pending Register Channel 0-7
   volatile EC_T_DWORD    RX_FreeBuffer[8];	   // 0xe0-0xfc CPDMA_INT Free Buffer Register Channel 0-7
} CPSW_CPDMA_REGS, *PCPSW_CPDMA_REGS;

typedef struct _CPSW_STATERAM_REGS
{
   volatile EC_T_DWORD    Tx_HDP[8];			// 0x00-0x1f CPDMA_STATERAM TX Channel 0-7 Head Desc Pointer
   volatile EC_T_DWORD    Rx_HDP[8];			// 0x20-0x2f CPDMA_STATERAM RX Channel 0-7 Head Desc Pointer
   volatile EC_T_DWORD    Tx_CP[8];			   // 0x40-0x5c CPDMA_STATERAM TX Channel 0-7 Completion Pointer
   volatile EC_T_DWORD    Rx_CP[8];			   // 0x60-0x7c CPDMA_STATERAM RX Channel 0-7 Completion Pointer
} CPSW_STATERAM_REGS, *PCPSW_STATERAM_REGS;

typedef struct _CPSW_STATS_REGS
{
   volatile EC_T_DWORD    RxGoodFrames;		   // 0x00 CPSW_STATS  Total number of good frames received
   volatile EC_T_DWORD    RxBcastFrames;		   // 0x04 CPSW_STATS  Total number of good broadcast frames received
   volatile EC_T_DWORD    RxMcastFrames;		   // 0x08 CPSW_STATS  Total number of good multicast frames received
   volatile EC_T_DWORD    RxPauseFrames;		   // 0x0c CPSW_STATS  Pause Receive Frames Register
   volatile EC_T_DWORD    RxCRCErrors;			   // 0x10 CPSW_STATS  Total number of CRC errors frames received
   volatile EC_T_DWORD    RxAlignCodeErrors;	   // 0x14 CPSW_STATS  Total number of alignment/code frames received
   volatile EC_T_DWORD    RxOversizedFrames; 	// 0x18 CPSW_STATS  Total number of oversized frames received
   volatile EC_T_DWORD    RxJabberFrames;		   // 0x1c CPSW_STATS  Total number of jabber frames received
   volatile EC_T_DWORD    RxUndersizedFrames;	// 0x20 CPSW_STATS  Total number of undersized frames received
   volatile EC_T_DWORD    RxFragments;			   // 0x24 CPSW_STATS  Receive Frame Fragments Register
   volatile EC_T_DWORD    Rsvd6[2];			      // 0x28-0x2f
   volatile EC_T_DWORD    RxOctets;			      // 0x30 CPSW_STATS  Total number of RX bytes in good frames Register
   volatile EC_T_DWORD    TxGoodFrames;		   // 0x34 CPSW_STATS  Good Transmit Frames
   volatile EC_T_DWORD    TxBcastFrames;		   // 0x38 CPSW_STATS  Broadcast Transmit Frames Register
   volatile EC_T_DWORD    TxMcastFrames;		   // 0x3c CPSW_STATS  Multicast Transmit Frames Register
   volatile EC_T_DWORD    TxPauseFrames;		   // 0x40 CPSW_STATS  Pause Transmit Frames Register
   volatile EC_T_DWORD    TxDeferredFrames;	   // 0x44 CPSW_STATS  Deferred Transmit Frames Register
   volatile EC_T_DWORD    TxCollisionFrames;	   // 0x48 CPSW_STATS  Tx Collision Frames Register
   volatile EC_T_DWORD    TxSinglecollFrames;	// 0x4c CPSW_STATS  Tx Single Collision Frames Register
   volatile EC_T_DWORD    TxMulticollFrames;	   // 0x50 CPSW_STATS  Tx Multiple Collision Frames Register
   volatile EC_T_DWORD    TxExcessiveCollision; // 0x54 CPSW_STATS  Tx Excessive Collision Frames Register
   volatile EC_T_DWORD    TxLateCollision;		// 0x58 CPSW_STATS  Tx Late Collision Frames Register
   volatile EC_T_DWORD    TxUnderrun;			   // 0x5c CPSW_STATS  Tx Underrun Error Register
   volatile EC_T_DWORD    TxCarrierSenseError;  // 0x60 CPSW_STATS  Tx Carrier Sense Errors Register
   volatile EC_T_DWORD    TxOcets;				   // 0x64 CPSW_STATS  Tx Octet Frames Register
   volatile EC_T_DWORD    Frame64;				   // 0x68 CPSW_STATS  Tx and Rx 64 Octet Frames Register
   volatile EC_T_DWORD    Frame65t127;			   // 0x6c CPSW_STATS  Tx and Rx 65 to 127 Octet Frames Register
   volatile EC_T_DWORD    Frame128t255;		   // 0x70 CPSW_STATS  Tx and Rx 128 to 255 Octet Frames Register
   volatile EC_T_DWORD    Frame256t511;		   // 0x74 CPSW_STATS  Tx and Rx 256 to 511 Octet Frames Register
   volatile EC_T_DWORD    Frame512t1023;		   // 0x78 CPSW_STATS  Tx and Rx 512 to 1023 Octet Frames Register
   volatile EC_T_DWORD    Frame1024tup;		   // 0x7c CPSW_STATS  Tx and Rx 1024 to 1518 Octet Frames Register
   volatile EC_T_DWORD    NetOctets;			   // 0x80 CPSW_STATS  Network Octet Frames Register
   volatile EC_T_DWORD    RxSofOverruns;		   // 0x84 CPSW_STATS  Rx FIFO or DMA Start of Frame Overruns Register
   volatile EC_T_DWORD    RxMofOverruns;		   // 0x88 CPSW_STATS  Rx FIFO or DMA Middle of Frame Overruns Register
   volatile EC_T_DWORD    RxDmaOverruns;		   // 0x8c CPSW_STATS  Rx DMA Start and Mid of Frame Overruns Reg
} CPSW_STATS_REGS, *PCPSW_STATS_REGS;

typedef struct _CPSW_ALE_REGS
{
   volatile EC_T_DWORD    ALE_IdVer;			// 0x00 ALE  id/Version Register
   volatile EC_T_DWORD    Rsvd11;				// 0x04
   volatile EC_T_DWORD    ALE_Control;		   // 0x08 ALE  Control Register
   volatile EC_T_DWORD    Rsvd12;				// 0x0c
   volatile EC_T_DWORD    ALE_PreScale;		// 0x10 ALE  PreScale Register
   volatile EC_T_DWORD    Rsvd13;				// 0x14
   volatile EC_T_DWORD    ALE_Unknown_VLAN;	// 0X18
   volatile EC_T_DWORD    Rsvd14;				// 0x1c
   volatile EC_T_DWORD    ALE_TblCtl;			// 0x20 ALE  Table Control Register
   volatile EC_T_DWORD    Rsvd15[4];			// 0x24-0x30
   volatile EC_T_DWORD    ALE_Tbl[3];			// 0x34 - 0x3c ALE  Table Word 2 Register -Word0
   volatile EC_T_DWORD    ALE_PortCtl[3];	   // 0x40-0x48 ALE Port N=0,...,2 Control Register
} CPSW_ALE_REGS, *PCPSW_ALE_REGS;

typedef struct _CPSW_MDIO_USR_REGS
{
    volatile EC_T_DWORD access;
    volatile EC_T_DWORD physel;
} CPSW_MDIO_USR_REGS, *PCPSW_MDIO_USR_REGS;

typedef struct _CPSW_MDIO_REGS
{
    volatile EC_T_DWORD    Version;      		   // 0x0 MDIO Version Register
    volatile EC_T_DWORD    Control;       	   // 0x4 MDIO Control Register
    volatile EC_T_DWORD    Alive;      			// 0x8 MDIO PHY Alive Status Register
    volatile EC_T_DWORD    Link;       			// 0xc MDIO PHY Link Status Register
    volatile EC_T_DWORD    Linkintraw;     		// 0x10 MDIO Link Status Change Interrupt (Unmasked) Register
    volatile EC_T_DWORD    Linkintmasked;     	// 0x14 MDIO Link Status Change Interrupt (Masked) Register
    volatile EC_T_DWORD    Rsvd1[2];	       	// 0x18 -0x1c Reserved
    volatile EC_T_DWORD    Userintraw;     		// 0x20 MDIO User Command Complete Interrupt (Unmasked) Register
    volatile EC_T_DWORD    Userintmasked;       // 0x24 MDIO User Command Complete Interrupt (Masked) Register
    volatile EC_T_DWORD    Userintmaskset;    	// 0x28 MDIO User Command Complete Interrupt Mask Set Register
    volatile EC_T_DWORD    Userintmaskclear;  	// 0x2c MDIO User Command Complete Interrupt Mask Clear Register
    volatile EC_T_DWORD    Rsvd2[20];      		// 0x30-0x7c Reserved
    _CPSW_MDIO_USR_REGS    Useraccess[2];
} CPSW_MDIO_REGS, *PCPSW_MDIO_REGS;

/*
 * version 1 declarations 
 */

#define EC_LINK_PARMS_SIGNATURE_CPSW_VERSION1 (EC_T_DWORD)0x00000001
#define EC_LINK_PARMS_SIGNATURE_CPSW_V1 (EC_T_DWORD)(EC_LINK_PARMS_SIGNATURE|EC_LINK_PARMS_SIGNATURE_CPSW_PATTERN|EC_LINK_PARMS_SIGNATURE_CPSW_VERSION1)

typedef struct _EC_T_LINK_PARMS_CPSW_V1
{
   /**
   * \brief Link Layer abstraction.
   * set linkParms.dwSignature = EC_LINK_PARMS_SIGNATURE_CPSW
   *
   * Must be first, see casts in usage.
   */
   EC_T_LINK_PARMS linkParms;

   EC_T_LINK_PLATFORMDATA_CPSW oPlatCfg;
   EC_T_DWORD                  dwPhyAddr;              /* [in]  PHY address (0 .. 31) on MII bus */
   EC_T_DWORD                  dwPortPrio;             /* [in]  0 (lowest), 1 (highest) */
   EC_T_BOOL                   bMaster;                /* [in]  TRUE := Initialize MAC */
   EC_T_BOOL                   bPhyRestartAutoNegotiation;
   EC_T_CPSW_PHYCONNECTION     ePhyConnection;         /* [in]  PHY connection type (GMII, RGMII) */

   /* Interrupt handling */
   EC_T_DWORD                  dwRxInterrupt;          /* [in]  Receive interrupt number (IRQ) */
} EC_T_LINK_PARMS_CPSW_V1;



#endif /* INC_CPSWPRIVATE */
