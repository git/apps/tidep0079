/*-----------------------------------------------------------------------------
 * EcDeviceCPSW.h           header file
 * Copyright                acontis technologies GmbH, Weingarten, Germany
 * Response                 Kai Olbrich
 * Description              Internal used types and structures for CPSW
 *                          Ethercat LinkLayer
 *---------------------------------------------------------------------------*/

#ifndef INC_ECDEVICECPSW_H
#define INC_ECDEVICECPSW_H

/*-INCLUDE-------------------------------------------------------------------*/

#include "LinkOsLayer.h"

#define EXCLUDE_LL_DMA
#include "LinkLayerDma.h"

/*-DEFINES-------------------------------------------------------------------*/

/*-TYPEDEFS/ENUMS------------------------------------------------------------*/

// CPSW hardware DMA descriptor structure
typedef struct _T_CPSW_DESC
{
   EC_T_DWORD dwNext;   // Next Descriptor Pointer
   EC_T_DWORD dwBuffer; // Buffer Pointer
   EC_T_DWORD dwLen;    // Buffer Offset | Buffer Length
   EC_T_DWORD dwMode;   // Flags | Packet Length
} T_CPSW_DESC, *PT_CPSW_DESC;

// Buffer management entry. Size is 16 byte in order to make debugging more comfortable.
typedef struct _T_CPSW_BUFENTRY
{
   EC_T_DWORD         dwMagicKey;    // Magic key for sanity checks.
   T_DMA_BUFSTATE     eBufState;
   EC_T_DWORD         dwDescIdx;     // Assigned T_CPSW_DESC descriptor index (set during send operation)
   EC_T_DWORD         dwFrameLen;    // Debugging: This info is also stored in the DMA descriptor
} T_CPSW_BUFENTRY, *PT_CPSW_BUFENTRY;
#define CPSW_BUFENTRY_SIZE 16 // Constant expression, because VS preprocessor doesn't understand sizeof()

// Helper access structure (used for casting raw memory)
#if (CACHELINE_SIZE > CPSW_BUFENTRY_SIZE)
#  define CPSW_BUFENTRY_FRAME_OFFSET CACHELINE_SIZE
#else
#  define CPSW_BUFENTRY_FRAME_OFFSET CPSW_BUFENTRY_SIZE
#endif

typedef struct _T_CPSW_FRAMEBUFENTRY
{
   T_CPSW_BUFENTRY bufentry;
#if (CPSW_BUFENTRY_FRAME_OFFSET != CPSW_BUFENTRY_SIZE)
   EC_T_BYTE       abyCacheAlignmentBuffer[CPSW_BUFENTRY_FRAME_OFFSET - CPSW_BUFENTRY_SIZE];
#endif
   EC_T_BYTE       pFrame[1];     // Framebuffer
} T_CPSW_FRAMEBUFENTRY, *PT_CPSW_FRAMEBUFENTRY;

typedef struct _T_CPSW_INTERNAL *PT_CPSW_INTERNAL;
typedef struct _CPSW_REGS *PCPSW_REGS;
typedef struct _CPSW_HOST_REGS *PCPSW_HOST_REGS;
typedef struct _CPSW_ALE_REGS *PCPSW_ALE_REGS;
typedef struct _CPSW_CPDMA_REGS *PCPSW_CPDMA_REGS;
typedef struct _CPSW_STATERAM_REGS *PCPSW_STATERAM_REGS;
typedef struct _CPSW_SS_REGS *PCPSW_SS_REGS;
typedef struct _CPSW_MDIO_REGS *PCPSW_MDIO_REGS;
typedef struct _CPSW_SLAVE_REGS *PCPSW_SLAVE_REGS;
typedef struct _CPSW_SLIVER_REGS *PCPSW_SLIVER_REGS;

typedef struct _CPSW_REGSET
{
   volatile PCPSW_REGS          Cpsw;
   volatile PCPSW_HOST_REGS     Host;
   volatile PCPSW_ALE_REGS      Ale;
   volatile PCPSW_CPDMA_REGS    Dma;
   volatile PCPSW_STATERAM_REGS StateRam;
   volatile PCPSW_SS_REGS       Ss;
   volatile PCPSW_MDIO_REGS     Mdio;
   volatile PT_CPSW_DESC        BdRam;
   volatile PCPSW_SLAVE_REGS    Slave[CPSW_SLAVECNT];
   volatile PCPSW_SLIVER_REGS   Sliver[CPSW_SLAVECNT];

   // Ports / Slaves
   volatile PCPSW_SLAVE_REGS    SelectedSlave;
   volatile PCPSW_SLIVER_REGS   SelectedSliver;

} CPSW_REGSET, *PCPSW_REGSET;

typedef struct _T_CPSW_INTERNAL
{
   // Misc
   EC_T_DWORD                  dwSignature;
   EC_T_LINK_PARMS_CPSW        oInitParms;
   EC_T_RECEIVEFRAMECALLBACK   pfReceiveFrameCallback;
   EC_T_VOID*                  pvCallbackContext;
   EC_T_DWORD                  dwRegisterBase; // mmap address to register set
   CPSW_REGSET                 regs; // Register set
   volatile T_CPSW_DESC*       pDmaDtorBase;
   EC_T_DWORD                  dwDmaDtorBasePa;
   T_PCIINFO                   oCardInfo; // PCI info
   EC_T_DWORD                  dwPhyFeatures;
   EC_T_DWORD                  oAdapterObject;
   EC_T_DWORD                  dwIrqEvent; // storage for interrupt events
   EC_T_LINKOS_IRQ_PARM        oIrqParms; // IRQ handling
   EC_T_DWORD                  dwLosalHandle;
   EC_T_DWORD                  dwLinkStatus;
   EC_T_BYTE                   abyStationAddress[MAC_ADDR_LEN]; // Read from EFUSE

   // DMA
   EC_T_DWORD                  dwTxDmaChan; // 0 .. 7
   EC_T_DWORD                  dwTxDmaChanCyc;  // 0 or 6 DMA channel for cyclic frames
   EC_T_DWORD                  dwTxDmaChanAcyc; // 1 or 7 DMA channel for acyclic frames
   EC_T_DWORD                  dwRxDmaChan; // 0 or 7
   EC_T_DWORD                  dwDMASize; // size of dynamically allocated DMA buffer
   EC_T_DWORD                  dwDMAVa; // base address of dynamically allocated DMA buffer (cached virt address)
   EC_T_DWORD                  dwDMAUncachedVa; // base address of dynamically allocated DMA buffer (uncached virt address)
   EC_T_DWORD                  dwDMAPa; // base address of dynamically allocated DMA buffer (phys address)
   EC_T_DWORD                  dwDMAOffset; // offset to aligned address (relative to DMA buffer base address)

   // RX
   volatile T_CPSW_DESC*       pRxDesc; // Descriptor base address (len: dwRxDescSize byte)
   volatile T_CPSW_DESC*       pRxTailDesc; // Point to last dtor (nextp == NULL)
   EC_T_DWORD                  dwRxDescSize; // Size of all RX descriptors (CPSW_RXDESCNT * sizeof(T_CPSW_DESC)) in byte
   volatile EC_T_BYTE*         pRxBuffer; // Buffer base address (len: dwRxBufferSize byte)
   EC_T_DWORD                  dwRxBufferSize; // Size of all RX buffers (CPSW_RXDMABUFCNT * CPSW_RXBUFLEN) in byte
   EC_T_DWORD                  dwRxDescIdx; // Ringbuffer index (RX T_CPSW_BUFENTRY)
   EC_T_DWORD                  dwRxBufIdx; // Ringbuffer index (RX buffers)
   // fields used for COPY_FRAME
   EC_T_BYTE*                  pRxCachedBuffer; // Cached buffer base address (len: dwRxCachedBufferSize byte)
   EC_T_DWORD                  dwRxCachedBufferSize; // Size of all cached RX buffers (CPSW_RXCACHEDBUFCNT * CPSW_RXBUFLEN) in byte
   EC_T_BYTE*                  pRxCachedBufferOriginal; // buffer address which was returned from malloc, but which can be not alligned

   // TX
   volatile T_CPSW_DESC*       pTxDesc; // Pointer to dtor list
   volatile T_CPSW_DESC*       pTxTailDesc; // Last processed dtor
   EC_T_DWORD                  dwTxDescSize; // Size of all TX descriptors (CPSW_TXDESCNT * sizeof(T_CPSW_DESC)) in byte
   volatile EC_T_BYTE*         pTxBuffer; // Buffer base address (len: dwTxBufferSize byte)
   EC_T_DWORD                  dwTxBufferSize; // Size of all TX buffers (CPSW_TXDMABUFCNT * CPSW_TXBUFLEN) in byte
   EC_T_DWORD                  dwTxDescIdx; // Next free TX Ringbuffer index (TX T_CPSW_BUFENTRY)
   EC_T_DWORD                  dwTxBufIdx; // Ringbuffer index (TX buffers)
   EC_T_BOOL                   bGatherFrames;   // When set, frames are not sent immediately, but gathered together to the queue
   volatile T_CPSW_DESC*       pTxHeadDesc;     // First descriptor added queue, when bGatherFrames is set 
   // fields used for COPY_FRAME
   EC_T_BYTE*                  pTxCachedBuffer; // Cached buffer base address (len: dwTxCachedBufferSize byte)
   EC_T_DWORD                  dwTxCachedBufferSize; // Size of all cached TX buffers (CPSW_TXCACHEDBUFCNT * CPSW_TXBUFLEN) in byte
   EC_T_BYTE*                  pTxCachedBufferOriginal; // buffer address which was returned from malloc, but which can be not alligned

   LinkLayerBuffers            rxBuffers;                  /* used when bUseDmaBuffers is false */

   // Instance Mgt
   PT_CPSW_INTERNAL            pPrev;
   PT_CPSW_INTERNAL            pNext;

} T_CPSW_INTERNAL, *PT_CPSW_INTERNAL;

#endif // INC_ECDEVICECPSW_H
