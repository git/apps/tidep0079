/*-----------------------------------------------------------------------------
 * EcDeviceCPSW.cpp
 * Copyright               acontis technologies GmbH, Weingarten, Germany
 * Response                Kai Olbrich
 * Description             CPSW EtherCAT linklayer driver.
 *---------------------------------------------------------------------------*/

/*-INCLUDES------------------------------------------------------------------*/

#include <LinkOsLayer.h>
#include <EcLink.h>
#include <EthernetServices.h>

#include "CPSWPrivate.h"
#include "EcDeviceCPSW.h"

/*-DEFINES-------------------------------------------------------------------*/

#define CPSWID "CPSW"
#define MDIO_ERROR 0xFFFFFFFF

#ifdef __cplusplus
#  define CCALLING extern "C" 
#else
#  define CCALLING
#endif /* ifdef __cplusplus */

#ifdef USE_CPPI_RAM
#  if (CPSW_RXDESCNT * 16 + CPSW_TXDESCNT * 16) > (CPSW_SOCDMA_LENGTH / CPSW_SLAVECNT)
#    error To many DMA descriptors or undef USE_CPPI_RAM
#  endif
#endif

#ifdef LINUX
#  define CPSW_ENABLE_CLOCKS
#endif

#undef TRACE

#define ALIGN_UP(addr, size)   (((addr)+((size)-1)) & (~((size)-1)))
#define ALIGN_DOWN(addr, size) ((addr) & (~((size)-1)))

#define CPSW_PAGE_SIZE         0x1000
#define PAGE_DOWN(addr)        ALIGN_DOWN((addr), CPSW_PAGE_SIZE)
#define PAGE_UP(addr)          ALIGN_UP((addr), CPSW_PAGE_SIZE)
#define CPSW_DMA_ALIGN(addr)   ALIGN_UP((addr), CPSW_DMA_ALIGNMENT)
#define CPSW_CACHE_DOWN(addr)  ALIGN_DOWN((addr), CACHELINE_SIZE)
#define CPSW_CACHE_UP(addr)    ALIGN_UP((addr), CACHELINE_SIZE)

#define DMACHAN_LOWPRIO        0
#define DMACHAN_HIGHPRIO       7
#define PRI2DMACHAN(prio)      (((prio) == 0) ? DMACHAN_LOWPRIO : DMACHAN_HIGHPRIO)
#define DMACHAN_INVERSE(chan)  (((chan) == DMACHAN_HIGHPRIO) ? DMACHAN_LOWPRIO : DMACHAN_HIGHPRIO)

// MSVC will do bytewise stores (4 * strb instead of str) if an 32bit register is 
// written e.g. with (pReg->arry[i] = 0).
// Use this macro to atomic write a dword!
#define W32(reg, val) \
   w32(((volatile EC_T_DWORD*) &(reg)), (val))

// MSVC will do bytewise load (4 * ldrb instead of ldr) if an 32bit register is 
// read e.g. with (val = pReg->arry[i]).
// Use this macro to atomic read a dword!
#define R32(reg) \
   r32((volatile EC_T_DWORD*) &(reg))

#define NEXT_RX_DESC_IDX(idx) (((idx) + 1) & (CPSW_RXDESCNT - 1))
#define PREV_RX_DESC_IDX(idx) (((idx) - 1) & (CPSW_RXDESCNT - 1))
#define RX_DESC_PTR(idx) (&pAdapter->pRxDesc[(idx)])

#define NEXT_TX_DESC_IDX(idx) (((idx) + 1) & (CPSW_TXDESCNT - 1))
#define PREV_TX_DESC_IDX(idx) (((idx) - 1) & (CPSW_TXDESCNT - 1))
#define TX_DESC_PTR(idx) (&pAdapter->pTxDesc[(idx)])

#define NEXT_RX_DMABUF_IDX(idx)  (((idx) + 1) & (CPSW_RXDMABUFCNT - 1))
#define RX_DMABUF_PTR(idx)  ((T_CPSW_FRAMEBUFENTRY *) (pAdapter->pRxBuffer + (idx) * CPSW_RXBUFLEN))
#if defined COPY_FRAME
#define NEXT_RX_CACHEDBUF_IDX(idx)  (((idx) + 1) & (CPSW_RXCACHEDBUFCNT - 1))
#define RX_CACHEDBUF_PTR(idx)  ((T_CPSW_FRAMEBUFENTRY *) (pAdapter->pRxCachedBuffer + (idx) * CPSW_RXBUFLEN))
#endif

#define NEXT_TX_DMABUF_IDX(idx)  (((idx) + 1) & (CPSW_TXDMABUFCNT - 1))
#define TX_DMABUF_PTR(idx)  ((T_CPSW_FRAMEBUFENTRY *) (pAdapter->pTxBuffer + (idx) * CPSW_TXBUFLEN))
#if defined COPY_FRAME
#define NEXT_TX_CACHEDBUF_IDX(idx)  (((idx) + 1) & (CPSW_TXCACHEDBUFCNT - 1))
#define TX_CACHEDBUF_PTR(idx)  ((T_CPSW_FRAMEBUFENTRY *) (pAdapter->pTxCachedBuffer + (idx) * CPSW_TXBUFLEN))
#endif

#ifndef PRINT
#  if 0 // Used for tracing
#    define PRINT(msg, ...) fprintf(stdout, (msg), __VA_ARGS__); fflush(stdout) 
#  elif defined __GNUC__ || (defined __TI_COMPILER_VERSION__ && __TI_COMPILER_VERSION__ > 5002000)
#    define PRINT(msg, ...) LinkOsDbgMsg((msg), ##__VA_ARGS__)
#  else
#    define PRINT(msg, ...) LinkOsDbgMsg((msg), __VA_ARGS__)
#  endif
#endif

#ifdef DEBUG
#  if defined STARTERWARE_NOOS
int G_bCPSWverbose;
#    define DBG if (G_bCPSWverbose) PRINT
#  elif defined __GNUC__
#    define DBG(msg, ...) PRINT(CPSWID " DBG: " msg, ##__VA_ARGS__)
#  else
#    define DBG(msg, ...) PRINT(CPSWID " DBG: " msg, __VA_ARGS__)
#  endif
#else
#  define DBG(msg, ...)
#endif

#ifdef TRACE
#  if defined __GNUC__
#    define TRC(msg, ...) PRINT(CPSWID " TRC: " msg, ##__VA_ARGS__)
#  else
#    define TRC(msg, ...) PRINT(CPSWID " TRC: " msg, __VA_ARGS__)
#  endif
#else
#  define TRC(msg, ...)
#endif

#define _TRC(msg, ...) // Noop. Disabled TRC()
#if defined STARTERWARE_NOOS
#  define INF PRINT
#  define ERR PRINT
#elif defined __GNUC__ || (defined __TI_COMPILER_VERSION__ && __TI_COMPILER_VERSION__ > 5002000)
#  define INF(msg, ...) PRINT(CPSWID " INF: " msg, ##__VA_ARGS__)
#  define ERR(msg, ...) PRINT(CPSWID " ERR: " msg, ##__VA_ARGS__)
#else
#  define INF(msg, ...) PRINT(CPSWID " INF: " msg, __VA_ARGS__)
#  define ERR(msg, ...) PRINT(CPSWID " ERR: " msg, __VA_ARGS__)
#endif

#if defined STARTERWARE_NOOS && defined ADAPTER_DESC_FROM_UC_MEM
CCALLING EC_T_DWORD GetStartAddrDdrUc(void);
#endif

/***************************************************************************************************
*                                 FORWARD DECLARATIONS
*/

static EC_T_DWORD EcLinkClose(void *pvInstance);
static EC_T_DWORD EcLinkAllocSendFrameFromDma(PT_CPSW_INTERNAL pAdapter, EC_T_LINK_FRAMEDESC* pLinkFrameDesc, EC_T_DWORD dwSize);
static EC_T_LINKSTATUS EcLinkGetStatus(void *pvInstance);
static EC_T_DWORD EcLinkIrqOpen(void *pvContext);
static EC_T_DWORD EcLinkIrqClose(void *pvContext);
static EC_T_DWORD EcLinkIrqAck(void *pvContext);
static EC_T_DWORD DoHandleInterrupt(void * pvLinkParms);

static EC_T_BOOL ListCanAddAdapter(PT_CPSW_INTERNAL pAdapter);
static EC_T_BOOL ListAddOI(PT_CPSW_INTERNAL poAdapter);
static EC_T_BOOL ListRmOI(PT_CPSW_INTERNAL poAdapter);

static EC_T_DWORD CPSWMapMemory(PT_CPSW_INTERNAL pAdapter, EC_T_DWORD dwBase, EC_T_DWORD *pdwBaseMapped);
static void CPSWMapRegisterSet(PT_CPSW_INTERNAL pAdapter);
static void CPSWReset(PT_CPSW_INTERNAL pAdapter);
static void CPSWTeardown(PT_CPSW_INTERNAL pAdapter);
static void CPSWSetupRxDtors(PT_CPSW_INTERNAL pAdapter);
static void CPSWSetupTxDtors(PT_CPSW_INTERNAL pAdapter);
static void CPSWSetupBuffers(PT_CPSW_INTERNAL pAdapter);
static T_CPSW_FRAMEBUFENTRY* CPSWAllocRxBuffer(PT_CPSW_INTERNAL pAdapter);
static T_CPSW_FRAMEBUFENTRY* CPSWAllocTxBuffer(PT_CPSW_INTERNAL pAdapter);
static void CPSWInitializeHardware(PT_CPSW_INTERNAL pAdapter);
static EC_T_DWORD CPSWMdioRead(PT_CPSW_INTERNAL pAdapter, EC_T_DWORD dwRegNo);
static EC_T_BOOL CPSWMdioWrite(PT_CPSW_INTERNAL pAdapter, EC_T_DWORD dwRegNo, EC_T_DWORD dwValue);
static EC_T_BOOL CPSWPhySetup(PT_CPSW_INTERNAL pAdapter);
static EC_T_BOOL CPSWPhyQueryCapabilities(PT_CPSW_INTERNAL pAdapter);
static EC_T_DWORD CPSWPhyUpdateLinkStatus(PT_CPSW_INTERNAL pAdapter);
static void CPSWUpdateMacSpeed(PT_CPSW_INTERNAL pAdapter);
static void CPSWCheckId(PT_CPSW_INTERNAL pAdapter);
#ifdef CPSW_ENABLE_CLOCKS
static void CPSWClkEnable(PT_CPSW_INTERNAL pAdapter);
#endif
static void CPSWCReadMacId(PT_CPSW_INTERNAL pAdapter);
#ifdef TRACE
static void emllDumpFrame(EC_T_BOOL bTxFrame, EC_T_BYTE *pbyFrame, EC_T_DWORD dwSize);
#endif

/***************************************************************************************************
*                                 GLOBALS
*/

/***************************************************************************************************
*                                 LOCALS
*/
static PT_CPSW_INTERNAL          S_oOpenInstanceRoot;
static EC_T_INT                  S_nOpenedInstances;

/***************************************************************************************************
*                                 HELPER FUNCTIONS
*/

static inline EC_T_DWORD dma_va2pa(PT_CPSW_INTERNAL pAdapter, EC_T_DWORD virtaddr)
{
   return pAdapter->dwDMAPa + (virtaddr - pAdapter->dwDMAVa);
}

static inline EC_T_DWORD dmadtor_va2pa(PT_CPSW_INTERNAL pAdapter, EC_T_DWORD virtaddr)
{
   return pAdapter->dwDmaDtorBasePa + (virtaddr - (EC_T_DWORD) pAdapter->pDmaDtorBase);
}

static inline EC_T_DWORD dma_pa2va(PT_CPSW_INTERNAL pAdapter, EC_T_DWORD physaddr)
{
   return pAdapter->dwDMAVa + (physaddr - pAdapter->dwDMAPa);
}

static inline EC_T_DWORD dmadtor_pa2va(PT_CPSW_INTERNAL pAdapter, EC_T_DWORD physaddr)
{
   return (EC_T_DWORD) pAdapter->pDmaDtorBase + (physaddr - pAdapter->dwDmaDtorBasePa);
}

#if (defined INSTRUMENT_LL)
#include <LinkOsMock.h>
#define w32(reg, val) LinkOsMock::RegWrite32(reg, val)
#define r32(reg) LinkOsMock::RegRead32(reg)
#else

inline void w32(volatile EC_T_DWORD* addr, EC_T_DWORD val)
{
   LinkOsMemoryBarrier();
   *addr = val;
}

inline EC_T_DWORD r32(volatile EC_T_DWORD* addr)
{
   EC_T_DWORD val = *addr;
   LinkOsMemoryBarrier();
   return val;
}
#endif

static inline void CPSWFreeBuffer(T_CPSW_BUFENTRY *pBuf)
{
   pBuf->eBufState = BS_FREE;
}

static EC_T_DWORD inline CPSWPortIdx(PT_CPSW_INTERNAL pAdapter)
{
   /* dwInstance is 1-based but port index is 0-based */
   return pAdapter->oInitParms.linkParms.dwInstance - 1;
}

static EC_T_VOID CPSWCopyInitParam(PT_CPSW_INTERNAL pAdapter, EC_T_LINK_PARMS_CPSW* pLinkParmsAdapter)
{
   if (pLinkParmsAdapter->linkParms.dwSignature == EC_LINK_PARMS_SIGNATURE_CPSW_V1)
   {
      LinkOsMemcpy(&(pAdapter->oInitParms), pLinkParmsAdapter, sizeof(EC_T_LINK_PARMS_CPSW_V1));

      // fill new fields
      pAdapter->oInitParms.bNotUseDmaBuffers = EC_FALSE; /* use old behavior */
   }
   else
   {
      LinkOsMemcpy(&(pAdapter->oInitParms), pLinkParmsAdapter, sizeof(EC_T_LINK_PARMS_CPSW));
   }
}



/***************************************************************************************************
*                                 DRIVER API
*/

/********************************************************************************/
/** \brief Open Link Layer connection.
*
* \return EC_E_NOERROR or error code.
*/
static EC_T_DWORD EcLinkOpen
(   
 void*                      pvLinkParms,            /* [in]  link parameters */
 EC_T_RECEIVEFRAMECALLBACK  pfReceiveFrameCallback, /* [in]  pointer to rx callback function */
 EC_T_LINK_NOTIFY           pfLinkNotifyCallback,   /* [in]  pointer to notification callback function */
 void*                      pvContext,              /* [in]  caller context, to be used in callback functions */
 void**                     ppvInstance             /* [out] instance handle */
 )
{
   EC_T_DWORD dwRetVal = EC_E_ERROR;
   EC_T_LINK_PARMS_CPSW *pLinkParmsAdapter = (EC_T_LINK_PARMS_CPSW*)pvLinkParms;
   PT_CPSW_INTERNAL pAdapter = EC_NULL;
   EC_T_DWORD dwRegisterBase = 0;
   EC_T_DWORD dwOffs;

   EC_UNREFPARM(pfLinkNotifyCallback);

   /* check parameters */
   if (EC_NULL == ppvInstance)
   {
      SYS_ERRORMSGC(("CPSW-EcLinkOpen(): No memory for Driver Instance handle provided (ppvInstance may not be EC_NULL)!\n"));
      dwRetVal = EC_E_INVALIDPARM;
      goto Exit;
   }
   if (EC_NULL == pLinkParmsAdapter)
   {
      SYS_ERRORMSGC(("CPSW-EcLinkOpen(): Missing Configuration for CPSW Link Layer!\n"));
      dwRetVal = EC_E_INVALIDPARM;
      goto Exit;
   }
   if (EC_LINK_PARMS_SIGNATURE_CPSW != pLinkParmsAdapter->linkParms.dwSignature
      && EC_LINK_PARMS_SIGNATURE_CPSW_V1 != pLinkParmsAdapter->linkParms.dwSignature)
   {
      SYS_ERRORMSGC(("CPSW-EcLinkOpen(): Invalid Configuration for CPSW Link Layer!\n"));
      dwRetVal = EC_E_INVALIDPARM;
      goto Exit;
   }
   if (EcLinkMode_INTERRUPT == pLinkParmsAdapter->linkParms.eLinkMode)
   {
      SYS_ERRORMSGC(("CPSW-EcLinkOpen(): interrupt mode not supported!\n"));
      dwRetVal = EC_E_INVALIDPARM;
      goto Exit;
   }
   if ((pLinkParmsAdapter->linkParms.dwInstance < 1) || (pLinkParmsAdapter->linkParms.dwInstance > 2))
   {
      SYS_ERRORMSGC(("CPSW-EcLinkOpen(): Instance must be 1 or 2!\n"));
      dwRetVal = EC_E_INVALIDPARM;
      goto Exit;
   }
   if (pLinkParmsAdapter->dwPortPrio != 0 && pLinkParmsAdapter->dwPortPrio != 1)
   {
      SYS_ERRORMSGC(("CPSW-EcLinkOpen(): Port priority must be 0 or 1!\n" ));
      dwRetVal = EC_E_INVALIDPARM;
      goto Exit;
   }

   /* create instance */
   *ppvInstance = EC_NULL;
#if defined STARTERWARE_NOOS && defined ADAPTER_DESC_FROM_UC_MEM
   pAdapter = (PT_CPSW_INTERNAL)(GetStartAddrDdrUc() + 0x100000);
   DBG( "pAdapter from uncached ram at 0x%x\n", (EC_T_DWORD)pAdapter );
#else
   pAdapter = (PT_CPSW_INTERNAL) LinkOsMalloc(sizeof(T_CPSW_INTERNAL));
#endif

   LinkOsMemset(pAdapter, 0, sizeof(T_CPSW_INTERNAL));
   CPSWCopyInitParam(pAdapter, pLinkParmsAdapter);

   pAdapter->oInitParms.dwPhyAddr &= 0x1F; // 0 .. 31
   pAdapter->dwRxDmaChan = PRI2DMACHAN(pAdapter->oInitParms.dwPortPrio);
   pAdapter->dwTxDmaChanCyc = pAdapter->dwRxDmaChan & (~1);
   pAdapter->dwTxDmaChanAcyc = pAdapter->dwRxDmaChan | 1;
   pAdapter->dwTxDmaChan = pAdapter->dwTxDmaChanCyc;

   if (!ListCanAddAdapter(pAdapter))
   {
      SYS_ERRORMSG(("CPSW-EcLinkOpen(): Instance already in use!!\n"));
      dwRetVal = EC_E_INVALIDPARM;
      goto Exit;
   }

   pAdapter->dwLosalHandle = LinkOsOpen();

   if (pAdapter->oInitParms.bMaster)
   {
      // Set PIN MUX / Enable Clocks
      LinkOsPlatformInit();
#ifdef CPSW_ENABLE_CLOCKS   
      CPSWClkEnable(pAdapter);
#endif
   }

   CPSWCReadMacId(pAdapter); // Read MAC-ID from EFUSE

   INF( "Port %d, Prio %d, Flags [%s] [%s], MAC %02x:%02x:%02x:%02x:%02x:%02x\n\n",
      pLinkParmsAdapter->linkParms.dwInstance,
      pLinkParmsAdapter->dwPortPrio,
      (pLinkParmsAdapter->linkParms.eLinkMode == EcLinkMode_INTERRUPT) ? "Interrupt" : "Polling",
      pLinkParmsAdapter->bMaster ? "Master" : "Slave",
      pAdapter->abyStationAddress[0], pAdapter->abyStationAddress[1], pAdapter->abyStationAddress[2],
      pAdapter->abyStationAddress[3], pAdapter->abyStationAddress[4], pAdapter->abyStationAddress[5]);

   // Map 32k register file
   if (CPSWMapMemory(pAdapter,
      pAdapter->oInitParms.oPlatCfg.dwRegisterBase, &dwRegisterBase) != EC_E_NOERROR)
   {
      dwRetVal = EC_E_NOMEMORY;
      goto Exit;
   }

   pAdapter->dwRegisterBase = dwRegisterBase;
   CPSWMapRegisterSet(pAdapter);

   /* configure new instance */

   pAdapter->dwSignature            = EC_LINK_PARMS_SIGNATURE_CPSW;
   pAdapter->pfReceiveFrameCallback = pfReceiveFrameCallback;
   pAdapter->pvCallbackContext      = pvContext;

   /* fill IRQ Parms */
   pAdapter->oIrqParms.pvAdapter        = (void *) pAdapter;
   pAdapter->oIrqParms.dwSysIrq         = pAdapter->oInitParms.dwRxInterrupt;
   pAdapter->oIrqParms.pfIrqOpen        = EcLinkIrqOpen;
   pAdapter->oIrqParms.pfIrqClose       = EcLinkIrqClose;
   pAdapter->oIrqParms.pfIrqAckAll      = EcLinkIrqAck;
   pAdapter->oIrqParms.pvLOSALContext   = EC_NULL;          /* Initialized by LOSAL */
   pAdapter->oIrqParms.dwIstPriority    = pLinkParmsAdapter->linkParms.dwIstPriority;
   pAdapter->oIrqParms.dwIstStackSize   = 0x4000;           /*  IST Thread Stack size */
   pAdapter->oIrqParms.pfIstFunction    = DoHandleInterrupt;
   pAdapter->oIrqParms.pfIsrFunction    = EC_NULL;
   pAdapter->oIrqParms.pvIsrFuncCtxt    = EC_NULL;
   pAdapter->oIrqParms.eIntSource       = INTSOURCE_INTERNAL; /* no PCI */

#if defined COPY_FRAME
   pAdapter->dwRxCachedBufferSize = CPSW_RXCACHEDBUFCNT * CPSW_RXBUFLEN;
   pAdapter->dwTxCachedBufferSize = CPSW_TXCACHEDBUFCNT * CPSW_TXBUFLEN;

   pAdapter->dwTxCachedBufferSize += CACHELINE_SIZE;
   pAdapter->dwRxCachedBufferSize += CACHELINE_SIZE;

   pAdapter->pRxCachedBuffer = (EC_T_BYTE *) LinkOsMalloc(pAdapter->dwRxCachedBufferSize);
   pAdapter->pTxCachedBuffer = (EC_T_BYTE *) LinkOsMalloc(pAdapter->dwTxCachedBufferSize);

   if (pAdapter->pRxCachedBuffer == EC_NULL || pAdapter->pTxCachedBuffer == EC_NULL)
   {
      SYS_ERRORMSGC(("CPSW-EcLinkOpen(): Error Alloc Buffer memory\n"));
      dwRetVal = EC_E_NOMEMORY;
      goto Exit;
   }

   pAdapter->pRxCachedBufferOriginal = pAdapter->pRxCachedBuffer;
   pAdapter->pTxCachedBufferOriginal = pAdapter->pTxCachedBuffer;

   pAdapter->pRxCachedBuffer = (EC_T_BYTE*)CPSW_CACHE_UP((EC_T_DWORD)pAdapter->pRxCachedBuffer);
   pAdapter->pTxCachedBuffer = (EC_T_BYTE*)CPSW_CACHE_UP((EC_T_DWORD)pAdapter->pTxCachedBuffer);

#endif

   //
   // Setup DMA
   //

   pAdapter->dwTxDescSize    = sizeof(T_CPSW_DESC) * CPSW_TXDESCNT;
   pAdapter->dwRxDescSize    = sizeof(T_CPSW_DESC) * CPSW_RXDESCNT;
   pAdapter->dwTxBufferSize  = CPSW_TXDMABUFCNT * CPSW_TXBUFLEN;
   pAdapter->dwRxBufferSize  = CPSW_RXDMABUFCNT * CPSW_RXBUFLEN;
   pAdapter->dwDMASize       = 0;

   // RX Data buffers (each 2k)
   pAdapter->dwDMASize    += pAdapter->dwRxBufferSize;
   // TX Data buffers (each 2k)
   pAdapter->dwDMASize    += pAdapter->dwTxBufferSize;
#ifndef USE_CPPI_RAM
   // Align this space (needed by CPSW)
   pAdapter->dwDMASize    = CPSW_DMA_ALIGN(pAdapter->dwDMASize);
   // RX Descriptors
   pAdapter->dwDMASize    += pAdapter->dwRxDescSize;
   // TX Descriptors
   pAdapter->dwDMASize    += pAdapter->dwTxDescSize;
#endif

   // Add one extra page then align to next page boundary
   pAdapter->dwDMASize    += CPSW_PAGE_SIZE;
   pAdapter->dwDMASize     = PAGE_UP(pAdapter->dwDMASize);

   // Allocate DMA memory
   LinkOsAllocDmaBuffer(
      &pAdapter->oAdapterObject,
      (EC_T_BYTE **)&pAdapter->dwDMAVa,
      (EC_T_BYTE **)&pAdapter->dwDMAUncachedVa,
      (EC_T_BYTE **)&pAdapter->dwDMAPa,
      pAdapter->dwDMASize );

   if ( pAdapter->dwDMAVa == EC_NULL )
   {
      SYS_ERRORMSGC(("CPSW-EcLinkOpen(): Error Mapping DMA memory\n"));
      dwRetVal = EC_E_NOMEMORY;
      goto Exit;
   }

   DBG("DMA buffer from uncached ram at 0x%x\n", pAdapter->dwDMAVa);

   // Make sure that start address is page aligned
   pAdapter->dwDMAOffset = PAGE_UP(pAdapter->dwDMAVa) - pAdapter->dwDMAVa;
   pAdapter->dwDMAVa += pAdapter->dwDMAOffset;
   pAdapter->dwDMAPa += pAdapter->dwDMAOffset;
   pAdapter->dwDMASize -= pAdapter->dwDMAOffset;

   // Initialize DMA space with zeros
   LinkOsMemset(pAdapter->dwDMAVa, 0, pAdapter->dwDMASize);

   // Store address of RX buffers
   pAdapter->pRxBuffer = (EC_T_BYTE *) pAdapter->dwDMAVa;

   // Store address of TX buffers
   pAdapter->pTxBuffer = (EC_T_BYTE *) pAdapter->pRxBuffer + pAdapter->dwRxBufferSize;

#ifdef USE_CPPI_RAM
   dwOffs = CPSWPortIdx(pAdapter) * CPSW_PORTDMA_LENGTH;
   pAdapter->pDmaDtorBase = (T_CPSW_DESC *) ((EC_T_DWORD) pAdapter->regs.BdRam + dwOffs);
   pAdapter->dwDmaDtorBasePa = pAdapter->oInitParms.oPlatCfg.dwRegisterBase + 
      pAdapter->oInitParms.oPlatCfg.dwBdRamOffs + dwOffs;
   LinkOsMemset((EC_T_BYTE *) pAdapter->pDmaDtorBase, 0, CPSW_PORTDMA_LENGTH);
#else
   EC_UNREFPARM(dwOffs);
   pAdapter->pDmaDtorBase = (T_CPSW_DESC *) CPSW_DMA_ALIGN((EC_T_DWORD) pAdapter->pTxBuffer + pAdapter->dwTxBufferSize);
   pAdapter->dwDmaDtorBasePa = dma_va2pa(pAdapter, (EC_T_DWORD) pAdapter->pDmaDtorBase);
#endif

   // Store address of RX descriptors
   pAdapter->pRxDesc = pAdapter->pDmaDtorBase;

   // Store address of TX descriptors
   pAdapter->pTxDesc = (T_CPSW_DESC *) ((EC_T_DWORD) pAdapter->pDmaDtorBase + pAdapter->dwRxDescSize);

   DBG("VirtAddr: RegBase 0x%08x, DmaDtorBase 0x%08x, DmaBase 0x%08x\n", 
      pAdapter->dwRegisterBase,
      pAdapter->pDmaDtorBase,
      pAdapter->dwDMAVa);
   DBG("PhysAddr: RegBase 0x%08x, DmaDtorBase 0x%08x, DmaBase 0x%08x\n", 
      pAdapter->oInitParms.oPlatCfg.dwRegisterBase,
      pAdapter->dwDmaDtorBasePa,
      pAdapter->dwDMAPa);

   // initialize RX buffer from the heap
   if (pAdapter->oInitParms.bNotUseDmaBuffers)
   {
      dwRetVal = pAdapter->rxBuffers.Initialize(CPSW_RXDMABUFCNT, CPSW_RXBUFLEN);
      if (EC_E_NOERROR != dwRetVal)
         goto Exit;
   }

   LinkOsInterruptDisable(&pAdapter->oIrqParms);

   if (pAdapter->oInitParms.bMaster)
   {
      CPSWReset(pAdapter); // Reset chip
      CPSWCheckId(pAdapter); // Print ID info
   }

   // Make sure that the DMA is stopped (An previously instance may have gone
   // without calling CPSWTeardown() on LL close.
   // Teardown may set the teardown bit in the (old) DMA dtor if present.
   // -> Make sure RX DMA dtors are initialized after teardown!
   CPSWTeardown(pAdapter);

   CPSWSetupBuffers(pAdapter);
   CPSWSetupRxDtors(pAdapter);
   CPSWSetupTxDtors(pAdapter);

   CPSWInitializeHardware(pAdapter);

   // Initialize IRQ if desired
   if ( EcLinkMode_INTERRUPT == pAdapter->oInitParms.linkParms.eLinkMode )
   {
      if (! LinkOsInterruptInitialize(&pAdapter->oIrqParms))
      {
         SYS_ERRORMSGC(("CPSW-EcLinkOpen(): Cannot Initialize interrupt\n"));
         dwRetVal = EC_E_ERROR;
         goto Exit;
      }
   }

   // enqueue adapter
   ListAddOI(pAdapter);

   // increment instance counter
   S_nOpenedInstances++;

   // return "handle"
   *ppvInstance = pAdapter;

   // no errors
   dwRetVal = EC_E_NOERROR;

Exit:

   if (EC_E_NOERROR != dwRetVal && EC_E_INVALIDSTATE != dwRetVal)
   {
      if (EC_NULL != pAdapter)
      {
         EcLinkClose(pAdapter);
      }
   }

   return dwRetVal;
}

/*****************************************************************************/
/** \brief Verifies adapter is correct (not null and type is CPSW).
*/
static inline EC_T_BOOL IsCorrectAdapter(PT_CPSW_INTERNAL pAdapter)
{
   if (EC_NULL == pAdapter || pAdapter->dwSignature != EC_LINK_PARMS_SIGNATURE_CPSW)
   {
      return EC_FALSE;
   }
   return EC_TRUE;
}

/********************************************************************************/
/** \brief Close Link Layer connection.
*
* \return EC_E_NOERROR or error code.
*/
static EC_T_DWORD EcLinkClose(
                              void* pvInstance         /* [in] instance handle */
                              )
{
   EC_T_DWORD              dwRetVal    = EC_E_ERROR;
   PT_CPSW_INTERNAL     pAdapter    = (PT_CPSW_INTERNAL) pvInstance;

#if defined DEBUG
   // check for Type Signature */
   if (EC_NULL == pAdapter || pAdapter->dwSignature != EC_LINK_PARMS_SIGNATURE_CPSW)
   {
      dwRetVal = EC_E_INVALIDPARM;
      goto Exit;
   }
#endif

   if ( EcLinkMode_INTERRUPT == pAdapter->oInitParms.linkParms.eLinkMode )
   {
      // Disable interrupt
      LinkOsInterruptDisable( &pAdapter->oIrqParms );
   }

   // Shutdown RX+TX for the current channel
   CPSWTeardown(pAdapter);

   // Free DMA memory
   if (pAdapter->dwDMAVa != 0)
   {
      if (pAdapter->oInitParms.bNotUseDmaBuffers)
      {
         pAdapter->rxBuffers.Deinitialize();
      }

      pAdapter->dwDMAVa -= pAdapter->dwDMAOffset;
      pAdapter->dwDMASize   += pAdapter->dwDMAOffset;
      pAdapter->dwDMAPa     -= pAdapter->dwDMAOffset;

      LinkOsFreeDmaBuffer(
         &pAdapter->oAdapterObject,
         (EC_T_BYTE *)pAdapter->dwDMAPa,
         (EC_T_BYTE *)pAdapter->dwDMAVa,
         (EC_T_BYTE *)pAdapter->dwDMAUncachedVa,
         pAdapter->dwDMASize );

      pAdapter->dwDMAPa     = 0;
      pAdapter->dwDMAVa     = 0;
   }

   // Unmap IO memory
   if (pAdapter->dwRegisterBase != 0)
   {
      LinkOsUnmapMemory((EC_T_BYTE *)pAdapter->dwRegisterBase, CPSW_IO_LENGTH);
   }

#if defined COPY_FRAME
   if (pAdapter->pRxCachedBufferOriginal != EC_NULL)
   {
      LinkOsFree(pAdapter->pRxCachedBufferOriginal);
      pAdapter->pRxCachedBuffer = EC_NULL;
      pAdapter->pRxCachedBufferOriginal = EC_NULL;
   }

   if (pAdapter->pTxCachedBufferOriginal != EC_NULL)
   {
      LinkOsFree(pAdapter->pTxCachedBufferOriginal);
      pAdapter->pTxCachedBuffer = EC_NULL;
      pAdapter->pTxCachedBufferOriginal = EC_NULL;
   }
#endif

   LinkOsClose();

   // Remove adapter instance from instance-list
   ListRmOI(pAdapter);
   S_nOpenedInstances--;

#if defined STARTERWARE_NOOS && defined ADAPTER_DESC_FROM_UC_MEM
#else
   LinkOsFree(pAdapter);
#endif

   dwRetVal = EC_E_NOERROR;
   goto Exit;

Exit:
   return dwRetVal;
}

#ifdef DEBUG
static void DumpDtor(PT_CPSW_INTERNAL pAdapter, volatile T_CPSW_DESC *pDesc)
{
   PRINT("PA 0x%x, Next 0x%x, Buf 0x%x, Len 0x%x, Mode 0x%x [%s %s %s %s]\n",
      dmadtor_va2pa(pAdapter, (EC_T_DWORD)pDesc),
      pDesc->dwNext,
      pDesc->dwBuffer,
      pDesc->dwLen,
      pDesc->dwMode,
      (pDesc->dwMode & CPDMA_DESC_SOP) ? "SOP" : "",
      (pDesc->dwMode & CPDMA_DESC_EOP) ? "EOP" : "",
      (pDesc->dwMode & CPDMA_DESC_OWNER) ? "OWN" : "",
      (pDesc->dwMode & CPDMA_DESC_EOQ) ? "EOQ" : ""
      );
}
#endif // ifdef DEBUG

static void DumpTxDtors(PT_CPSW_INTERNAL pAdapter, const char *id)
{
#ifdef DEBUG
   PCPSW_REGSET regs = &pAdapter->regs;
   PRINT("TX Dtors (%s): DMA-Status 0x%x, CP 0x%0x, HDP 0x%x, PA[N] 0x%x, PA[N-1] 0x%x\n", 
      id,
      R32(regs->Dma->DMAStatus),
      R32(regs->StateRam->Tx_CP[pAdapter->dwTxDmaChan]),
      R32(regs->StateRam->Tx_HDP[pAdapter->dwTxDmaChan]),
      dmadtor_va2pa(pAdapter, (EC_T_DWORD)TX_DESC_PTR(pAdapter->dwTxDescIdx)),
      dmadtor_va2pa(pAdapter, (EC_T_DWORD)pAdapter->pTxTailDesc)
      );
   for (int i = 0; i < CPSW_TXDESCNT; ++i)
   {
      PRINT("Dtor %d: ", i);
      DumpDtor(pAdapter, TX_DESC_PTR(i));
   }
#else
   EC_UNREFPARM(pAdapter);
   EC_UNREFPARM(id);
#endif // ifdef DEBUG
}

static EC_T_DWORD CPSWWaitUntilTxDmaIsEmpty(PT_CPSW_INTERNAL pAdapter)
{
   // Wait until any previous DMA has finished (HDP is set to 0 if TX-DMA-Channel goes idle).
   // Workaround for an issue where appending to a running DMA-Q, the MAC runs to EOQ 
   // even though pTxDescTail EOQ-flag has been reset.
   // This issue has been monitored for the shared User/Kernel CPSW-MAC mode under EC7.

   EC_T_DWORD dwCnt = 0;
   while (R32(pAdapter->regs.StateRam->Tx_HDP[pAdapter->dwTxDmaChan]))
   {
      LinkOsSysDelay(10);
      dwCnt++;
      if (dwCnt > 500000) // 500ms
      {
         ERR("DMA wait timeout\n");
         DumpTxDtors(pAdapter, "DMA-Wait-Timeout");

         return EC_E_NOMEMORY;
      }
   }
   return EC_E_NOERROR;
}

#ifdef DEBUG
static EC_T_BOOL IsCorrectParamsForSendFrame(PT_CPSW_INTERNAL pAdapter,
   EC_T_LINK_FRAMEDESC* pLinkFrameDesc)
{
   T_CPSW_BUFENTRY*         pBufEntry = (T_CPSW_BUFENTRY*)(pLinkFrameDesc->pbyFrame - CPSW_BUFENTRY_FRAME_OFFSET);
   EC_T_BYTE*               pBufMem = (EC_T_BYTE*)pBufEntry;

   LinkOsDbgAssert((EC_T_DWORD)pBufMem == CPSW_CACHE_DOWN((EC_T_DWORD)pBufMem));
   if (!IsCorrectAdapter(pAdapter))
   {
      return EC_FALSE;
   }

   /* Check if frame address was allocated from the DMA buffer pool */
   if (
#ifdef COPY_FRAME
      (pBufMem < pAdapter->pTxCachedBuffer)
      || ((pBufMem + pLinkFrameDesc->dwSize) >(pAdapter->pTxCachedBuffer + pAdapter->dwTxCachedBufferSize))
#else
      (pBufMem < pAdapter->pTxBuffer)
      || ((pBufMem + pLinkFrameDesc->dwSize) < pAdapter->pTxBuffer)
      || ((pBufMem + pLinkFrameDesc->dwSize) >(pAdapter->pTxBuffer + pAdapter->dwTxBufferSize))
#endif /* !COPY_FRAME */
      || (pBufEntry->dwMagicKey != BUFFER_MAGIC_TX)
      )
   {
      return EC_FALSE;
   }

   if (pLinkFrameDesc->dwSize < 60)
   {
      return EC_FALSE;
   }

   return EC_TRUE;
}
#endif /* DEBUG */

/*****************************************************************************/
/** \brief Send data frame
*
* \return EC_E_NOERROR or error code.
*/
static EC_T_DWORD EcLinkSendFrameFromDma(
                                  void*           pvInstance,
                                  EC_T_LINK_FRAMEDESC* pLinkFrameDesc      /* [in]  link frame descriptor */
                                  )
{
   EC_T_DWORD               dwRetVal  = EC_E_ERROR;
   PT_CPSW_INTERNAL         pAdapter  = (PT_CPSW_INTERNAL)pvInstance;
   volatile T_CPSW_DESC*    pTxDesc;
   EC_T_DWORD               txDescPa;
   T_CPSW_BUFENTRY*         pBufEntry = (T_CPSW_BUFENTRY*)(pLinkFrameDesc->pbyFrame - CPSW_BUFENTRY_FRAME_OFFSET);
   EC_T_DWORD               dwTmp, dwMode;
   PCPSW_REGSET             regs      = &pAdapter->regs;
   EC_T_DWORD               dwDmaChan = pAdapter->dwTxDmaChan;
#ifdef COPY_FRAME
   T_CPSW_FRAMEBUFENTRY*    pTxBuf;
#endif

#ifdef DEBUG
   if (!IsCorrectParamsForSendFrame(pAdapter, pLinkFrameDesc))
   {
      dwRetVal = EC_E_INVALIDPARM;
      goto Exit;
   }
#endif /* DEBUG */

#if !defined USE_CPSW_TEST
   /* Overwrite the dst address with the Ethernet broadcast address. Our stack will use
      the multicast address defined in the ENI as dst address.
      We would need to retrieve this multicast address (e.g. by Master's ioctl) and then
      setup an vlan/multicast ALE entry to recognize that address if the frame is received. */
   for (int i = 0; i < MAC_ADDR_LEN; ++i) pLinkFrameDesc->pbyFrame[i] = 0xFF;
#endif

   /* Lookup next free descriptor in descriptor ring buffer */
   pTxDesc = TX_DESC_PTR(pAdapter->dwTxDescIdx);

   /* If the current descriptor has the OWN bit set, then the corresponding buffer was not (already) processed by the NIC.
      This means that the ring buffer write pointer (Host: dwTxDescIdx) has reached the read pointer (NIC: OWN bit) */
   if (R32(pTxDesc->dwMode) & (CPDMA_DESC_OWNER))
   {
      ERR("EcLinkSendFrame: Owner bit set, tx buffer overflow?!\n");
      DumpTxDtors(pAdapter, "TX-Buffer-Underun");
      dwRetVal = EC_E_NOMEMORY;
      goto Exit;
   }

#if defined COPY_FRAME
   pTxBuf = TX_DMABUF_PTR(pAdapter->dwTxDescIdx); /* Get DMA buffer: Tx Desc Idx == Tx Buffer Idx */
   LinkOsMemcpy(pTxBuf->pFrame, pLinkFrameDesc->pbyFrame, pLinkFrameDesc->dwSize); /* Copy cached buffer to DMA buffer */
   CPSWFreeBuffer(pBufEntry); /* Free buffer from cached pool */
   pBufEntry = &pTxBuf->bufentry; /* pBufEntry now points to the DMA buffer */
#endif

   /* Assign descriptor index to the management object for the send buffer */
   pBufEntry->dwDescIdx = pAdapter->dwTxDescIdx;
   /* Mark buffer as used for DMA transfer */
   pBufEntry->eBufState = BS_DMA_STARTED;
   /* Debugging helper */
   pBufEntry->dwFrameLen = pLinkFrameDesc->dwSize & CPDMA_BUFLEN_MASK;

   /* Prepare descriptor for send */
   W32(pTxDesc->dwNext, 0);
#if defined COPY_FRAME
   LinkOsDbgAssert((EC_T_DWORD) pTxBuf->pFrame == CPSW_CACHE_DOWN((EC_T_DWORD) pTxBuf->pFrame));
#else
   LinkOsDbgAssert((EC_T_DWORD)pLinkFrameDesc->pbyFrame == CPSW_CACHE_DOWN((EC_T_DWORD)pLinkFrameDesc->pbyFrame));

   /* Write cache lines back to memory */
   LinkOsCacheDataSync(pLinkFrameDesc->pbyFrame,
      dma_va2pa(pAdapter, (EC_T_DWORD) pLinkFrameDesc->pbyFrame),
      CPSW_CACHE_UP(pLinkFrameDesc->dwSize));

   W32(pTxDesc->dwBuffer, dma_va2pa(pAdapter, (EC_T_DWORD) pLinkFrameDesc->pbyFrame));
#endif
   W32(pTxDesc->dwLen, pLinkFrameDesc->dwSize & CPDMA_BUFLEN_MASK);
   dwTmp = (pLinkFrameDesc->dwSize & CPDMA_BUFLEN_MASK) 
      | CPDMA_DESC_SOP | CPDMA_DESC_EOP | CPDMA_DESC_OWNER | CPDMA_DESC_PORTEN;
   dwTmp |= (pAdapter->oInitParms.linkParms.dwInstance) << CPDMA_DESC_PORTSHIFT; /* Directed send to port */
   W32(pTxDesc->dwMode, dwTmp);

   txDescPa = dmadtor_va2pa(pAdapter, (EC_T_DWORD) pTxDesc);

   _TRC("Read HDPPhys[%d] 0x%08x\n", dwDmaChan, R32(regs->StateRam->Tx_HDP[dwDmaChan]));

   if (pAdapter->bGatherFrames)
   {
      if (!pAdapter->pTxHeadDesc)
         pAdapter->pTxHeadDesc = pTxDesc;
      
      if (pAdapter->pTxTailDesc)
      {
         volatile T_CPSW_DESC *pTxDescTail = pAdapter->pTxTailDesc;
         W32(pTxDescTail->dwNext, txDescPa); /* Append dtor to the tail */
      }
   }
   else
   {
      if (pAdapter->pTxTailDesc == EC_NULL) /* First send after adapter reset / initialization */
      {
         TRC("TX Init HDP[%d] with 0x%08x\n", dwDmaChan, txDescPa);
         W32(regs->StateRam->Tx_HDP[dwDmaChan], txDescPa);
      }
      else
      {
         volatile T_CPSW_DESC *pTxDescTail = pAdapter->pTxTailDesc;
         W32(pTxDescTail->dwNext, txDescPa); /* Append dtor to the tail */

#ifdef _WIN32_WCE
         if (EC_E_NOERROR != (dwRetVal = CPSWWaitUntilTxDmaIsEmpty(pAdapter)))
            goto Exit;
#endif /* _WIN32_WCE */

         /* If the DMA engine, already reached the end of the chain,
            the EOQ will be set. In that case, the HDP shall be written again. */
         dwMode = R32(pTxDescTail->dwMode);
         if ((dwMode & (CPDMA_DESC_EOQ | CPDMA_DESC_OWNER)) == CPDMA_DESC_EOQ)
         {
            TRC("TX EOQ HDP[%d] reinit with 0x%08x\n", dwDmaChan, txDescPa);
            W32(pTxDescTail->dwMode, dwMode & ~CPDMA_DESC_EOQ); /* Clear EOQ */
            W32(regs->StateRam->Tx_HDP[dwDmaChan], txDescPa); /* Restart DMA */
         }
      }
      dwTmp = R32(regs->StateRam->Tx_HDP[dwDmaChan]);
   }

   pAdapter->pTxTailDesc = pTxDesc;
   
   /* Increment ring buffer pointer */
   pAdapter->dwTxDescIdx = NEXT_TX_DESC_IDX(pAdapter->dwTxDescIdx);

   /* immediate after sending packet do timestamping */
   if (EC_NULL != pLinkFrameDesc->pfnTimeStamp)
   {
      *pLinkFrameDesc->pdwLastTSResult    =  pLinkFrameDesc->pfnTimeStamp(pLinkFrameDesc->pvTimeStampCtxt, pLinkFrameDesc->pdwTimeStampLo);
      *pLinkFrameDesc->pdwTimeStampPostLo = *pLinkFrameDesc->pdwTimeStampLo;
   }

   /* no errors */
   dwRetVal = EC_E_NOERROR;

   _TRC("Write DescPhys 0x%08x, DescVirt 0x%08x, HDPPhys[%d] 0x%08x\n",
      txDescPa,
      (EC_T_DWORD) pTxDesc,
      dwDmaChan, dwTmp);

#if defined TRACE && 0
   TRC("TX buffer send %d, Size %d\n", pAdapter->dwTxDescIdx + 1, pLinkFrameDesc->dwSize);
   emllDumpFrame(EC_TRUE, pLinkFrameDesc->pbyFrame, pLinkFrameDesc->dwSize);
   /* wait until send and read result memory */
   LinkOsSleep(100);
   TRC("pTxDesc 0x%08x, TxDescPa 0x%08x, HDP[%d] 0x%08x, Next 0x%08x, Buffer 0x%08x, Len 0x%08x, Mode 0x%08x\n",
      (EC_T_DWORD)pTxDesc,
      dmadtor_va2pa(pAdapter, (EC_T_DWORD) pTxDesc),
      dwDmaChan, dwTmp,
      R32(pTxDesc->dwNext),
      R32(pTxDesc->dwBuffer),
      R32(pTxDesc->dwLen),
      R32(pTxDesc->dwMode));
#endif

Exit:

   return dwRetVal;
}

/*****************************************************************************/
/** \brief Send data frame
*
* \return EC_E_NOERROR or error code.
*/
static EC_T_DWORD EcLinkSendFrame(
   void*           pvInstance,
   EC_T_LINK_FRAMEDESC* pLinkFrameDesc      /* [in]  link frame descriptor */
   )
{
   PT_CPSW_INTERNAL      pAdapter = (PT_CPSW_INTERNAL)pvInstance;

#if defined DEBUG
   if (!IsCorrectAdapter(pAdapter) || EC_NULL == pLinkFrameDesc)
   {
      return EC_E_INVALIDPARM;
   }
#endif

   if (!pAdapter->oInitParms.bNotUseDmaBuffers)
   {
      return EcLinkSendFrameFromDma(pvInstance, pLinkFrameDesc);
   }
   else
   {
      EC_T_BYTE* pbyFrame = pLinkFrameDesc->pbyFrame;
      EC_T_DWORD dwSize = pLinkFrameDesc->dwSize;

      EC_T_DWORD dwRetVal = EcLinkAllocSendFrameFromDma(pAdapter, pLinkFrameDesc, pLinkFrameDesc->dwSize);
      if (EC_E_NOERROR == dwRetVal)
      {
         // copy frame
         OsMemcpy(pLinkFrameDesc->pbyFrame, pbyFrame, dwSize);

         dwRetVal = EcLinkSendFrameFromDma(pvInstance, pLinkFrameDesc);

         // restore members
         pLinkFrameDesc->pbyFrame = pbyFrame;
         pLinkFrameDesc->dwSize = dwSize;
      }

      return dwRetVal;
   }
}


/********************************************************************************/
/** \brief Send data frame and free the frame buffer. This function must be
*          supported if EcLinkAllocSendFrame() is supported
*
* \return EC_E_NOERROR or error code.
*/
static EC_T_DWORD EcLinkSendAndFreeFrame(
   void*           pvInstance,
   EC_T_LINK_FRAMEDESC* pLinkFrameDesc      /* [in]  link frame descriptor */
   )
{
   EC_T_DWORD dwRetVal = EC_E_ERROR;

   /* free-ing of buffers and descriptors is done during EcLinkAllocSendFrame so
   * calling the send routine is enough */
   dwRetVal = EcLinkSendFrame(pvInstance, pLinkFrameDesc);

   /* nothing to do */
   return dwRetVal;
}

/********************************************************************************/
/** \brief Poll for received frame from DMA memory
*
* \return EC_E_NOERROR or error code.
*/
static EC_T_DWORD EcLinkRecvFrameFromDma(
   PT_CPSW_INTERNAL pAdapter,
   EC_T_LINK_FRAMEDESC* pLinkFrameDesc)      /* [in]  link frame descriptor */
{
   EC_T_DWORD            dwRetVal = EC_E_ERROR;
   volatile T_CPSW_DESC* pRxDesc;
   EC_T_DWORD dwRxDescPa;
   PCPSW_REGSET regs = &pAdapter->regs;
   EC_T_DWORD dwMode;
   EC_T_DWORD dwDmaChan;

   // Lookup next descriptor and assigned buffer
   pRxDesc = RX_DESC_PTR(pAdapter->dwRxDescIdx);
   dwRxDescPa = dmadtor_va2pa(pAdapter, (EC_T_DWORD) pRxDesc);

   dwDmaChan = pAdapter->dwRxDmaChan;
#if defined TRACE && 0
   TRC("\npRxDesc 0x%08x, RxDescPa 0x%08x, HDP[%d] 0x%08x, Next 0x%08x, Buffer 0x%08x, Len 0x%08x, Mode 0x%08x\n\n",
      (EC_T_DWORD)pRxDesc,
      dwRxDescPa,
      dwDmaChan, R32(regs->StateRam->Rx_HDP[dwDmaChan]),
      R32(pRxDesc->dwNext),
      R32(pRxDesc->dwBuffer),
      R32(pRxDesc->dwLen),
      R32(pRxDesc->dwMode));
   
   // Dump dtors
   for (int i = 0; i < CPSW_RXDESCNT; ++i)
   {
     volatile T_CPSW_DESC* pTmpDesc = RX_DESC_PTR(i);
     TRC("Desc[%d] 0x%08x, RxDescPa 0x%08x, HDP[%d] 0x%08x, Next 0x%08x, Buffer 0x%08x, Len 0x%08x, Mode 0x%08x\n",
         i,
         (EC_T_DWORD)pTmpDesc,
         dmadtor_va2pa(pAdapter, (EC_T_DWORD) pTmpDesc),
         dwDmaChan, R32(regs->StateRam->Rx_HDP[dwDmaChan]),
         R32(pTmpDesc->dwNext),
         R32(pTmpDesc->dwBuffer),
         R32(pTmpDesc->dwLen),
         R32(pTmpDesc->dwMode));
    }
#endif

   dwMode = R32(pRxDesc->dwMode);
   if (! (dwMode & CPDMA_DESC_OWNER) ) // OWN Bit clear?
   {
      volatile T_CPSW_DESC* pRxDescTail;
      T_CPSW_FRAMEBUFENTRY* pBufEntry = EC_NULL;
      T_CPSW_BUFENTRY*      pTmpBufEntry = EC_NULL;
      T_CPSW_FRAMEBUFENTRY* pFreeBufEntry = EC_NULL;

      pTmpBufEntry = (T_CPSW_BUFENTRY *)dma_pa2va(pAdapter, R32(pRxDesc->dwBuffer));
      pBufEntry = (T_CPSW_FRAMEBUFENTRY *)((EC_T_BYTE *) pTmpBufEntry - CPSW_BUFENTRY_FRAME_OFFSET);

      LinkOsDbgAssert(pBufEntry->bufentry.dwMagicKey == BUFFER_MAGIC_RX);

      pFreeBufEntry = CPSWAllocRxBuffer(pAdapter);
      if (pFreeBufEntry == EC_NULL) // All buffers used?
      {
         ERR("Out of RX buffers\n");
         dwRetVal = EC_E_NOMEMORY;
         goto Exit;
      }

      // Get frame pointer and frame size
      pLinkFrameDesc->dwSize = dwMode & CPDMA_BUFLEN_MASK;

      LinkOsDbgAssert( (EC_T_DWORD)pBufEntry->pFrame == CPSW_CACHE_DOWN((EC_T_DWORD)pBufEntry->pFrame) );

      // Invalidate cachelines
      LinkOsCacheDataInvalidateBuff(pBufEntry->pFrame,
    		  dma_va2pa(pAdapter, (EC_T_DWORD) pBufEntry->pFrame),
    		  CPSW_CACHE_UP(pLinkFrameDesc->dwSize));

#if defined COPY_FRAME
      OsMemcpy(pFreeBufEntry->pFrame, pBufEntry->pFrame, pLinkFrameDesc->dwSize);
      pLinkFrameDesc->pbyFrame = pFreeBufEntry->pFrame;
#else
      pLinkFrameDesc->pbyFrame = pBufEntry->pFrame;
#endif
 
      // Prepare descriptor so that the NIC can reuse this descriptor for DMA transfer
      W32(pRxDesc->dwNext, 0);
#if !defined COPY_FRAME

      // Invalidate cache lines
      LinkOsCacheDataInvalidateBuff(pFreeBufEntry->pFrame,
    		  dma_va2pa(pAdapter, (EC_T_DWORD) pFreeBufEntry->pFrame),
    		  CPSW_CACHE_UP(CPSW_RXBUFLEN - CACHELINE_SIZE));

      W32(pRxDesc->dwBuffer, dma_va2pa(pAdapter, (EC_T_DWORD) pFreeBufEntry->pFrame));
#endif
      W32(pRxDesc->dwLen, CPDMA_RXBUFLEN);
      W32(pRxDesc->dwMode, CPDMA_DESC_OWNER | CPDMA_RXBUFLEN); // descriptor owned by MAC

      // Link dtor
      pRxDescTail = pAdapter->pRxTailDesc;
      W32(pRxDescTail->dwNext, dwRxDescPa);

      // If the DMA engine, already reached the end of the chain,
      // the EOQ will be set. In that case, the HDP shall be written again.
      dwMode = R32(pRxDescTail->dwMode);
      if ((dwMode & (CPDMA_DESC_EOQ | CPDMA_DESC_OWNER)) == CPDMA_DESC_EOQ)
      {
        TRC("RX EOQ HDP[%d] reinit with 0x%08x\n", dwDmaChan, dwRxDescPa);
        W32(pRxDescTail->dwMode, dwMode & ~CPDMA_DESC_EOQ); // Clear EOQ
        W32(regs->StateRam->Rx_HDP[dwDmaChan], dwRxDescPa); // Restart DMA
      }
      
#if defined TRACE && 0
      TRC("RX buffer recv %d, Size %d\n", pAdapter->dwRxDescIdx + 1, pLinkFrameDesc->dwSize);
      emllDumpFrame(EC_FALSE, pLinkFrameDesc->pbyFrame, pLinkFrameDesc->dwSize);
#endif

      pAdapter->pRxTailDesc = pRxDesc;

      // Increment descriptor ring buffer pointer
      pAdapter->dwRxDescIdx = NEXT_RX_DESC_IDX(pAdapter->dwRxDescIdx);
   }
   else
   {
       TRC("RX buffer empty %d\n", pAdapter->dwRxDescIdx);
   }

   dwRetVal = EC_E_NOERROR;

Exit:

   return dwRetVal;
}

/********************************************************************************/
/** \brief Release a frame buffer from DMA memory
*
* \return EC_E_NOERROR or error code.
*/
static void  FreeRecvFrameFromDma(
   PT_CPSW_INTERNAL     pAdapter,
   EC_T_LINK_FRAMEDESC* pLinkFrameDesc      /* [in]  link frame descriptor */
   )
{
   EC_UNREFPARM(pAdapter);

   T_CPSW_BUFENTRY* pBufEntry = (T_CPSW_BUFENTRY *)(pLinkFrameDesc->pbyFrame - CPSW_BUFENTRY_FRAME_OFFSET);
#if defined DEBUG
   EC_T_BYTE* pBufMem = (EC_T_BYTE *)pBufEntry;
   LinkOsDbgAssert((EC_T_DWORD)pBufMem == CPSW_CACHE_DOWN((EC_T_DWORD)pBufMem));
   LinkOsDbgAssert(pBufEntry->dwMagicKey == BUFFER_MAGIC_RX);

   // Check if frame address was allocated from the buffer pool
#if defined COPY_FRAME
   LinkOsDbgAssert((pBufMem >= pAdapter->pRxCachedBuffer)
      && ((pBufMem + pLinkFrameDesc->dwSize) <= (pAdapter->pRxCachedBuffer + pAdapter->dwRxCachedBufferSize)));
#else
   LinkOsDbgAssert((pBufMem >= pAdapter->pRxBuffer)
      && ((pBufMem + pLinkFrameDesc->dwSize) <= (pAdapter->pRxBuffer + pAdapter->dwRxBufferSize)));
#endif
#endif /* DEBUG */

   CPSWFreeBuffer(pBufEntry);
}

/********************************************************************************/
/** \brief Poll for received frame. This function is called by the ethercat Master
*          if the function EcLinkGetMode() returns EcLinkMode_POLLING
*
* \return EC_E_NOERROR or error code.
*/
static EC_T_DWORD EcLinkRecvFrame(
   void*           pvInstance,
   EC_T_LINK_FRAMEDESC* pLinkFrameDesc      /* [in]  link frame descriptor */
   )
{
   EC_T_DWORD            dwRetVal = EC_E_ERROR;
   PT_CPSW_INTERNAL      pAdapter = (PT_CPSW_INTERNAL)pvInstance;

#if defined DEBUG
   /* check for Type Signature */
   if (!IsCorrectAdapter(pAdapter) || EC_NULL == pLinkFrameDesc)
   {
      ERR("EcLinkRecvFrame: Invalid parameter");
      dwRetVal = EC_E_INVALIDPARM;
      goto Exit;
   }
#endif

   // Needed by upper layer
   pLinkFrameDesc->pbyFrame = NULL;
   pLinkFrameDesc->dwSize = 0;
   
   dwRetVal = EcLinkRecvFrameFromDma(pAdapter, pLinkFrameDesc);
   if (EC_E_NOERROR != dwRetVal)
   {
      goto Exit;
   }

   // copy data from DMA buffer to rx buffer
   if (pAdapter->oInitParms.bNotUseDmaBuffers && pLinkFrameDesc->pbyFrame && pLinkFrameDesc->dwSize)
   {
      EC_T_BYTE* pbyFrame = EC_NULL;

      dwRetVal = pAdapter->rxBuffers.Alloc(&pbyFrame);
      if (EC_E_NOERROR != dwRetVal)
      {
         FreeRecvFrameFromDma(pAdapter, pLinkFrameDesc);
         pLinkFrameDesc->dwSize = 0;
         pLinkFrameDesc->pbyFrame = EC_NULL;
         goto Exit;
      }

      // copy data and return DMA buffer
      LinkOsMemcpy(pbyFrame, pLinkFrameDesc->pbyFrame, pLinkFrameDesc->dwSize);
      FreeRecvFrameFromDma(pAdapter, pLinkFrameDesc);

      pLinkFrameDesc->pbyFrame = pbyFrame;
   }

   dwRetVal = EC_E_NOERROR;

Exit:

   return dwRetVal;
}

/********************************************************************************/
/** \brief Allocate a frame buffer used for send
*
*    If the link layer doesn't support frame allocation, this function must return
*    EC_E_NOTSUPPORTED
*
* \return EC_E_NOERROR or error code.
*/
static EC_T_DWORD EcLinkAllocSendFrameFromDma(
                                       PT_CPSW_INTERNAL     pAdapter,
                                       EC_T_LINK_FRAMEDESC* pLinkFrameDesc,     /* [in/out]  link frame descriptor */
                                       EC_T_DWORD           dwSize              /* [in]      size of the frame to allocate */
                                       )
{
   EC_T_DWORD            dwRetVal    = EC_E_ERROR;
   T_CPSW_FRAMEBUFENTRY* pBufEntry = EC_NULL;

   // Allocate TX buffer
   pBufEntry = CPSWAllocTxBuffer(pAdapter);
   if (pBufEntry == EC_NULL) // No free buffer found?
   {
      ERR("Can't alloc TX buffer (No free buffer)\n");
      dwRetVal = EC_E_NOMEMORY;
      goto Exit;
   }

   pBufEntry->bufentry.dwFrameLen = dwSize;

   pLinkFrameDesc->pbyFrame = pBufEntry->pFrame;
   pLinkFrameDesc->dwSize = dwSize;
   
   dwRetVal = EC_E_NOERROR;

Exit:
   return dwRetVal;
}

/********************************************************************************/
/** \brief Allocate a frame buffer used for send
*
*    If the link layer doesn't support frame allocation, this function must return
*    EC_E_NOTSUPPORTED
*
* \return EC_E_NOERROR or error code.
*/
static EC_T_DWORD EcLinkAllocSendFrame(
   void*           pvInstance,
   EC_T_LINK_FRAMEDESC* pLinkFrameDesc,     /* [in/out]  link frame descriptor */
   EC_T_DWORD           dwSize              /* [in]      size of the frame to allocate */
   )
{
   EC_T_DWORD            dwRetVal = EC_E_ERROR;
   PT_CPSW_INTERNAL      pAdapter = (PT_CPSW_INTERNAL)pvInstance;

#if defined DEBUG
   if (!IsCorrectAdapter(pAdapter) || EC_NULL == pLinkFrameDesc)
   {
      ERR("Can't alloc TX buffer (Invalid param)\n");
      dwRetVal = EC_E_INVALIDPARM;
      goto Exit;
   }
#endif

   // Needed by upper layer
   pLinkFrameDesc->pbyFrame = NULL;
   pLinkFrameDesc->dwSize = 0;

   // license check
   if (0x4154 == dwSize)
   {
      EC_T_BYTE* pDest;

      if (pLinkFrameDesc->pdwTimeStampLo)
      {
         pDest = (EC_T_BYTE*)pLinkFrameDesc->pdwTimeStampLo;
         *pDest++ = pAdapter->abyStationAddress[0];
         *pDest++ = pAdapter->abyStationAddress[1];
         *pDest++ = pAdapter->abyStationAddress[2];
         *pDest++ = pAdapter->abyStationAddress[3];
      }
      if (pLinkFrameDesc->pdwTimeStampPostLo)
      {
         pDest = (EC_T_BYTE*)pLinkFrameDesc->pdwTimeStampPostLo;
         *pDest++ = pAdapter->abyStationAddress[4];
         *pDest++ = pAdapter->abyStationAddress[5];
         *pDest++ = 0;
         *pDest++ = 0;
      }
      dwRetVal = EC_E_NOERROR;
      goto Exit;
   }

   if (pAdapter->oInitParms.bNotUseDmaBuffers)
   {
      dwRetVal = EC_E_NOTSUPPORTED;
      goto Exit;
   }

   dwRetVal = EcLinkAllocSendFrameFromDma(pAdapter, pLinkFrameDesc, dwSize);

Exit:

   return dwRetVal;
}

/********************************************************************************/
/** \brief Release a frame buffer previously allocated with EcLinkAllocFrame()
*
* \return EC_E_NOERROR or error code.
*/
static void  EcLinkFreeSendFrame(
                                      void*           pvInstance,
                                      EC_T_LINK_FRAMEDESC* pLinkFrameDesc      /* [in]  link frame descriptor */
                                      )
{
   EC_UNREFPARM(pvInstance);

   if (!pLinkFrameDesc || !pLinkFrameDesc->pbyFrame)
   {
      return; // wrong parameter
   }

   T_CPSW_BUFENTRY* pBufEntry = (T_CPSW_BUFENTRY *)(pLinkFrameDesc->pbyFrame - CPSW_BUFENTRY_FRAME_OFFSET);
   CPSWFreeBuffer(pBufEntry); // Free buffer from cached pool
}

/********************************************************************************/
/** \brief Release a frame buffer given to the ethercat master through the receive
*          callback function
*
* \return EC_E_NOERROR or error code.
*/
static void  EcLinkFreeRecvFrame(
                                      void*           pvInstance,
                                      EC_T_LINK_FRAMEDESC* pLinkFrameDesc      /* [in]  link frame descriptor */
                                      )
{
   PT_CPSW_INTERNAL      pAdapter    = (PT_CPSW_INTERNAL) pvInstance;

#if defined DEBUG
   if (!IsCorrectAdapter(pAdapter) || pLinkFrameDesc == EC_NULL || pLinkFrameDesc->pbyFrame == EC_NULL)
   {
      return;
   }
#endif

   if (pAdapter->oInitParms.bNotUseDmaBuffers)
   {
      pAdapter->rxBuffers.FreeBuffer(pLinkFrameDesc->pbyFrame);
   }
   else
   {
      FreeRecvFrameFromDma(pAdapter, pLinkFrameDesc);
   }
}

/********************************************************************************/
/** \brief Determine link layer MAC address
*
* \return EC_E_NOERROR or error code.
*/
static EC_T_DWORD EcLinkGetEthernetAddress(
   void* pvInstance,
   EC_T_BYTE* pbyEthernetAddress )   /* [out]  Ethernet MAC address */
{
   PT_CPSW_INTERNAL     pAdapter    = (PT_CPSW_INTERNAL) pvInstance;
   EC_T_DWORD              dwRetVal    = EC_E_ERROR;

   if (!IsCorrectAdapter(pAdapter))
   {
      dwRetVal = EC_E_INVALIDPARM;
      goto Exit;
   }
   
   OsMemcpy(pbyEthernetAddress, pAdapter->abyStationAddress, MAC_ADDR_LEN);

   dwRetVal = EC_E_NOERROR;

Exit:
   return dwRetVal;
}

/********************************************************************************/
/** \brief Determine current link status.
* This routine is called in the EtherCAT main cycle. Be fast...
* 
* \return Current link status.
*/
static EC_T_LINKSTATUS EcLinkGetStatus(void* pvInstance)
{
   PT_CPSW_INTERNAL    pAdapter    = (PT_CPSW_INTERNAL) pvInstance;
   
#if defined DEBUG
   if (!IsCorrectAdapter(pAdapter))
   {
      return eLinkStatus_UNDEFINED;
   }
#endif

   if (pAdapter->dwLinkStatus & CPSW_LINKFLAG_LINKOK)
   {
      if (pAdapter->dwLinkStatus & 
            (CPSW_LINKFLAG_1000baseT_Half | CPSW_LINKFLAG_100baseT_Half | CPSW_LINKFLAG_10baseT_Half))
      {
         return eLinkStatus_HALFDUPLEX;  
      }
      return eLinkStatus_OK;
   }
   return eLinkStatus_DISCONNECTED;
}

/********************************************************************************/
/** \brief Determine link speed.
*
* \return link speed in MBit/sec
*/
static EC_T_DWORD EcLinkGetSpeed(void* pvInstance)
{
   PT_CPSW_INTERNAL     pAdapter    = (PT_CPSW_INTERNAL) pvInstance;
   EC_T_DWORD            dwSpeed     = 10;

#if defined DEBUG
   if (!IsCorrectAdapter(pAdapter))
   {
      dwSpeed = EC_E_INVALIDPARM;
      goto Exit;
   }
#endif

   if (pAdapter->dwLinkStatus & (CPSW_LINKFLAG_1000baseT_Full | CPSW_LINKFLAG_1000baseT_Half))
   {
      dwSpeed = 1000;
   }
   else if (pAdapter->dwLinkStatus & (CPSW_LINKFLAG_100baseT_Full | CPSW_LINKFLAG_100baseT_Half))
   {
      dwSpeed = 100;
   }
   goto Exit;

Exit:
   return dwSpeed;
}

/********************************************************************************/
/** \brief Determine link mode
*
* \return link mode
*/
static EC_T_LINKMODE EcLinkGetMode(void* pvInstance)
{
   PT_CPSW_INTERNAL      pAdapter    = (PT_CPSW_INTERNAL)pvInstance;
   EC_T_LINKMODE           oLink       = EcLinkMode_UNDEFINED;

#if defined DEBUG
   if (!IsCorrectAdapter(pAdapter))
   {
      goto Exit;
   }
#endif

   oLink = pAdapter->oInitParms.linkParms.eLinkMode;
   goto Exit;

Exit:
   return oLink;
}

static EC_T_BOOL IsCorrectIoctlParamInput(EC_T_LINK_IOCTLPARMS* pParms, EC_T_DWORD dwSize)
{
   if ((EC_NULL == pParms)
      || (EC_NULL == pParms->pbyInBuf)
      || (dwSize != pParms->dwInBufSize))
      return EC_FALSE;
   else
      return EC_TRUE;
}

static EC_T_BOOL IsCorrectIoctlParamOutput(EC_T_LINK_IOCTLPARMS* pParms, EC_T_DWORD dwSize)
{
   if ((EC_NULL == pParms)
      || (EC_NULL == pParms->pbyOutBuf)
      || (dwSize != pParms->dwOutBufSize))
      return EC_FALSE;
   else
      return EC_TRUE;
}

/********************************************************************************/
/** \brief Multi Purpose LinkLayer IOCTL
*
* \return EC_E_NOERROR or error code.
*/

static EC_T_DWORD      EcLinkIoctl(
                                   void*              pvInstance,
                                   EC_T_DWORD              dwCode,
                                   EC_T_LINK_IOCTLPARMS*   pParms
                                   )
{
   EC_T_DWORD        dwRetVal    = EC_E_ERROR;
   PT_CPSW_INTERNAL  pAdapter    = (PT_CPSW_INTERNAL)pvInstance;

#if defined DEBUG
   if (!IsCorrectAdapter(pAdapter))
   {
      dwRetVal = EC_E_INVALIDPARM;
      goto Exit;
   }
#endif

   /* fan out IOCTL functions */

   switch( dwCode )
   {
   case EC_LINKIOCTL_REGISTER_FRAME_CLBK:
      {
         EC_T_LINK_FRM_RECV_CLBK* pFrameRecvClbk = EC_NULL;

         if (!IsCorrectIoctlParamInput(pParms, sizeof(EC_T_LINK_FRM_RECV_CLBK)))
         {
            dwRetVal = EC_E_INVALIDPARM;
            goto Exit;
         }

         pFrameRecvClbk = (EC_T_LINK_FRM_RECV_CLBK*)pParms->pbyInBuf;

         pAdapter->pvCallbackContext = pFrameRecvClbk->pvDevice;
         pAdapter->pfReceiveFrameCallback = (EC_T_RECEIVEFRAMECALLBACK)pFrameRecvClbk->pfFrameReceiveCallback;
         dwRetVal = EC_E_NOERROR;
      } break;
   case EC_LINKIOCTL_UNREGISTER_FRAME_CLBK:
      {
         pAdapter->pvCallbackContext = EC_NULL;
         pAdapter->pfReceiveFrameCallback = EC_NULL;
         dwRetVal = EC_E_NOERROR;
      } break;
   case EC_LINKIOCTL_INTERRUPTENABLE:
      {
         if (!IsCorrectIoctlParamInput(pParms, sizeof(EC_T_BOOL)))
         {
            dwRetVal = EC_E_INVALIDPARM;
            goto Exit;
         }

         if( EC_TRUE == ((EC_T_BOOL)(pParms->pbyInBuf)) )
         {
            LinkOsInterruptDisable( &(pAdapter->oIrqParms) );

            if( !LinkOsInterruptInitialize(&(pAdapter->oIrqParms)) )
            {
               SYS_ERRORMSGC(("CPSW-EcLinkOpen(): Cannot Initialize interrupt\n"));
               dwRetVal = EC_E_ERROR;
               goto Exit;
            }
         }
         else
         {
            LinkOsInterruptDisable( &(pAdapter->oIrqParms) );
         }
      } break;
   case EC_LINKIOCTL_PROMISCOUSMODE:
      {
      } break;
   case EC_LINKIOCTL_SETLINKMODE:
      {
         if (!IsCorrectIoctlParamInput(pParms, sizeof(EC_T_LINKSTATUS)))
         {
            dwRetVal = EC_E_INVALIDPARM;
            goto Exit;
         }

         switch( *((EC_T_LINKMODE*)pParms->pbyInBuf) )
         {
         case EcLinkMode_INTERRUPT:
            {
               if (EcLinkMode_INTERRUPT != pAdapter->oInitParms.linkParms.eLinkMode)
               {
                  LinkOsInterruptDisable( &(pAdapter->oIrqParms) );

                  if( !LinkOsInterruptInitialize(&(pAdapter->oIrqParms)) )
                  {
                     SYS_ERRORMSGC(("CPSW-EcLinkOpen(): Cannot Initialize interrupt\n"));
                     dwRetVal = EC_E_ERROR;
                     goto Exit;
                  }
               }
               break;
            }
         case EcLinkMode_POLLING:
            {
               if (EcLinkMode_POLLING != pAdapter->oInitParms.linkParms.eLinkMode)
               {
                  LinkOsInterruptDisable( &(pAdapter->oIrqParms) );
               }
               break;
            }
         default:
            {
               dwRetVal = EC_E_INVALIDPARM;
               goto Exit;
            }
         }

         pAdapter->oInitParms.linkParms.eLinkMode = *((EC_T_LINKMODE*)pParms->pbyInBuf);

         break;
      }
   case EC_LINKIOCTL_GETLINKMODE:
      {
         if (!IsCorrectIoctlParamOutput(pParms, sizeof(EC_T_LINKSTATUS)))
         {
            dwRetVal = EC_E_INVALIDPARM;
            goto Exit;
         }
         EC_T_LINKMODE *pMode = (EC_T_LINKMODE*)pParms->pbyOutBuf;
         *pMode = pAdapter->oInitParms.linkParms.eLinkMode;

         break;
      }
   case EC_LINKIOCTL_UPDATE_LINKSTATUS:
      {
         EC_T_DWORD dwOldLinkStatus = pAdapter->dwLinkStatus;
         pAdapter->dwLinkStatus = CPSWPhyUpdateLinkStatus(pAdapter);
         
         // Reconfigure MAC if PHY speed has changed
         if (dwOldLinkStatus != pAdapter->dwLinkStatus)
         {
            CPSWUpdateMacSpeed(pAdapter);
         }
      } break;
   case EC_LINKIOCTL_GET_ETHERNET_ADDRESS:
      {
         if (!IsCorrectIoctlParamOutput(pParms, 6))
         {
            dwRetVal = EC_E_INVALIDPARM;
            goto Exit;
         }
            
         dwRetVal = EcLinkGetEthernetAddress(pAdapter, pParms->pbyOutBuf); 
      } break;   
   case EC_LINKIOCTL_IS_FRAMETYPE_REQUIRED:
      {
         if (!IsCorrectIoctlParamOutput(pParms, sizeof(EC_T_BOOL)))
         {
            dwRetVal = EC_E_INVALIDPARM;
            goto Exit;
         }
            
         EC_T_BOOL* pbRequired = (EC_T_BOOL*)pParms->pbyOutBuf;
         *pbRequired = EC_TRUE;
      } break;   
   case EC_LINKIOCTL_IS_FLUSHFRAMES_REQUIRED:
      {
         if (!IsCorrectIoctlParamOutput(pParms, sizeof(EC_T_BOOL)))
         {
            dwRetVal = EC_E_INVALIDPARM;
            goto Exit;
         }
            
         EC_T_BOOL* pbRequired = (EC_T_BOOL*)pParms->pbyOutBuf;
         *pbRequired = EC_TRUE;
      } break;   
   case EC_LINKIOCTL_FLUSHFRAMES:
      {
         if (!pAdapter->bGatherFrames)
         {
            dwRetVal = EC_E_INVALIDSTATE;
            goto Exit;
         }

         /* send frames to DMA if we have something */
         if (pAdapter->pTxHeadDesc)
         {
            if (EC_E_NOERROR != (dwRetVal = CPSWWaitUntilTxDmaIsEmpty(pAdapter)))
               goto Exit;

            EC_T_DWORD txDescPa = dmadtor_va2pa(pAdapter, (EC_T_DWORD)pAdapter->pTxHeadDesc);
            W32(pAdapter->regs.StateRam->Tx_HDP[pAdapter->dwTxDmaChan], txDescPa);
         }
         pAdapter->bGatherFrames = EC_FALSE;
         pAdapter->pTxHeadDesc = EC_NULL;
         pAdapter->pTxTailDesc = EC_NULL;

      } break;   
   case EC_LINKIOCTL_SENDCYCLICFRAMES:
      {
         /* cannot start sending if previous if not finished yet */
         if (pAdapter->bGatherFrames)
         {
            dwRetVal = EC_E_INVALIDSTATE;
            goto Exit;
         }
         pAdapter->bGatherFrames = EC_TRUE;
         pAdapter->dwTxDmaChan = pAdapter->dwTxDmaChanCyc;
      } break;   
   case EC_LINKIOCTL_SENDACYCLICFRAMES:
   {
      /* cannot start sending if previous if not finished yet */
      if (pAdapter->bGatherFrames)
      {
         dwRetVal = EC_E_INVALIDSTATE;
         goto Exit;
      }
      pAdapter->bGatherFrames = EC_TRUE;
      pAdapter->dwTxDmaChan = pAdapter->dwTxDmaChanAcyc;
   } break;
   default:
      {
         dwRetVal = EC_E_INVALIDCMD;
         goto Exit;
      }
   }

   /* no error */

   dwRetVal = EC_E_NOERROR;

Exit:
   if( EC_E_NOERROR != dwRetVal )
   {
#if (defined __RCX__)
      if( bAllocIrq )
      {
         static RX_RESULT rxRes = RX_OK;
         rxRes = rX_MemFreeMemory(pAdapter->hInterrupt);
         OsDbgAssert(rxRes == RX_OK);
      }
#else
      ;;
#endif
   }
   return dwRetVal;
}

/********************************************************************************/
/** \brief Register link layer driver.
*
* \return EC_E_NOERROR or error code.
*/
CCALLING EC_T_DWORD emllRegisterCPSW(
   EC_T_LINK_DRV_DESC*  pLinkDrvDesc        /* [in,out] link layer driver descriptor */
   ,EC_T_DWORD           dwLinkDrvDescSize   /* [in]     size in bytes of link layer driver descriptor */
   )
{
   EC_T_DWORD dwResult = EC_E_NOERROR;
   EC_T_BOOL bInterfaceSupported = EC_FALSE;

   if (pLinkDrvDesc == EC_NULL)
   {
      ERR("emllRegister: NULL drive descriptor \n");
      dwResult = EC_E_INVALIDPARM;
      goto Exit;
   }

   if (pLinkDrvDesc->dwValidationPattern != LINK_LAYER_DRV_DESC_PATTERN)
   {
      ERR("emllRegister: invalid descriptor pattern 0x%x instead of 0x%x\n",
         pLinkDrvDesc->dwValidationPattern, LINK_LAYER_DRV_DESC_PATTERN);
      dwResult = EC_E_INVALIDPARM;
      goto Exit;
   }

   /* Check the size of the given link layer driver descriptor */
   if (dwLinkDrvDescSize > sizeof(EC_T_LINK_DRV_DESC))
   {
      ERR("emllRegister: link layer driver descriptor size too large\n");
      dwResult = EC_E_INVALIDPARM;
      goto Exit;
   }

   bInterfaceSupported = EC_FALSE;

   /* Check if the version of the interface is supported  */
   if (   (sizeof(EC_T_LINK_DRV_DESC) == dwLinkDrvDescSize)
      && (pLinkDrvDesc->dwInterfaceVersion == LINK_LAYER_DRV_DESC_VERSION) )
   {
      bInterfaceSupported = EC_TRUE;
   }

   /* Interface is not supported. */
   if (! bInterfaceSupported)
   {
      ERR("emllRegister: invalid descriptor interface version 0x%x instead of 0x%x\n",
         pLinkDrvDesc->dwInterfaceVersion, LINK_LAYER_DRV_DESC_VERSION);
      dwResult = EC_E_INVALIDPARM;
      goto Exit;
   }

   pLinkDrvDesc->pfEcLinkOpen = EcLinkOpen;
   pLinkDrvDesc->pfEcLinkClose = EcLinkClose;
   pLinkDrvDesc->pfEcLinkSendFrame = EcLinkSendFrame;
   pLinkDrvDesc->pfEcLinkSendAndFreeFrame = EcLinkSendAndFreeFrame;
   pLinkDrvDesc->pfEcLinkRecvFrame = EcLinkRecvFrame;
   pLinkDrvDesc->pfEcLinkAllocSendFrame = EcLinkAllocSendFrame;
   pLinkDrvDesc->pfEcLinkFreeSendFrame = EcLinkFreeSendFrame;
   pLinkDrvDesc->pfEcLinkFreeRecvFrame = EcLinkFreeRecvFrame;
   pLinkDrvDesc->pfEcLinkGetEthernetAddress = EcLinkGetEthernetAddress;
   pLinkDrvDesc->pfEcLinkGetStatus = EcLinkGetStatus;
   pLinkDrvDesc->pfEcLinkGetSpeed = EcLinkGetSpeed;
   pLinkDrvDesc->pfEcLinkGetMode = EcLinkGetMode;
   pLinkDrvDesc->pfEcLinkIoctl = EcLinkIoctl;

   /* store DBG Message hook */
   LinkOsAddDbgMsgHook(pLinkDrvDesc->pfOsDbgMsgHook);

Exit:

   return dwResult;
}

/***************************************************************************************************/
/**
\brief  Acknowledge all pending NIC IRQ'S.

\return EC_E_NOERROR on success, error code otherwise.
*/
static EC_T_DWORD EcLinkIrqAck(void* pvContext)
{
   EC_UNREFPARM(pvContext);
   return EC_E_NOERROR;
}

/***************************************************************************************************/
/**
\brief  Mask NIC IRQ's.

\return EC_E_NOERROR on success, error code otherwise.
*/
static EC_T_DWORD EcLinkIrqClose(void* pvContext)
{
   EC_UNREFPARM(pvContext);
   return EC_E_NOERROR;
}

/***************************************************************************************************/
/**
\brief  Re-Open NIC IRQ's (UnMask).

\return EC_E_NOERROR on success, error code otherwise.
*/
static EC_T_DWORD EcLinkIrqOpen(void* pvContext)
{
   EC_UNREFPARM(pvContext);
   return EC_E_NOERROR;
}

/***************************************************************************************************/
/**
\brief  Handle Chip Interrupts.

\return EC_E_NOERROR on success, error code otherwise.
*/
static EC_T_DWORD DoHandleInterrupt(
                                    void * pvLinkParms
                                    )
{
   EC_UNREFPARM(pvLinkParms);
   return EC_E_NOERROR;
}


/***************************************************************************************************
*                                 List Management
*/


static EC_T_BOOL ListAddOI(PT_CPSW_INTERNAL poAdapter)
{
   EC_T_BOOL           bResult = EC_TRUE;
   PT_CPSW_INTERNAL oCur    = EC_NULL;

   oCur = S_oOpenInstanceRoot;

   if( EC_NULL == oCur )
   {
      S_oOpenInstanceRoot = poAdapter;
      S_oOpenInstanceRoot->pPrev = S_oOpenInstanceRoot->pNext = EC_NULL;
   }
   else
   {
      while( EC_NULL != oCur->pNext )
      {
         oCur = oCur->pNext;
      }

      oCur->pNext = poAdapter;
      poAdapter->pPrev = oCur;
      poAdapter->pNext = EC_NULL;
   }

   return bResult;
}

static EC_T_BOOL ListRmOI(PT_CPSW_INTERNAL poAdapter)
{
   EC_T_BOOL       bResult = EC_TRUE;

   if( S_oOpenInstanceRoot == poAdapter )
   {
      S_oOpenInstanceRoot = poAdapter->pNext;
   }

   if( EC_NULL != poAdapter->pPrev )
   {
      poAdapter->pPrev->pNext = poAdapter->pNext;
   }

   if( EC_NULL != poAdapter->pNext )
   {
      poAdapter->pNext->pPrev = poAdapter->pPrev;
   }

   poAdapter->pPrev = EC_NULL;
   poAdapter->pNext = EC_NULL;

   return bResult;
}

static EC_T_BOOL ListCanAddAdapter(PT_CPSW_INTERNAL pAdapter)
{
   PT_CPSW_INTERNAL  oCur = EC_NULL;
   EC_T_BOOL bCanAddAdapter = EC_TRUE;

   oCur = S_oOpenInstanceRoot;

   while (EC_NULL != oCur)
   {
	  /* port and DmaChan could not be identical */
      if ( (pAdapter->oInitParms.linkParms.dwInstance == oCur->oInitParms.linkParms.dwInstance)
    	|| (pAdapter->dwRxDmaChan == oCur->dwRxDmaChan))
      {
    	  bCanAddAdapter = EC_FALSE;
    	  break;
      }
      oCur = oCur->pNext;
   }

   return bCanAddAdapter;
}


/***************************************************************************************************
*                                 Helpers
*/


static EC_T_DWORD CPSWMapMemory(PT_CPSW_INTERNAL pAdapter, EC_T_DWORD dwBase, EC_T_DWORD *pdwBaseMapped)
{
   EC_UNREFPARM(pAdapter);
   *pdwBaseMapped = (EC_T_DWORD)LinkOsMapMemory((EC_T_BYTE *)dwBase, CPSW_IO_LENGTH);

   if (*pdwBaseMapped == 0)
   {
     SYS_ERRORMSGC(("Error: CPSWMapMemory() LinkOsMapMemory can't mmap IO memory!\n"));
     return EC_E_NOMEMORY;
   }
  
   return EC_E_NOERROR;
}

static EC_T_BOOL CPSWSoftReset(volatile EC_T_DWORD *reg) 
{
   int timeout = 1000; // 10ms

   w32(reg, CPSW3G_SOFT_RESET_BIT);

   do
   {
      LinkOsSysDelay(10);
   }
   while (((r32(reg) & CPSW3G_SOFT_RESET_BIT) != 0) && timeout--);

   if ((r32(reg) & CPSW3G_SOFT_RESET_BIT) != 0)
   {
      return EC_FALSE;
   }
   return EC_TRUE;
}

static void CPSWReset(PT_CPSW_INTERNAL pAdapter)
{
   PCPSW_REGSET pReg = &pAdapter->regs;
   
   // (5) Apply Soft Reset to 3PSW Subsystem, CPSW_3G, CPGMAC_SL 1/2 and CPDMA
   // \sa 9.2.1.13 Software Reset

   if (!CPSWSoftReset(&pReg->Dma->CPDMA_Soft_Reset))
   {
      ERR("Reset (CPDMA) failed.\n");
      return;
   }
  
   if (!CPSWSoftReset(&pReg->Cpsw->CPSW_Soft_Reset))
   {
      ERR("Reset (CPSW_3G) failed.\n");
      return;
   }
   
   if (!CPSWSoftReset(&pReg->Ss->Soft_Reset))
   {
      ERR("Reset (CPSW_3GSS) failed.\n");
      return;
   }

   // (6) Initialize the HDPs (Header Description Pointer) and CPs (Completion Pointer) to NULL
   for (int i = 0; i < CPDMA_MAX_CHANNELS; i++)
   {
      W32(pReg->StateRam->Tx_HDP[i], 0);
      W32(pReg->StateRam->Rx_HDP[i], 0);
      W32(pReg->StateRam->Tx_CP[i], 0);
      W32(pReg->StateRam->Rx_CP[i], 0);
   }
}

static void CPSWTeardown(PT_CPSW_INTERNAL pAdapter)
{
   PCPSW_REGSET regs = &pAdapter->regs;
   int timeout = 500; // 5ms
   EC_T_BOOL bDmaEnabled = EC_FALSE;

   if (R32(regs->StateRam->Rx_HDP[pAdapter->dwRxDmaChan]) != 0)
   {
      bDmaEnabled = EC_TRUE;
      INF("Disable running RX-DMA\n");
   }

   if (R32(regs->StateRam->Tx_HDP[pAdapter->dwTxDmaChan]) != 0)
   {
      bDmaEnabled = EC_TRUE;
      INF("Disable running TX-DMA\n");
   }

   if (! bDmaEnabled) return;

   W32(regs->Dma->Rx_Teardown, pAdapter->dwRxDmaChan); // Shutdown RX DMA
   W32(regs->Dma->Tx_Teardown, pAdapter->dwTxDmaChan); // Shutdown TX DMA
 
   while (   R32(regs->StateRam->Rx_HDP[pAdapter->dwRxDmaChan]) != 0 
          && R32(regs->StateRam->Tx_HDP[pAdapter->dwTxDmaChan]) != 0
          && timeout--)
   {
      LinkOsSysDelay(10);
   }

   if (timeout < 0)
   {
      ERR("DMA teardown timeout\n");
      return;
   }

   W32(regs->StateRam->Rx_CP[pAdapter->dwRxDmaChan], 0);
   W32(regs->StateRam->Tx_CP[pAdapter->dwTxDmaChan], 0);

   INF("RX+TX DMA disabled. Delay %d\n", timeout);
}

static inline void CPSWSetMacAddress(volatile CPSW_SLAVE_REGS* pSlaveReg, EC_T_BYTE macAddr[6])
{
   W32(pSlaveReg->SL_SA_LO, (macAddr[5] << 8) | macAddr[4]);
   W32(pSlaveReg->SL_SA_HI, (macAddr[3] << 24) | (macAddr[2] << 16) |
     (macAddr[1] << 8) | macAddr[0]);
}

/***************************************************************************************************
*                                 MDIO
*/

static void CPSWMdioInit(PT_CPSW_INTERNAL pAdapter)
{
   volatile PCPSW_MDIO_REGS pMdio = pAdapter->regs.Mdio;

   DBG("MDIO Version 0x%08x\n", R32(pMdio->Version));

   if (pAdapter->oInitParms.bMaster)
   {
      // 1. Configure the PREAMBLE and CLKDIV bits in the MDIO Control register (CONTROL).
      // 2. Enable the MDIO module by setting the ENABLE bit in CONTROL.
      W32(pMdio->Control, MDIO_ENABLE | 0xFF);
   }

   // 3. The MDIO PHY alive status register (ALIVE) can be read in polling fashion until a PHY connected to
   // the system responded, and the MDIO PHY link status register (LINK) can determine whether this PHY
   // already has a link.
   // N/A

   // 4. Setup the appropriate PHY addresses in the MDIO user PHY select register (USERPHYSELn), and set
   // the LINKINTENB bit to enable a link change event interrupt if desirable.
   W32(pMdio->Useraccess[CPSWPortIdx(pAdapter)].physel, pAdapter->oInitParms.dwPhyAddr);

   // 5. If an interrupt on general MDIO register access is desired, set the corresponding bit in the MDIO user
   // command complete interrupt mask set register (USERINTMASKSET) to use the MDIO user access
   // register (USERACCESSn). Since only one PHY is used in this device, the application software can use
   // one USERACCESSn to trigger a completion interrupt; the other USERACCESSn is not setup.
   // N/A
}


static EC_T_BOOL MdioWaitForAccessComplete(volatile EC_T_DWORD *pAccess)
{
   int timeout = 100;

   while ((r32(pAccess) & MDIO_GO) && timeout--)
   {
      LinkOsSysDelay(10);
   }
   
   if (timeout < 0)
   {
      ERR("mdio wait timeout\n");
      return EC_FALSE;
   }

   return EC_TRUE;
}

/* \brief MDIO read
 * 
 * \param dwPhyAddr 5-bit PHY address
 * \param dwRegNo 5-bit PHY register field to read
 * \return Read PHY register value or MDIO_ERROR on error
 */
static EC_T_DWORD CPSWMdioRead(PT_CPSW_INTERNAL pAdapter, EC_T_DWORD dwRegNo)
{
   EC_T_DWORD dwRet = MDIO_ERROR;
   volatile PCPSW_MDIO_REGS pMdio = pAdapter->regs.Mdio;
   EC_T_DWORD dwPortIdx = CPSWPortIdx(pAdapter);
   volatile PCPSW_MDIO_USR_REGS pMdioPort = &pMdio->Useraccess[dwPortIdx];
   EC_T_DWORD dwPhyAddr = pAdapter->oInitParms.dwPhyAddr & 0x1F;
   
   // 1. Check to ensure that the GO bit in the MDIO user access register (USERACCESSn) is cleared.
   if (!MdioWaitForAccessComplete(&pMdioPort->access)) goto Exit;

   // 2. Write to the GO, REGADR, and PHYADR bits in USERACCESSn corresponding to the PHY and PHY
   // register you want to read.
   W32(pMdioPort->access,
      MDIO_GO | MDIO_READ | 
      ((dwRegNo & 0x1F) << 21) |
      ((dwPhyAddr & 0x1F) << 16));

   // 3. The read data value is available in the DATA bits in USERACCESSn after the module completes the
   // read operation on the serial bus. Completion of the read operation can be determined by polling the
   // GO and ACK bits in USERACCESSn. After the GO bit has cleared, the ACK bit is set on a successful
   // read.
   if (! MdioWaitForAccessComplete(&pMdioPort->access)) goto Exit;
   if (! (R32(pMdioPort->access) & MDIO_ACK))
   {
      ERR("mdio ACK missing\n");
      goto Exit;
   }

   dwRet = R32(pMdioPort->access) & 0xFFFF;

   // 4. Completion of the operation sets the corresponding USERINTRAW bit (0 or 1) in the MDIO user
   // command complete interrupt register (USERINTRAW) corresponding to USERACCESSn used. If
   // interrupts have been enabled on this bit using the MDIO user command complete interrupt mask set
   // register (USERINTMASKSET), then the bit is also set in the MDIO user command complete interrupt
   // register (USERINTMASKED) and an interrupt is triggered on the CPU.
   // N/A

Exit:

   if (dwRet == MDIO_ERROR)
   {
      INF("mdio reg %d read error\n", dwRegNo);
   }

   return dwRet;
}

/* \brief MDIO write
 * 
 * \param dwPhyAddr 5-bit PHY address
 * \param dwRegNo 5-bit PHY register field to write
 * \param dwValue Value to write to the PHY register
 * \return EC_TRUE on success or EC_FALSE on error 
 */
static EC_T_BOOL CPSWMdioWrite(PT_CPSW_INTERNAL pAdapter, EC_T_DWORD dwRegNo, EC_T_DWORD dwValue)
{
   EC_T_BOOL bRet = EC_FALSE;
   volatile PCPSW_MDIO_REGS pMdio = pAdapter->regs.Mdio;
   EC_T_DWORD dwPortIdx = CPSWPortIdx(pAdapter);
   volatile PCPSW_MDIO_USR_REGS pMdioPort = &pMdio->Useraccess[dwPortIdx];
   EC_T_DWORD dwPhyAddr = pAdapter->oInitParms.dwPhyAddr & 0x1F;
   
   // 1. Check to ensure that the GO bit in the MDIO user access register (USERACCESSn) is cleared.
   if (!MdioWaitForAccessComplete(&pMdioPort->access)) goto Exit;

   // 2. Write to the GO, WRITE, REGADR, PHYADR, and DATA bits in USERACCESSn corresponding to the
   // PHY and PHY register you want to write.
   W32(pMdioPort->access,
      MDIO_GO | MDIO_WRITE | 
      ((dwRegNo & 0x1F) << 21) |
      ((dwPhyAddr & 0x1F) << 16) |
      (dwValue & 0xFFFF));

   // 3. The write operation to the PHY is scheduled and completed by the MDIO module. Completion of the
   // write operation can be determined by polling the GO bit in USERACCESS n for a 0.
   if (! MdioWaitForAccessComplete(&pMdioPort->access)) goto Exit;
  
   bRet = EC_TRUE;
   
Exit:
   return bRet;
}

/***************************************************************************************************
*                                 PHY & Co
*/

static EC_T_BOOL CPSWPhySetup(PT_CPSW_INTERNAL pAdapter)
{
   EC_T_BOOL bRet = EC_FALSE;
   EC_T_DWORD tmp;
   EC_T_DWORD dwPhyId = 0;
   int sleepTick = 50; // ms
   int timeout = 6000 / sleepTick; // 6s
   
   if ((tmp = CPSWMdioRead(pAdapter, MII_PHYID1)) == MDIO_ERROR) goto Exit;
   dwPhyId = tmp;
   if ((tmp = CPSWMdioRead(pAdapter, MII_PHYID2)) == MDIO_ERROR) goto Exit;
   dwPhyId = (dwPhyId << 16) | tmp;
   
   if ((dwPhyId == 0xFFFFFFFF) || (dwPhyId == 0))
   {
      ERR("No PHY found at MII addr %d\n", pAdapter->oInitParms.dwPhyAddr);
      goto Exit;
   }
   
   INF("PHY found. Id=0x%08x\n", dwPhyId);
   
   if (!CPSWPhyQueryCapabilities(pAdapter))
   {
      ERR("Query PHY capabilities failed\n");
      goto Exit;
   }

   if (pAdapter->oInitParms.bPhyRestartAutoNegotiation)
   {
      INF("Restart PHY auto negotiation\n");

      // Restart auto negotiation
      if ((tmp = CPSWMdioRead(pAdapter, MII_CR)) == MDIO_ERROR) goto Exit;
      tmp |= MII_CR_ANRESTART | MII_CR_ANENABLE;
      tmp &= ~0x0800;
      if (!CPSWMdioWrite(pAdapter, MII_CR, tmp)) goto Exit;
      
      // Poll for auto negotiation completion
      // Read TBI SR to ensure SR[AN Done]=1 and SR[Link Status]=1.
      do
      {
         if ((tmp = CPSWMdioRead(pAdapter, MII_SR)) == MDIO_ERROR) goto Exit;
         LinkOsSleep(sleepTick);
      } while ( ((tmp & MII_SR_LINKOKVAL) != MII_SR_LINKOKVAL) && timeout-- );
      
      if (timeout < 0)
      {
         // No error. Ethernet cable may not be connected.
         INF("PHY link timeout (waiting for auto negotiation completion).\n");
         bRet = EC_TRUE;
         goto Exit;
      }

      if (ATH8031_PHY_ID == dwPhyId && eCpswPhySource_RGMII == pAdapter->oInitParms.ePhyConnection)
      {
         /* special initialization for ATH8031 PHY required */
         CPSWMdioWrite(pAdapter, AT803X_DEBUG_ADDR, 0x00000005);
         CPSWMdioWrite(pAdapter, AT803X_DEBUG_DATA, 0x00000100);
      }

      INF("PHY auto negotiation completed\n");
   }

   bRet = EC_TRUE;

Exit:
   return bRet;
}

static EC_T_DWORD CPSWPhyUpdateLinkStatus(PT_CPSW_INTERNAL pAdapter)
{
   EC_T_DWORD dwStat = 0;
   EC_T_DWORD dwLpa1000 = 0;
   EC_T_DWORD dwAdv1000 = 0;
   EC_T_DWORD dwLpa = 0;
   EC_T_DWORD dwAdv = 0;
   EC_T_DWORD dwLinkStatus = 0;

   if ((dwStat = CPSWMdioRead(pAdapter, MII_SR)) == MDIO_ERROR) goto Exit;
   if (dwStat & MII_SR_LSTATUS)  
   {
      dwLinkStatus |= CPSW_LINKFLAG_LINKOK;
   }
   
   if (pAdapter->dwPhyFeatures & (CPSW_LINKFLAG_1000baseT_Half | CPSW_LINKFLAG_1000baseT_Full))
   {
      // Link partner ability (advertised by remote PHY)
      if ((dwLpa1000 = CPSWMdioRead(pAdapter, MII_STAT1000)) == MDIO_ERROR) goto Exit;

      // Local ability (advertised by local PHY)
      if ((dwAdv1000 = CPSWMdioRead(pAdapter, MII_CTRL1000)) == MDIO_ERROR) goto Exit;

      // If local and remote abilities match, then this is the current line speed.
      dwLpa1000 &= dwAdv1000 << 2;
   }

   // Link partner ability (advertised by remote PHY)
   if ((dwLpa = CPSWMdioRead(pAdapter, MII_ANLPBPA)) == MDIO_ERROR) goto Exit;

   // Local ability (advertised by local PHY)
   if ((dwAdv = CPSWMdioRead(pAdapter, MII_ANA)) == MDIO_ERROR) goto Exit;

   // If local and remote abilities match, then this is the current line speed.
   dwLpa &= dwAdv;

   if (dwLpa1000 & MII_STAT1000_1000FDX)
   {
      dwLinkStatus |= CPSW_LINKFLAG_1000baseT_Full;
   }
   else if (dwLpa1000 & MII_STAT1000_1000HDX)
   {
      dwLinkStatus |= CPSW_LINKFLAG_1000baseT_Half;
   }
   else if (dwLpa & MII_ANLPBPA_100FDX)
   {
      dwLinkStatus |= CPSW_LINKFLAG_100baseT_Full;
   }
   else if (dwLpa & MII_ANLPBPA_100HDX)
   {
      dwLinkStatus |= CPSW_LINKFLAG_100baseT_Half;
   }
   else if (dwLpa & MII_ANLPBPA_10FDX)
   {
      dwLinkStatus |= CPSW_LINKFLAG_10baseT_Full;
   }
   else
   {
      dwLinkStatus |= CPSW_LINKFLAG_10baseT_Half;
   }

Exit:
   return dwLinkStatus;
}

static EC_T_BOOL CPSWPhyQueryCapabilities(PT_CPSW_INTERNAL pAdapter)
{
   EC_T_BOOL bRet = EC_FALSE;
   EC_T_DWORD dwStat;
   EC_T_DWORD dwEstat;
  
   pAdapter->dwPhyFeatures = 0;
   
   // Query if register 15 (Extended Status is available)
   if ((dwStat = CPSWMdioRead(pAdapter, MII_SR)) == MDIO_ERROR) goto Exit;
   if (dwStat & MII_SR_ESTATEN)
   {
      if ((dwEstat = CPSWMdioRead(pAdapter, MII_ESTATUS)) == MDIO_ERROR) goto Exit;

      if (dwEstat & MII_ESTATUS_1000HDX)
      {
         pAdapter->dwPhyFeatures |= CPSW_LINKFLAG_1000baseT_Half;
      }
      if (dwEstat & MII_ESTATUS_1000FDX)
      {
         pAdapter->dwPhyFeatures |= CPSW_LINKFLAG_1000baseT_Full;
      }
   }
   if (dwStat & MII_SR_100HALF)
   {
      pAdapter->dwPhyFeatures |= CPSW_LINKFLAG_100baseT_Half;
   }
   if (dwStat & MII_SR_100FULL)
   {
      pAdapter->dwPhyFeatures |= CPSW_LINKFLAG_100baseT_Full;
   }
   if (dwStat & MII_SR_10HALF)
   {
      pAdapter->dwPhyFeatures |= CPSW_LINKFLAG_10baseT_Half;
   }
   if (dwStat & MII_SR_10FULL)
   {
      pAdapter->dwPhyFeatures |= CPSW_LINKFLAG_10baseT_Full;
   }

   bRet = EC_TRUE;

Exit:
   return bRet;
}

//
// Configure MAC for P1 or P2
//
static void CPSWSetupSliver(PT_CPSW_INTERNAL pAdapter)
{
   PCPSW_REGSET regs = &pAdapter->regs;
   volatile PCPSW_SLIVER_REGS pSliverReg = regs->SelectedSliver;
   EC_T_DWORD dwPortIdx = CPSWPortIdx(pAdapter);

   if (!CPSWPhySetup(pAdapter)) 
   {
      ERR("PHY initialization failed\n");
      goto Exit;
   }
   pAdapter->dwLinkStatus = CPSWPhyUpdateLinkStatus(pAdapter);
   
#ifdef DEBUG
   DBG(CPSWID "Link status: ");
   if (pAdapter->dwLinkStatus & CPSW_LINKFLAG_LINKOK) PRINT("LINK-UP ");
   if (pAdapter->dwLinkStatus & CPSW_LINKFLAG_1000baseT_Full) PRINT("1000-FD ");
   if (pAdapter->dwLinkStatus & CPSW_LINKFLAG_1000baseT_Half) PRINT("1000-HD ");
   if (pAdapter->dwLinkStatus & CPSW_LINKFLAG_100baseT_Full) PRINT("100-FD ");
   if (pAdapter->dwLinkStatus & CPSW_LINKFLAG_100baseT_Half) PRINT("100-HD ");
   if (pAdapter->dwLinkStatus & CPSW_LINKFLAG_10baseT_Full) PRINT("10-FD ");
   if (pAdapter->dwLinkStatus & CPSW_LINKFLAG_10baseT_Half) PRINT("10-HD ");
   PRINT("\n");
#endif

   // (15) Configure CPGMAC_SL1 and CPGMAC_SL2, as per the desired mode of operations.

   if (!CPSWSoftReset(&pSliverReg->SL_Soft_Reset))
   {
      ERR("Reset (CPGMAC_SL%d) failed.\n", dwPortIdx);
      goto Exit;
   }

   // RX Packet Priority to Header Priority Mapping
   W32(pSliverReg->SL_Rx_Pri_Map, 0x76543210);

   W32(pSliverReg->SL_MacControl, 0);
   CPSWUpdateMacSpeed(pAdapter);

Exit:
   return;
}

// \sa CE driver's Cpsw3g_cpgmac_control()
static void CPSWUpdateMacSpeed(PT_CPSW_INTERNAL pAdapter)
{ 
   volatile PCPSW_SLIVER_REGS pSliverReg = pAdapter->regs.SelectedSliver;
   EC_T_DWORD dwLinkStatus = pAdapter->dwLinkStatus;

   EC_T_DWORD dwMacCtrl = R32(pSliverReg->SL_MacControl);

   dwMacCtrl &= ~CPMAC_MACCONTROL_FULLDUPLEXEN_MASK;
   if (dwLinkStatus & (CPSW_LINKFLAG_1000baseT_Full | CPSW_LINKFLAG_100baseT_Full | CPSW_LINKFLAG_10baseT_Full))
   {
      dwMacCtrl |= CPMAC_MACCONTROL_FULLDUPLEXEN_MASK;
   }

   dwMacCtrl &= ~(CPMAC_MACCONTROL_GIGABITEN_MASK);
   if (dwLinkStatus & (CPSW_LINKFLAG_1000baseT_Full | CPSW_LINKFLAG_1000baseT_Half))
   {
      dwMacCtrl |= CPMAC_MACCONTROL_GIGABITEN_MASK;
   }

   dwMacCtrl &= ~(CPMAC_MACCONTROL_MIIEN_MASK);
   if (dwLinkStatus & (CPSW_LINKFLAG_100baseT_Full | CPSW_LINKFLAG_100baseT_Half))
   {
      dwMacCtrl |= CPMAC_MACCONTROL_MIIEN_MASK;

      if ( eCpswPhySource_RGMII == pAdapter->oInitParms.ePhyConnection )
      {
         // for RGMII set those bits to indicate that speed is 100Mbps
         dwMacCtrl |= CPMAC_MACCONTROL_IFCTLB_MASK | CPMAC_MACCONTROL_IFCTLA_MASK;
      }
   }

   W32(pSliverReg->SL_MacControl, dwMacCtrl);
}

/***************************************************************************************************
*                                 ALE (Address Lookup Engine)
*/

#if !defined ALE_BYPASS
static void CPSWAleTableEntrySet(PT_CPSW_INTERNAL pAdapter, EC_T_DWORD tableIdx, EC_T_DWORD *pAleTableEntry)
{
   volatile PCPSW_ALE_REGS pReg = pAdapter->regs.Ale;

	for (int i = 0; i < ALE_ENTRY_WORDS; i++)
   {
		W32(pReg->ALE_Tbl[i], pAleTableEntry[ALE_ENTRY_WORDS - i - 1]);
   }
	W32(pReg->ALE_TblCtl, tableIdx | ALE_PORTCTL_TABLEWRITE);
}

void CPSWAleTableEntryGet(PT_CPSW_INTERNAL pAdapter, EC_T_DWORD tableIdx, EC_T_DWORD *pAleTableEntry)
{
   volatile PCPSW_ALE_REGS pReg = pAdapter->regs.Ale;

   W32(pReg->ALE_TblCtl, tableIdx);

   for (int i = 0; i < ALE_ENTRY_WORDS; i++)
   {
      pAleTableEntry[i] = R32(pReg->ALE_Tbl[ALE_ENTRY_WORDS - i - 1]);
   }
}

static void CPSWAleVlanEntrySet(
   PT_CPSW_INTERNAL pAdapter,
   EC_T_DWORD tableIdx,
   EC_T_DWORD vlanId,
   EC_T_DWORD portMask) // 0x1 | 0x2 | 0x4
{
  EC_T_DWORD ale_v_entry[ALE_ENTRY_NUM_WORDS];
  EC_T_BYTE *pAle = (EC_T_BYTE *) ale_v_entry;
  memset(ale_v_entry, 0, sizeof(ale_v_entry));

  vlanId &= 0xFFF; // 12bit
  portMask &= 0x7; // 3 bit

  // Set up the VLAN Entry
  pAle[ALE_VLAN_ENTRY_MEMBER_LIST] = (EC_T_BYTE) portMask; 
  pAle[ALE_VLAN_ENTRY_FRC_UNTAG_EGR] = (EC_T_BYTE) portMask;
  pAle[ALE_VLAN_ENTRY_ID_BIT0_BIT7] = (EC_T_BYTE) (vlanId & 0xFF);
  pAle[ALE_VLAN_ENTRY_TYPE_ID_BIT8_BIT11] = (EC_T_BYTE) (ALE_ENTRY_VLAN | (vlanId >> 8));
 
  // Set the VLAN entry in the ALE table 
  CPSWAleTableEntrySet(pAdapter, tableIdx, ale_v_entry);
}

static void CPSWAleVlanUcastEntrySet(
   PT_CPSW_INTERNAL pAdapter,
   EC_T_DWORD tableIdx,
   EC_T_DWORD portNo, // 0, 1, 2
   EC_T_DWORD vlanId,
   EC_T_BYTE ethAddress[MAC_ADDR_LEN])
{
  EC_T_DWORD ale_vu_entry[ALE_ENTRY_NUM_WORDS];
  EC_T_BYTE *pAle = (EC_T_BYTE *) ale_vu_entry;
  memset(ale_vu_entry, 0, sizeof(ale_vu_entry));

  vlanId &= 0xFFF; // 12bit
  portNo &= 0x3; // 0, 1, 2

  for (int i = 0; i < MAC_ADDR_LEN; i++)
  {
     pAle[i] = ethAddress[MAC_ADDR_LEN - i - 1];
  }

  pAle[ALE_VLANUCAST_ENTRY_ID_BIT0_BIT7] = (EC_T_BYTE) (vlanId & 0xFF);
  pAle[ALE_VLANUCAST_ENTRY_TYPE_ID_BIT8_BIT11] = (EC_T_BYTE)
     (ALE_ENTRY_VLANUCAST | (vlanId >> 8));
  pAle[ALE_VLANUCAST_ENTRY_TYPE_PORTNO] = (EC_T_BYTE)
     (portNo << ALE_VLANUCAST_ENTRY_PORT_SHIFT);

  // Set the VLAN entry in the ALE table 
  CPSWAleTableEntrySet(pAdapter, tableIdx, ale_vu_entry);
}

static void CPSWAleSetupTable(PT_CPSW_INTERNAL pAdapter)
{
   EC_T_BOOL bMaster = pAdapter->oInitParms.bMaster;
   EC_T_DWORD dwOffs = bMaster ? 0 : 2;
   EC_T_DWORD dwPortIdx = CPSWPortIdx(pAdapter); // 0 .. 1
   EC_T_DWORD dwVlanId = dwPortIdx + 1; // Use portnumber (1 or 2) as VLAN Id

   // Idx0: Add a VLAN Table Entry with ports 0 and 1 as members (clear the flood masks).
   CPSWAleVlanEntrySet(pAdapter, 0 + dwOffs, dwVlanId, HOST_PORT_MASK | (1 << (dwPortIdx + 1)));

   // Idx1: Add a VLAN/Unicast Address Table Entry with the Port1/0 VLAN and a port number of 0.
   CPSWAleVlanUcastEntrySet(pAdapter, 1 + dwOffs, HOST_PORT_NO, dwVlanId, pAdapter->abyStationAddress);
}
#endif // if !defined ALE_BYPASS

/***************************************************************************************************
*                                 DMA / Buffer
*/

static void CPSWSetupBuffers(PT_CPSW_INTERNAL pAdapter)
{
   T_CPSW_FRAMEBUFENTRY *pBufEntry;
   int i;

   for (i = 0; i < CPSW_RXDMABUFCNT; ++i)
   {
      pBufEntry = RX_DMABUF_PTR(i);
      pBufEntry->bufentry.dwMagicKey = BUFFER_MAGIC_RX;
      pBufEntry->bufentry.eBufState = BS_FREE;
   }

   for (i = 0; i < CPSW_TXDMABUFCNT; ++i)
   {
      pBufEntry = TX_DMABUF_PTR(i);
      pBufEntry->bufentry.dwMagicKey = BUFFER_MAGIC_TX;
      pBufEntry->bufentry.eBufState = BS_FREE;
   }

#if defined COPY_FRAME
   for (i = 0; i < CPSW_RXCACHEDBUFCNT; ++i)
   {
      pBufEntry = RX_CACHEDBUF_PTR(i);
      pBufEntry->bufentry.dwMagicKey = BUFFER_MAGIC_RX;
      pBufEntry->bufentry.eBufState = BS_FREE;
   }

   for (i = 0; i < CPSW_TXCACHEDBUFCNT; ++i)
   {
      pBufEntry = TX_CACHEDBUF_PTR(i);
      pBufEntry->bufentry.dwMagicKey = BUFFER_MAGIC_TX;
      pBufEntry->bufentry.eBufState = BS_FREE;
   }
#endif
}

//
// Setup and link RX DMA descriptors
//
static void CPSWSetupRxDtors(PT_CPSW_INTERNAL pAdapter)
{
   volatile T_CPSW_DESC *pRxDesc;
   T_CPSW_FRAMEBUFENTRY *pBufEntry;
   int i;

#if !defined COPY_FRAME
   // Only half of the buffers are directly assigned here. The unused buffers are used in EcLinkReceiveFrame()
   // for descriptor assignment after DMA transfer has completed.
   pAdapter->dwRxBufIdx = CPSW_RXDESCNT;
#endif

   for (i = 0; i < CPSW_RXDESCNT; ++i)
   {
      pRxDesc = RX_DESC_PTR(i);
      pBufEntry = RX_DMABUF_PTR(i);

      // Write cachelines back to memory
      LinkOsCacheDataSync(pBufEntry->pFrame,
     		  dma_va2pa(pAdapter, (EC_T_DWORD) pBufEntry->pFrame),
     		  CPSW_CACHE_UP(CPSW_RXBUFLEN - CACHELINE_SIZE));

      // Fill DMA dtor
      W32(pRxDesc->dwNext, dmadtor_va2pa(pAdapter, (EC_T_DWORD) RX_DESC_PTR(i + 1)));
      W32(pRxDesc->dwBuffer, dma_va2pa(pAdapter, (EC_T_DWORD) pBufEntry->pFrame));
      W32(pRxDesc->dwLen, CPDMA_RXBUFLEN);
      W32(pRxDesc->dwMode, CPDMA_DESC_OWNER | CPDMA_RXBUFLEN); // descriptor owned by MAC

      // Mark buffer as used
      pBufEntry->bufentry.eBufState = BS_ALLOC;
   }

   // Last dtor is EOQ
   pAdapter->pRxDesc[CPSW_RXDESCNT - 1].dwNext = 0;
   pAdapter->pRxTailDesc = RX_DESC_PTR(CPSW_RXDESCNT - 1);

#if defined TRACE && 0
   for (i = 0; i < CPSW_RXDESCNT; ++i)
   {
      pRxDesc = &pAdapter->pRxDesc[i];
      TRC("pRxDescVirt 0x%08x, pRxDescPhys 0x%08x, Next 0x%08x, Buffer 0x%08x, Len 0x%08x, Mode 0x%08x\n",
         (EC_T_DWORD)pRxDesc,
         dmadtor_va2pa(pAdapter, (EC_T_DWORD) pRxDesc),
         R32(pRxDesc->dwNext),
         R32(pRxDesc->dwBuffer),
         R32(pRxDesc->dwLen),
         R32(pRxDesc->dwMode));
   }
#endif
}

//
// Setup and link TX DMA descriptors
//
static void CPSWSetupTxDtors(PT_CPSW_INTERNAL pAdapter)
{
#if defined COPY_FRAME
   volatile T_CPSW_DESC *pTxDesc;
   T_CPSW_FRAMEBUFENTRY *pBufEntry;
   int i;

   for (i = 0; i < CPSW_TXDESCNT; ++i) // Assign each TX-DMA dtor one fixed buffer
   {
      pTxDesc = TX_DESC_PTR(i);
      pBufEntry = TX_DMABUF_PTR(i);

      // Fill DMA dtor
      W32(pTxDesc->dwBuffer, dma_va2pa(pAdapter, (EC_T_DWORD) pBufEntry->pFrame));

      pBufEntry->bufentry.eBufState = BS_ALLOC;
   }
#else
   EC_UNREFPARM(pAdapter);
#endif
}

#if defined COPY_FRAME
static T_CPSW_FRAMEBUFENTRY* CPSWFindNextFreeTxBufferCached(PT_CPSW_INTERNAL pAdapter)
{  
   T_CPSW_FRAMEBUFENTRY *pBufEntry = EC_NULL;

   // Search first free TX buffer
   for (int i = 0; i < CPSW_TXCACHEDBUFCNT; ++i)
   {
      pBufEntry = TX_CACHEDBUF_PTR(pAdapter->dwTxBufIdx);

      // Increment ring buffer pointer
      pAdapter->dwTxBufIdx = NEXT_TX_CACHEDBUF_IDX(pAdapter->dwTxBufIdx);

      if (pBufEntry->bufentry.eBufState == BS_FREE) return pBufEntry;
   }

   return EC_NULL;
}
#else
static T_CPSW_FRAMEBUFENTRY* CPSWFindNextFreeTxBuffer(PT_CPSW_INTERNAL pAdapter)
{  
   volatile T_CPSW_DESC* pDesc = EC_NULL;
   T_CPSW_FRAMEBUFENTRY* pBufEntry = EC_NULL;

   // Search first free TX buffer
   for (int i = 0; i < CPSW_TXDMABUFCNT; ++i)
   {
      pBufEntry = TX_DMABUF_PTR(pAdapter->dwTxBufIdx);

      LinkOsMemoryBarrier();
      
      pDesc = &pAdapter->pTxDesc[pBufEntry->bufentry.dwDescIdx];

      LinkOsMemoryBarrier();
      
      LinkOsDbgAssert(pBufEntry->bufentry.dwMagicKey == BUFFER_MAGIC_TX);

      // Increment ring buffer pointer
      pAdapter->dwTxBufIdx = NEXT_TX_DMABUF_IDX(pAdapter->dwTxBufIdx);

      if (   (pBufEntry->bufentry.eBufState == BS_FREE)          // Buffer not allocated or ...
         || ((pBufEntry->bufentry.eBufState == BS_DMA_STARTED)   // buffer allocated and DMA pending and ...
         && (! (R32(pDesc->dwMode) & CPDMA_DESC_OWNER) )         // OWN bit in coresponding dtor cleared
         )                                                       //    -> DMA transfer completed
         )
      {
         return pBufEntry;
      }
   }

   return EC_NULL;
}
#endif

static T_CPSW_FRAMEBUFENTRY* CPSWAllocTxBuffer(PT_CPSW_INTERNAL pAdapter)
{
   T_CPSW_FRAMEBUFENTRY *pBuf;
#if defined COPY_FRAME
   pBuf = CPSWFindNextFreeTxBufferCached(pAdapter);
#else
   pBuf = CPSWFindNextFreeTxBuffer(pAdapter);
#endif
   if (pBuf != EC_NULL)
   {
      pBuf->bufentry.eBufState = BS_ALLOC;
   }
   return pBuf;
}

#if defined COPY_FRAME
static T_CPSW_FRAMEBUFENTRY* CPSWFindNextFreeRxBufferCached(PT_CPSW_INTERNAL pAdapter)
{  
   T_CPSW_FRAMEBUFENTRY* pBufEntry = EC_NULL;

   // Search next free RX buffer which is used to reassign the descriptor
   for (int i = 0; i < CPSW_RXCACHEDBUFCNT; ++i)
   {
      pBufEntry = RX_CACHEDBUF_PTR(pAdapter->dwRxBufIdx);

      LinkOsDbgAssert(pBufEntry->bufentry.dwMagicKey == BUFFER_MAGIC_RX);

      // Increment ring buffer pointer
      pAdapter->dwRxBufIdx = NEXT_RX_CACHEDBUF_IDX(pAdapter->dwRxBufIdx);

      if (pBufEntry->bufentry.eBufState == BS_FREE)
      {
         return pBufEntry;
      }
   }

   return EC_NULL;
}
#else
static T_CPSW_FRAMEBUFENTRY* CPSWFindNextFreeRxBuffer(PT_CPSW_INTERNAL pAdapter)
{  
   T_CPSW_FRAMEBUFENTRY* pBufEntry = EC_NULL;

   // Search next free RX buffer which is used to reassign the descriptor
   for (int i = 0; i < CPSW_RXDMABUFCNT; ++i)
   {
      pBufEntry = RX_DMABUF_PTR(pAdapter->dwRxBufIdx);

      LinkOsDbgAssert(pBufEntry->bufentry.dwMagicKey == BUFFER_MAGIC_RX);

      // Increment ring buffer pointer
      pAdapter->dwRxBufIdx = NEXT_RX_DMABUF_IDX(pAdapter->dwRxBufIdx);

      if (pBufEntry->bufentry.eBufState == BS_FREE)
      {
         return pBufEntry;
      }
   }

   return EC_NULL;
}
#endif

static T_CPSW_FRAMEBUFENTRY* CPSWAllocRxBuffer(PT_CPSW_INTERNAL pAdapter)
{
   T_CPSW_FRAMEBUFENTRY *pBuf;
#if defined COPY_FRAME
   pBuf = CPSWFindNextFreeRxBufferCached(pAdapter);
#else
   pBuf = CPSWFindNextFreeRxBuffer(pAdapter);
#endif
   if (pBuf != EC_NULL)
   {
      pBuf->bufentry.eBufState = BS_ALLOC;
   }
   return pBuf;
}

/***************************************************************************************************
*                                 Init
*/

static void CPSWInitializeHardware(PT_CPSW_INTERNAL pAdapter)
{
   PCPSW_REGSET regs = &pAdapter->regs;
   volatile PCPSW_SLAVE_REGS pSlaveReg = regs->SelectedSlave;
   EC_T_BOOL bMaster = pAdapter->oInitParms.bMaster;
   EC_T_DWORD dwPortIdx = CPSWPortIdx(pAdapter);
   EC_T_DWORD dwPacketPrio = pAdapter->dwRxDmaChan; // DMA chan is 0 .. 7. So can be mapped to packet prio.

   DBG("Tx Version 0x%08x\n", R32(regs->Dma->Tx_Idver));
   DBG("Rx Version 0x%08x\n", R32(regs->Dma->Rx_Idver));

   if (bMaster)
   {
      // (8) Configure the CPSW_3G_CONTROL register
      W32(regs->Cpsw->CPSW_Control, 0);

      // (9) Configure the Statistic Port Enable register
      W32(regs->Cpsw->CPSW_Stat_Port_En, 0);

      // (10) Configure ALE

      // Dual Mac Mode: Set the ale_vlan_aware bit in the ALE_Control register.
#ifdef ALE_BYPASS
      W32(regs->Ale->ALE_Control, 
         CPSW3G_ALECONTROL_ENABLEALE | CPSW3G_ALECONTROL_CLEARTABLE | CPSW3G_ALECONTROL_VLANAWARE 
         | CPSW3G_ALECONTROL_ALEBYPASS);
#else
      W32(regs->Ale->ALE_Control, 
         CPSW3G_ALECONTROL_ENABLEALE | CPSW3G_ALECONTROL_CLEARTABLE | CPSW3G_ALECONTROL_VLANAWARE);
#endif

      // Set the ports (host, port1, port2) to FORWARD
      W32(regs->Ale->ALE_PortCtl[0], ALE_PORTCTL_PSTATE_FORWARD); 
      W32(regs->Ale->ALE_PortCtl[1], ALE_PORTCTL_PSTATE_FORWARD); 
      W32(regs->Ale->ALE_PortCtl[2], ALE_PORTCTL_PSTATE_FORWARD); 

      // Dual Mac Mode: Configure port0 and port1 for one VLAN ID;
      // port0 and port2 for a different VLAN ID.
      // Use VLAN ID 0 for the host port. 
      W32(regs->Host->P0_Port_VLAN, 0);

      // (12) Configure the CPDMA receive DMA controller
      // (13) Configure the CPDMA transmit DMA controller

      // The queue uses a fixed (channel 7 highest priority) priority
      // scheme to select the next channel for transmission
      W32(regs->Dma->DMAControl, CPDMA_DMACONTROL_TXPTYPE_MASK);

      W32(regs->Dma->Tx_Control, CPDMA_TXCONTROL_TXEN); // Enable TX-DMA
      W32(regs->Dma->Rx_Control, CPDMA_RXCONTROL_RXEN); // Enable RX-DMA

      // CPSW CPDMA RX (PORT 0 TX) SWITCH PRIORITY TO DMA CHANNEL
      // E.g.: All priorities of port2 -> DMA channel 1
      //       All priorities of port1 -> DMA channel 0
      //       -> 0x11110000
      EC_T_DWORD dwChP2 = (dwPortIdx == 1) ? pAdapter->dwRxDmaChan : DMACHAN_INVERSE(pAdapter->dwRxDmaChan);
      EC_T_DWORD dwChP1 = DMACHAN_INVERSE(dwChP2);
      EC_T_DWORD dwTmp = (dwChP1 << 0) | (dwChP1 << 4) | (dwChP1 << 8) | (dwChP1 << 12) |
             (dwChP2 << 16) | (dwChP2 << 20) | (dwChP2 << 24) | (dwChP2 << 28);
      TRC("CPDMA_Rx_Ch_Map 0x%08x\n", dwTmp);
      W32(regs->Host->CPDMA_Rx_Ch_Map, dwTmp);

      // CPSW PORT 0 TX HEADER PRI TO SWITCH PRI MAPPING REGISTER
      // Map TX packet header priorities of:
      // 7 and 6 to switch queue priority 3 (highest)
      // 5 and 4 to switch queue priority 3
      // 3 and 2 to switch queue priority 2
      // 1 and 0 to switch queue priority 2
      W32(regs->Host->P0_Tx_Pri_Map, 0x33332222);

      // Dual Mac Mode: Select the dual mac mode on the port 0 FIFO by setting tx_in_sel[1:0] = 01 in P0_Tx_In_Ctl.
      W32(regs->Host->P0_Tx_In_Ctl, 0x1 << CPSW3G_PORTTXFIFO_INSEL_SHIFT);
   }

   // Dual Mac Mode: Configure port0 and port1 for one VLAN ID;
   // port0 and port2 for a different VLAN ID. Here we choose the 
   // port number (1 or 2) as VLAN ID.
   W32(pSlaveReg->Port_VLAN, (dwPacketPrio << CPSW3G_PORTVLAN_PRI_SHIFT) | (dwPortIdx + 1));

#if !defined ALE_BYPASS
   // Fill ALE table
   CPSWAleSetupTable(pAdapter);
#endif

   // Set MAC address for port
   CPSWSetMacAddress(pSlaveReg, pAdapter->abyStationAddress);
   
   // (11) Configure the MDIO
   CPSWMdioInit(pAdapter);
   
   // (15) Configure CPGMAC_SL1 and CPGMAC_SL2, as per the desired mode of operations.
   CPSWSetupSliver(pAdapter);

   // CPSW PORT n TX HEADER PRIORITY TO SWITCH PRI MAPPING REGISTER
   // Map TX packet header priorities of:
   // 7 and 6 to switch queue priority 3 (highest)
   // 5 and 4 to switch queue priority 3
   // 3 and 2 to switch queue priority 2
   // 1 and 0 to switch queue priority 2
   W32(pSlaveReg->Tx_Pri_Map, 0x33332222);

   // FIFO allocation for TX:14, RX:6. (TX+RX=20)
   // W32(pSlaveReg->Max_Blks, 0xE6);

   // (16) Start up RX and TX DMA (Write to HDP of RX and TX)

   W32(regs->StateRam->Rx_HDP[pAdapter->dwRxDmaChan],
      dmadtor_va2pa(pAdapter, (EC_T_DWORD) pAdapter->pRxDesc));
}

static void CPSWMapRegisterSet(PT_CPSW_INTERNAL pAdapter)
{
   PCPSW_REGSET pRegs = &pAdapter->regs;
   EC_T_LINK_PLATFORMDATA_CPSW *pPCfg = &pAdapter->oInitParms.oPlatCfg;
   EC_T_DWORD dwPortIdx = CPSWPortIdx(pAdapter);
   EC_T_DWORD dwBase = pAdapter->dwRegisterBase;

   pRegs->Cpsw = (PCPSW_REGS) dwBase;
   pRegs->Host = (PCPSW_HOST_REGS) (dwBase + pPCfg->dwHostPortOffs);
   pRegs->Ale = (PCPSW_ALE_REGS) (dwBase + pPCfg->dwCpswAleOffs);
   pRegs->Dma = (PCPSW_CPDMA_REGS) (dwBase + pPCfg->dwCpswCpdmaOffs);
   pRegs->StateRam = (PCPSW_STATERAM_REGS) (dwBase + pPCfg->dwCpdmaStateramOffs);
   pRegs->Ss = (PCPSW_SS_REGS) (dwBase + pPCfg->dwCpswSsOffs);
   pRegs->Mdio = (PCPSW_MDIO_REGS) (dwBase + pPCfg->dwMdioOffs);
#ifdef USE_CPPI_RAM
   pRegs->BdRam = (T_CPSW_DESC *) (dwBase + pPCfg->dwBdRamOffs);
#endif

   // Port
   for (int i = 0; i < CPSW_SLAVECNT; ++i)
   {
      pRegs->Slave[i] = (PCPSW_SLAVE_REGS) (dwBase + pPCfg->slave[i].dwCpswSlaveOffs);
      pRegs->Sliver[i] = (PCPSW_SLIVER_REGS) (dwBase + pPCfg->slave[i].dwCpswSliverOffs);
   }

   pRegs->SelectedSlave = pRegs->Slave[dwPortIdx];
   pRegs->SelectedSliver = pRegs->Sliver[dwPortIdx];
}

static void CPSWCheckId(PT_CPSW_INTERNAL pAdapter)
{
   PCPSW_REGS pReg = pAdapter->regs.Cpsw;
   EC_T_DWORD dwIdReg = R32(pReg->CPSW_IdVer);

   EC_T_DWORD dwId = (dwIdReg >> 16) & 0xFFFF;
   EC_T_DWORD dwRtlVer = (dwIdReg >> 11) & 0x1F;
   EC_T_DWORD dwMajVer = (dwIdReg >> 8) & 0x7;
   EC_T_DWORD dwMinVer = dwIdReg & 0xFF;

   if (dwId == CPSW_ID_IDENT)
   {
      INF("CPSW3G found. ");
   }
   else
   {
      INF("No CPSW3G found (Unknown HW-Id). ");
   }
   
   INF("HW-Id: 0x%04x, RTL: %d, Major: %d, Minor: 0x%x\n",
      dwId, dwRtlVer, dwMajVer, dwMinVer);

#ifdef DEBUG
   // Sanity check
   {
      EC_T_LINK_PLATFORMDATA_CPSW *pPCfg = &pAdapter->oInitParms.oPlatCfg;
      PCPSW_REGSET pRegs = &pAdapter->regs;

      EC_T_DWORD cpswId = R32(pRegs->Cpsw->CPSW_IdVer) >> 16;
      EC_T_DWORD aleId = R32(pRegs->Ale->ALE_IdVer) >> 16;
      EC_T_DWORD txId = R32(pRegs->Dma->Tx_Idver) >> 16;
      EC_T_DWORD ssId = R32(pRegs->Ss->IDVER) >> 16;
      EC_T_DWORD mdioId = (R32(pRegs->Mdio->Version) >> 16) & 0xFF;
      EC_T_DWORD p0MaxBlk = R32(pRegs->Host->P0_Max_blks);
      EC_T_DWORD p1MaxBlk = R32(pRegs->Slave[0]->Max_Blks);
      EC_T_DWORD p2MaxBlk = R32(pRegs->Slave[1]->Max_Blks);
      EC_T_DWORD sliverId0 = R32(pRegs->Sliver[0]->SL_IDVER) >> 16;
      EC_T_DWORD sliverId1 = R32(pRegs->Sliver[1]->SL_IDVER) >> 16;


      DBG("Register offsets: CPSW %x, Host %x, ALE %x, CPDMA %x, StateRAM %x, SS %x, MDIO %x, BD %x, "
         "Slave0 %x, Slave1 %x, Sliver0 %x, Sliver1 %x\n",
            0,
            pPCfg->dwHostPortOffs,
            pPCfg->dwCpswAleOffs,
            pPCfg->dwCpswCpdmaOffs,
            pPCfg->dwCpdmaStateramOffs,
            pPCfg->dwCpswSsOffs,
            pPCfg->dwMdioOffs,
            pPCfg->dwBdRamOffs,
            pPCfg->slave[0].dwCpswSlaveOffs,
            pPCfg->slave[1].dwCpswSlaveOffs,
            pPCfg->slave[0].dwCpswSliverOffs,
            pPCfg->slave[1].dwCpswSliverOffs
           );

      DBG("Subsystem ID's: CPSW %x, ALE %x, TX %x, SS %x, MDIO %x, Host %x, "
         "Slave0 %x, Slave1 %x, Sliver0 %x, Sliver1 %x\n",
            cpswId, aleId, txId, ssId, mdioId, p0MaxBlk,
            p1MaxBlk, p2MaxBlk, sliverId0, sliverId1);

      if (cpswId == 0x19 &&
         aleId == 0x29 &&
         txId == 0x18 &&
         (ssId == 0x4EDB || ssId == 0x4EDA) &&
         mdioId == 0x7 &&
         p0MaxBlk == 0x104 && // Valid after reset!
         p1MaxBlk == 0x113 && // "
         p2MaxBlk == 0x113 && // "
         sliverId0 == 0x17 &&
         sliverId1 == 0x17)
      {
         DBG("Register mapping valid\n");
      }
      else
      {
         DBG("!!! Register mapping invalid\n");
      }

   }
#endif
}

static void CPSWCReadMacId(PT_CPSW_INTERNAL pAdapter)
{
   EC_T_LINK_PLATFORMDATA_CPSW *pPCfg = &pAdapter->oInitParms.oPlatCfg;
   EC_T_DWORD ioaddr = (EC_T_DWORD)LinkOsMapMemory((EC_T_BYTE *)pPCfg->dwCtlModuleBase, 0x2000);
   if (ioaddr == 0)
   {
      ERR("Map CTL-Module failed\n");
      return;
   }

   EC_T_DWORD ioaddrChId = ioaddr + CPSWPortIdx(pAdapter) * 8;

   if (pAdapter->oInitParms.oPlatCfg.dwCtlModuleBase == AM57xx_pdata.dwCtlModuleBase)
   {
      EC_T_DWORD dwReg0 = r32((volatile EC_T_DWORD *)(ioaddrChId + CTRL_CORE_MAC_ID_SW_0));
      EC_T_DWORD dwReg1 = r32((volatile EC_T_DWORD *)(ioaddrChId + CTRL_CORE_MAC_ID_SW_1));
      pAdapter->abyStationAddress[0] = (EC_T_BYTE)((dwReg1 >> 16U) & 0xFFU);
      pAdapter->abyStationAddress[1] = (EC_T_BYTE)((dwReg1 >> 8U ) & 0xFFU);
      pAdapter->abyStationAddress[2] = (EC_T_BYTE)((dwReg1       ) & 0xFFU);
      pAdapter->abyStationAddress[3] = (EC_T_BYTE)((dwReg0 >> 16U) & 0xFFU);
      pAdapter->abyStationAddress[4] = (EC_T_BYTE)((dwReg0 >> 8U ) & 0xFFU);
      pAdapter->abyStationAddress[5] = (EC_T_BYTE)((dwReg0        )& 0xFFU);
   }
   else
   {
      *((EC_T_DWORD *)pAdapter->abyStationAddress) = r32((volatile EC_T_DWORD *)(ioaddrChId + CTLM_MACID0_HI));
      *((EC_T_WORD *)(pAdapter->abyStationAddress + 4)) = (EC_T_WORD)r32((volatile EC_T_DWORD *)(ioaddrChId + CTLM_MACID0_LO));
   }

   LinkOsUnmapMemory((EC_T_BYTE *)ioaddr, 0x2000);
}

#ifdef CPSW_ENABLE_CLOCKS
static void CPSWClkEnable(PT_CPSW_INTERNAL pAdapter)
{
   /* clocks should be not enabled for am387x chipset */
   if (pAdapter->oInitParms.oPlatCfg.dwCtlModuleBase == AM387x_pdata.dwCtlModuleBase
      && pAdapter->oInitParms.oPlatCfg.dwHostPortOffs == AM387x_pdata.dwHostPortOffs)
   {
      return;
   }

   EC_T_DWORD ioaddr = (EC_T_DWORD)LinkOsMapMemory((EC_T_BYTE *)SOC_PRCM_REGS, 0x1000);
   if (ioaddr == 0)
   {
      ERR("clock en failed\n");
      return;
   }

   /* enable both clocks registers and wait after that */
   w32((volatile EC_T_DWORD *)(ioaddr + CM_PER_CPGMAC0_CLKCTRL), CM_PER_CPGMAC0_CLKCTRL_MODULEMODE_ENABLE);
   w32((volatile EC_T_DWORD *)(ioaddr + CM_PER_CPSW_CLKSTCTRL), CM_PER_CPSW_CLKSTCTRL_CLKTRCTRL_SW_WKUP);

   size_t counter = 0;
   while(0 != (r32((volatile EC_T_DWORD *)(ioaddr + CM_PER_CPGMAC0_CLKCTRL))
         & CM_PER_CPGMAC0_CLKCTRL_IDLEST))
   {
      counter++;
      if ( counter > 1000000 )  /* do not wait forever */
         break;
   }

   counter = 0;
   while(0 == (r32((volatile EC_T_DWORD *)(ioaddr + CM_PER_CPSW_CLKSTCTRL))
         & CM_PER_CPSW_CLKSTCTRL_CLKACTIVITY_CPSW_125MHZ_GCLK))
   {
      counter++;
      if ( counter > 1000000 )  /* do not wait forever */
         break;
   }

   LinkOsUnmapMemory((EC_T_BYTE *)ioaddr, 0x1000);
} 
#endif

#ifdef TRACE
/********************************************************************************/
/** \brief Tracing of a received frame.
*
* \return N/A.
*/
static void emllDumpFrame
   (   EC_T_BOOL bTxFrame, /* [in]  EC_TRUE if it is a tx frame */
   EC_T_BYTE*  pbyFrame,   /* [in]  ethernet frame */
   EC_T_DWORD  dwSize      /* [in]  frame size */
   )
{
   EC_T_DWORD dwCnt;
   const char* szPrefix;
   static EC_T_DWORD s_dwTxFrameCnt = 0;
   static EC_T_DWORD s_dwRxFrameCnt = 0;
   EC_T_DWORD dwFrameCnt;

   if( bTxFrame )
   {
      s_dwTxFrameCnt++;
      szPrefix="SND";
      dwFrameCnt=s_dwTxFrameCnt;
   }
   else
   {
      s_dwRxFrameCnt++;
      szPrefix="RCV";
      dwFrameCnt=s_dwRxFrameCnt;
   }

   PRINT( "\n------------------------------------------------------\n" );
   PRINT( "FRAME-%s[%d](%d):", szPrefix, dwSize, dwFrameCnt );
   for( dwCnt = 0; dwCnt < dwSize; dwCnt++ )
   {
      if( (dwCnt % 16) == 0 )
      {
         PRINT( "\n[%03X]  ", dwCnt );
      }
      PRINT( "%02X ", pbyFrame[dwCnt] );
   }
   PRINT( "\n" );
}
#endif




#if defined USE_CPSW_TEST || 0

#define CFG_AM33XX 1
#define TXBURST_FRAMES 4 // Number of frames to send per cycle

CCALLING void emllCPSWTest(int argc, char **argv);
EC_T_DWORD emllCPSWTestFrameRcv(void* pvContext, struct _EC_T_LINK_FRAMEDESC* pLinkFrameDesc, EC_T_BOOL* pbFrameProcessed);
EC_T_DWORD emllCPSWTestNotify(EC_T_DWORD dwCode, struct _EC_T_LINK_NOTIFYPARMS* pParms);

void* pvLLInstance;

static EC_T_BYTE   S_abyFrameData[60] =
{
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,   /* Dest Mac */                            /*0-5*/
   0xD0, 0xAD, 0xBE, 0xEF, 0xBA, 0xBE,   /* Src Mac */                             /*6-11*/
   0x88, 0xa4,                           /* Type ECAT */                           /*12-13*/
   0x0e, 0x10,                           /* E88A4 Length 0x0e, Type ECAT (0x1) */  /*14-15*/
   0x01,                                 /* APRD */                                /*16*/
   0x80,                                 /* Index */                               /*17*/
   0x00, 0x00,                           /* Slave Address */                       /*18-19*/
   0x30, 0x01,                           /* Offset Address */                      /*20-21*/
   0x04, 0x00,                           /* Length - Last sub command */           /*22-23*/
   0x00, 0x00,                           /* Interrupt */                           /*24-25*/
   0x00, 0x00,                           /* Data */                                /*26-27*/
   0x00, 0x00,                           /* Data 2 */                              /*28-29*/
   0x00, 0x00                            /* Working Counter */                     /*30-31*/
};

static EC_T_BOOL s_bNotUseDmaBuffers = EC_TRUE;

#if !(defined STARTERWARE_NOOS) && !(defined EC_VERSION_SYSBIOS)
// For Windows CE, change entry point to mainACRTStartup under Linker/Advanced settings in VS
CCALLING int main(int argc, char **argv)
{
   emllCPSWTest(argc, argv);
   PRINT("Quit\n");
   return 0;
}
#endif

EC_T_BOOL DbgMsgHook(const EC_T_CHAR* szFormat, EC_T_LINKVALIST vaArgs)
{
   vfprintf(stderr, szFormat, vaArgs);
   fflush(stderr);
   return EC_FALSE;
}

static EC_T_LINKSTATUS emllCPSWTestGetStatus(void *pvLLInstance) // Query PHY's link status and reconfigure MAC
{
   EcLinkIoctl(pvLLInstance, EC_LINKIOCTL_UPDATE_LINKSTATUS, 0); // Update link status
   return EcLinkGetStatus(pvLLInstance);
}

static EC_T_BOOL sendFrame(
  EC_T_BYTE *frame, EC_T_DWORD frameLen,
  EC_T_BYTE dstAddr[6], EC_T_BYTE srcAddr[6])
{
   EC_T_LINK_FRAMEDESC oSendFrame = {0};
   EC_T_DWORD dwStatus;

   if (!s_bNotUseDmaBuffers)
   {
	   dwStatus = EcLinkAllocSendFrame(pvLLInstance, &oSendFrame, frameLen);
	   if (dwStatus != EC_E_NOERROR)
	   {
	      PRINT( "EcLinkAllocSendFrame returned error 0x%x\n", dwStatus);
	      return EC_FALSE;
	   }

	   // Copy frame into DMA buffer
	   memcpy(oSendFrame.pbyFrame, frame, frameLen);

	   // Dst addr
	   for (int j = 0; j < 6; ++j) oSendFrame.pbyFrame[j] = dstAddr[j];
	   // Src addr
	   for (int j = 0; j < 6; ++j) oSendFrame.pbyFrame[6 + j] = srcAddr[j];
   }
   else
   {
	   oSendFrame.pbyFrame = frame;
	   oSendFrame.dwSize = frameLen;
   }

   dwStatus = EcLinkSendAndFreeFrame(pvLLInstance, &oSendFrame);
   if (dwStatus != EC_E_NOERROR) 
   {
      PRINT( "EcLinkSendAndFreeFrame returned error 0x%x\n", dwStatus);
      return EC_FALSE;
   }

   return EC_TRUE;
}

static EC_T_BOOL sendAllFrames(
   EC_T_BYTE *frame, EC_T_DWORD frameLen,
   EC_T_BYTE dstAddr[6], EC_T_BYTE srcAddr[6],
   EC_T_DWORD *frameCounter, int txFrames)
{
   for (int j = 0; j < txFrames; ++j)
   {
     if (!sendFrame(frame, frameLen, dstAddr, srcAddr)) return EC_FALSE;
     (*frameCounter)++;
   }
   return EC_TRUE;
}

static void IoCtrl(EC_T_DWORD dwCode)
{
   EC_T_DWORD dwErr = EcLinkIoctl(pvLLInstance, dwCode, EC_NULL);
   if (dwErr != EC_E_NOERROR)
   {
      PRINT( "EcLinkIoctl returned error 0x%x\n", dwErr);
   }
}

void emllCPSWTest(int argc, char **argv)
{
   EC_T_LINK_PARMS_CPSW LinkParamCPSW;
   EC_T_DWORD dwStatus;
   EC_T_LINK_FRAMEDESC oRecvFrame = {0};
#if 0
   EC_T_BOOL bLinkOKFlag = EC_FALSE;
#endif
   EC_T_DWORD i;
   EC_T_BYTE dstAddress[] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
   EC_T_BYTE srcAddress[] = { 0xD0, 0xAD, 0xBE, 0xEF, 0xBA, 0xBE };
   int macbuf[6];
   EC_T_DWORD dwInstance = 0;
   EC_T_DWORD intFlag = 0;
   EC_T_BOOL bMaster = EC_FALSE;
   EC_T_DWORD rxtxMask = 0;
   EC_T_DWORD numframes = 0;
   EC_T_DWORD txFrames = 0;
   EC_T_DWORD rxFrames = 0;
   EC_T_DWORD oldRxFrames = 0;
   
#define RXMSK 1
#define TXMSK 2

   LinkOsAddDbgMsgHook(DbgMsgHook);

   PRINT("emllCPSWTest test program\n");

   if (argc < 4)
   {
      PRINT(
         "Usage: emllCPSWTest <CPSW-Port> <IntFlag> [m | s] { <rxtx> <numframes> <dstaddr> <srcaddr>}\n"
            "  CPSW-Port: 1 first, 2 second.\n"
            "  IntFlag: [0 | 1] 0 Polling, 1 Interrupt\n"
            "  m := Master (Init MAC), s := Slave (for 2nd instance)\n"
            "  rxtx := 1 rx, 2 tx, 3 rx+tx\n"
            "  numframes := TX-Frames, 0 for infinite\n"
            "  dstaddr := Destination MAC in format xx:xx:xx:xx:xx:xx\n"
            "  srcaddr := Destination MAC in format xx:xx:xx:xx:xx:xx\n"
            "\n"
            );
      return;
   }

   dwInstance = (EC_T_DWORD)atoi(argv[1]);
   intFlag = (EC_T_DWORD)atoi(argv[2]);
   bMaster = argv[3][0] == 'm';
   rxtxMask = argc >= 5 ? atoi(argv[4]) : RXMSK | TXMSK;
   rxtxMask &= 0x3;
   numframes = argc >= 6 ? atoi(argv[5]) : 0;
   
   if (dwInstance < 1 || dwInstance > 2 || intFlag > 1 || strlen(argv[3]) != 1 || (argv[3][0] != 'm' && argv[3][0] != 's'))
   {
      PRINT("Invalid usage\n");
      return;
   }

   if (argc >= 7)
   {
      sscanf(argv[6], "%2x:%2x:%2x:%2x:%2x:%2x", // %x needs DWORD buffers, not BYTE!
         macbuf+0, macbuf+1, macbuf+2, macbuf+3, macbuf+4, macbuf+5);
      for (int i = 0; i < 6; ++i) dstAddress[i] = (EC_T_BYTE) (macbuf[i] & 0xFF);
   }

   if (argc >= 8)
   {
      sscanf(argv[7], "%2x:%2x:%2x:%2x:%2x:%2x", // %x needs DWORD buffers, not BYTE!
         macbuf+0, macbuf+1, macbuf+2, macbuf+3, macbuf+4, macbuf+5);
      for (int i = 0; i < 6; ++i) srcAddress[i] = (EC_T_BYTE) (macbuf[i] & 0xFF);
   }

   PRINT( "Instance %d, Flags [%s] [%s] [%s %s], TX-Frames %d\n, ",
      dwInstance,
      intFlag ? "Interrupt" : "Polling", 
      bMaster ? "Master" : "Slave",
      (rxtxMask & RXMSK) ? "RX" : "--",
      (rxtxMask & TXMSK) ? "TX" : "--",
      numframes);

   /* initialization */
   memset(&LinkParamCPSW, 0, sizeof(LinkParamCPSW));
  
#if CFG_AM387X
   memcpy(&LinkParamCPSW.oPlatCfg, &AM387x_pdata, sizeof(AM387x_pdata));
#elif (defined SOC_AM572x)
   memcpy(&LinkParamCPSW.oPlatCfg, &AM57xx_pdata, sizeof(AM57xx_pdata));
#elif CFG_AM33XX
   memcpy(&LinkParamCPSW.oPlatCfg, &AM33xx_pdata, sizeof(AM33xx_pdata));
#else
#  error No board configuration
#endif
   LinkParamCPSW.dwPhyAddr = dwInstance - 1; // 0 -> Port1, 1 -> Port2
#if (defined EC_VERSION_SYSBIOS)
#if !(defined SOC_AM572x)
   LinkParamCPSW.dwPhyAddr = (dwInstance == 1) ? 1 : 3;
#endif
   LinkParamCPSW.ePhyConnection = eCpswPhySource_RGMII;
#endif
   LinkParamCPSW.linkParms.dwInstance = dwInstance;
   LinkParamCPSW.dwPortPrio = 1;// /*bMaster ? 0 : 1*/ dwInstance - 1;
   LinkParamCPSW.bPhyRestartAutoNegotiation = EC_TRUE;
   LinkParamCPSW.bNotUseDmaBuffers = s_bNotUseDmaBuffers;
   LinkParamCPSW.bMaster = bMaster;
   LinkParamCPSW.linkParms.dwSignature = EC_LINK_PARMS_SIGNATURE_CPSW;
   LinkParamCPSW.linkParms.eLinkMode = intFlag ? EcLinkMode_INTERRUPT : EcLinkMode_POLLING;

   LinkParamCPSW.ePhyConnection = eCpswPhySource_RGMII;

   dwStatus = EcLinkOpen( &LinkParamCPSW, emllCPSWTestFrameRcv, emllCPSWTestNotify, EC_NULL, &pvLLInstance );
   if (dwStatus != EC_E_NOERROR)
   {
      PRINT( "emllCPSWTest: error 0x%x in EcLinkOpen!\n", dwStatus );
      return;
   }
   
   /* wait for link */
   while (emllCPSWTestGetStatus(pvLLInstance) != eLinkStatus_OK)
   {
      LinkOsSleep(2000);
      PRINT( "No Link\n");
   }

   /* Send and receive frames */
   LinkOsSleep(1);
   for (i = 0; numframes == 0 || i < numframes; ++i)
   {
      if (rxtxMask & TXMSK)
      {
         IoCtrl(EC_LINKIOCTL_SENDACYCLICFRAMES);
         if (!sendAllFrames(
            S_abyFrameData, sizeof(S_abyFrameData),
            dstAddress, srcAddress, &txFrames, TXBURST_FRAMES)) goto Exit;
         IoCtrl(EC_LINKIOCTL_FLUSHFRAMES);
      }

      oldRxFrames = rxFrames;

      LinkOsSleep(1); // Wait for next cycle

#if 0
      /* Check link status */
      if (bLinkOKFlag && emllCPSWTestGetStatus(pvLLInstance) != eLinkStatus_OK)
      {
         PRINT( "Link Down\n");
         bLinkOKFlag = EC_FALSE;
      }
      if (! bLinkOKFlag && emllCPSWTestGetStatus(pvLLInstance) == eLinkStatus_OK)
      {
         PRINT( "Link Up. Speed %u MBit\n", EcLinkGetSpeed(pvLLInstance));
         bLinkOKFlag = EC_TRUE;
      }
#endif

      if (rxtxMask & RXMSK)
      {
         /* receive frame(s) */
         for (;;)
         {
            dwStatus = EcLinkRecvFrame(pvLLInstance, &oRecvFrame);
            if (dwStatus != EC_E_NOERROR)
            {
               PRINT( "EcLinkRecvFrame returned error 0x%x\n", dwStatus);
               goto Exit;
            }

            if (oRecvFrame.pbyFrame == EC_NULL) break;

            rxFrames++;

#ifdef TRACE
            // Print frame
            emllDumpFrame(EC_FALSE, (EC_T_BYTE *) oRecvFrame.pbyFrame, oRecvFrame.dwSize);
#endif
            // Release frame
            EcLinkFreeRecvFrame(pvLLInstance, &oRecvFrame);
         }
      }

      /* simulated frame processing */
      LinkOsSysDelay(50);

      if (   ( rxtxMask == RXMSK && rxFrames != oldRxFrames ) // Print if new frame received
         ||  ( rxtxMask == (RXMSK | TXMSK) && i > 0 && (i % 5000) == 0 ) ) // Print each 5s
      {
         PRINT("Cycle %d: TxFrames %d, RxFrames %d\n", i, txFrames, rxFrames);
      }
   }

   /* debug */
   PRINT("Graceful shutdown. TxFrames %d, RxFrames %d\n", txFrames, rxFrames);

Exit:

   dwStatus = EcLinkClose( pvLLInstance );
   if( dwStatus != EC_E_NOERROR )
   {
      PRINT( "EcLinkClose returned error 0x%x\n", dwStatus );
      return;
   }
}

EC_T_DWORD emllCPSWTestFrameRcv
   (   void* pvContext,
       struct _EC_T_LINK_FRAMEDESC* pLinkFrameDesc,
       EC_T_BOOL* pbFrameProcessed
   )
{
   PT_CPSW_INTERNAL pAdapter = (PT_CPSW_INTERNAL) pvLLInstance;
   EC_UNREFPARM(pvContext);
   EC_UNREFPARM(pbFrameProcessed);

#ifdef TRACE
   /* print frame */
   emllDumpFrame(EC_FALSE, (EC_T_BYTE *) pLinkFrameDesc->pbyFrame, pLinkFrameDesc->dwSize);
#endif

   /* release frame */
   EcLinkFreeRecvFrame(pAdapter, pLinkFrameDesc);

   return EC_E_NOERROR;
}

EC_T_DWORD emllCPSWTestNotify
   (   EC_T_DWORD dwCode,
       struct _EC_T_LINK_NOTIFYPARMS* pParms
   )
{
   EC_UNREFPARM(dwCode);
   EC_UNREFPARM(pParms);
   return EC_E_NOERROR;
}

#endif /* USE_CPSW_TEST */


/*-END OF SOURCE FILE--------------------------------------------------------*/
