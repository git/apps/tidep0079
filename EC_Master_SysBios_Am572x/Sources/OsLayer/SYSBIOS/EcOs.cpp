/*-----------------------------------------------------------------------------
 * EcOs.cpp                 Implementation file
 * Copyright                acontis technologies GmbH, Weingarten, Germany
 * Response                 Vladimir Pustovalov
 * Description              EtherCAT master OS Layer for SYS/BIOS
 *---------------------------------------------------------------------------*/

/*-INCLUDES------------------------------------------------------------------*/
#include <EcOsPlatform.h>
#include <EcOs.h>
#include <EcError.h>

#include <time.h>

// sys/bios specific includes
#include <xdc/std.h>

#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Timestamp.h>
#include <xdc/runtime/Assert.h>
#include <xdc/runtime/Types.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Event.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/timers/dmtimer/Timer.h>
#include <ti/sysbios/gates/GateMutex.h>


/*-DEFINES-------------------------------------------------------------------*/

/*-TYPEDEFS------------------------------------------------------------------*/
typedef struct _OS_LOCK_DESC
{
    EC_T_OS_LOCK_TYPE   eLockType;  /* type of lock e. g. Default, SpinLock, Semaphore */
    int                 nLockCnt;
    GateMutex_Handle    hGate;
    IArg                key;        /* a value returned by Lock (GateMutex_enter) function */
} OS_LOCK_DESC;



/*-LOCALS--------------------------------------------------------------------*/
static EC_PF_SYSTIME    S_pfSystemTimeGet       = EC_NULL;

static EC_PF_OSDBGMSGHK S_pfOsDbgMsgHook        = EC_NULL;

/* static parameters for Aux clock system */
static Clock_Params s_clockParams;
static Clock_Handle s_hClock = 0; /* the clock handle*/
static Error_Block s_clockErrorBlock;

EC_T_DWORD              G_dwOsLockCounter       = 0;
EC_T_DWORD              G_dwOsUnLockCounter     = 0;
EC_T_BOOL               G_bOsInit               = EC_FALSE;

static EC_T_BOOL  S_bCalibrated       = EC_FALSE;
static EC_T_DWORD S_dw100kHzFrequency = 0;

/*-EXTERNS--------------------------------------------------------------------*/
extern "C" Timer_Handle g_auxClocksTimerHandle;
extern "C" int UARTVprintf(const char* pStr, va_list vaArgs);




/********************************************************************************/
/** \brief Clock function which triggers Aux event
*
* \return EC_E_NOERROR on success.
*/
xdc_Void ClockFunc(xdc_UArg arg)
{
    if ( arg != 0 )
    {
        OsSetEvent((EC_T_VOID*)arg);
    }
}

/********************************************************************************/
/** \brief Set aux clk freq for job task using standard clocks
*
* \return EC_E_NOERROR on success.
*/
EC_T_DWORD AuxClkInitUsingClock(EC_T_DWORD dwFrequencyHz, EC_T_VOID* pvOsEvent)
{
    EC_T_DWORD dwAuxClkCycleTimeUsec = 1000000/dwFrequencyHz;
    if ( dwAuxClkCycleTimeUsec < Clock_tickPeriod )
    {
        OsDbgMsg( "OsSetAuxClkFreq, minimum cycle time = %d usec, curr request = %d usec\n",
                Clock_tickPeriod, dwAuxClkCycleTimeUsec );
        return EC_E_INVALIDPARM;
    }

    if ( EC_NULL != s_hClock)
    {
        OsDbgMsg("Aux clock already initialized. Please call OsAuxClkDeinit\n");
        return EC_E_INVALIDPARM;
    }

    Error_init(&s_clockErrorBlock);

    Clock_Params_init(&s_clockParams);
    s_clockParams.period = dwAuxClkCycleTimeUsec / Clock_tickPeriod;
    s_clockParams.startFlag = TRUE;
    s_clockParams.arg = (UArg)pvOsEvent;

    s_hClock = Clock_create(ClockFunc, 1, &s_clockParams, &s_clockErrorBlock);
    if (s_hClock == NULL)
    {
        OsDbgMsg("Clock create failed\n");
        return EC_E_INVALIDPARM;
    }

    return EC_E_NOERROR;
}

/********************************************************************************/
/** \brief Set aux clk freq for job task using g_auxClocksTimerHandle
*
* \return EC_E_NOERROR on success.
*/
EC_T_DWORD AuxClkInitUsingTimer(EC_T_DWORD dwFrequencyHz, EC_T_VOID* pvOsEvent)
{
    OsDbgAssert(EC_NULL != g_auxClocksTimerHandle);

    /* assumed that timer frequency is less enough to be stored in xdc_Bits32*/
    Types_FreqHz timerFreq;
    Timer_getFreq(g_auxClocksTimerHandle, &timerFreq);

    if ( dwFrequencyHz > timerFreq.lo )
    {
        OsDbgMsg( "OsSetAuxClkFreq, minimum cycle Frequency = %d Hz, curr request = %d Hz\n",
                timerFreq.lo, dwFrequencyHz );
        return EC_E_INVALIDPARM;
    }

    EC_T_DWORD dwNewPeriod = timerFreq.lo / dwFrequencyHz;

    /* reconfigure timer */
    Timer_stop(g_auxClocksTimerHandle);

    Timer_setPeriod(g_auxClocksTimerHandle, dwNewPeriod);
    Timer_setFunc(g_auxClocksTimerHandle, ClockFunc, (UArg)pvOsEvent);

    Timer_start(g_auxClocksTimerHandle);

    return EC_E_NOERROR;
}

/********************************************************************************/
/** \brief Set aux clk freq for job task.
*
* \return EC_E_NOERROR on success.
*/
EC_T_DWORD OsAuxClkInit(EC_T_DWORD dwCpuIndex, EC_T_DWORD dwFrequencyHz, EC_T_VOID* pvOsEvent)
{
    if ( EC_NULL == pvOsEvent )
    {
        OsDbgMsg("Event is NULL\n");
        return EC_E_INVALIDPARM;
    }

    if ( EC_NULL == g_auxClocksTimerHandle )
        return AuxClkInitUsingClock(dwFrequencyHz, pvOsEvent);
    else
        return AuxClkInitUsingTimer(dwFrequencyHz, pvOsEvent);
}

/********************************************************************************/
/** \brief Removes any aux clocks from the system.
*/
EC_T_VOID OsAuxClkDeinit(EC_T_VOID)
{
    if ( s_hClock )
    {
        Clock_delete(&s_hClock);
    }
    if ( g_auxClocksTimerHandle )
    {
        Timer_stop(g_auxClocksTimerHandle);
    }
}


/********************************************************************************/
/** \brief OS Layer initialization.
*
* \return status code
*/
EC_T_DWORD OsInit(EC_T_OS_PARMS* pInitDesc)
{
	if( G_bOsInit == EC_TRUE )
	{
		return EC_E_NOERROR;
	}
    if(pInitDesc != EC_NULL)
    {
        if ((EC_NULL == S_pfSystemTimeGet) && (EC_NULL != pInitDesc->pfSystemTimeGet))
        {
            S_pfSystemTimeGet = pInitDesc->pfSystemTimeGet;
        }

    }
    G_bOsInit = EC_TRUE;
    return EC_E_NOERROR;
}

/********************************************************************************/
/** \brief OS Layer de-initialization.
*
* \return status code
*/
EC_T_DWORD OsDeinit(EC_T_VOID)
{
    G_bOsInit = EC_FALSE;
    return EC_E_NOERROR;
}

/********************************************************************************/
/** \brief Add OS Layer Debug Message hook
*
* \return N/A
*/
EC_T_VOID OsAddDbgMsgHook(EC_PF_OSDBGMSGHK pfOsDbgMsgHook)
{
    S_pfOsDbgMsgHook = pfOsDbgMsgHook;
}

/********************************************************************************/
/** \brief Puts string to the system output and UART
*
*/
EC_T_VOID SysBiosVprintf(const EC_T_CHAR* szFormat, EC_T_VALIST vaArgs)
{
    xdc_runtime_System_printf_va__E(szFormat, vaArgs);

    UARTVprintf(const_cast<char *>(szFormat), vaArgs);
}

/********************************************************************************/
/** \brief Puts string to the system output and UART
*
*/
EC_T_DWORD SysBiosPrintf(const char *szFormat, ...)
{
    EC_T_VALIST vaList;

    EC_VASTART(vaList, szFormat);
    OsVprintf(szFormat, vaList);
    EC_VAEND(vaList);

    return 0;
}

// pvv CEcTimer S_oTimeout;
//extern "C" void StartTimer(unsigned int millisec)
//{
//    S_oTimeout.Start( millisec );
//}
//extern "C" void StopTimer()
//{
//    S_oTimeout.Stop();
//}
//extern "C" unsigned int IsTimerElapsed(void)
//{
//    return (unsigned int)S_oTimeout.IsElapsed();
//}


/********************************************************************************/
/** \brief OsQueryMsecCount
*
* \return ticks count in MSec
*/
EC_T_DWORD OsQueryMsecCount(EC_T_VOID)
{
    /* ticks returned in clock ticks */
    UInt32 ticks = Clock_getTicks();

    UInt64 ticksInMsec = (((UInt64)ticks) * Clock_tickPeriod) / 1000;

    return (EC_T_DWORD)ticksInMsec;
}

/********************************************************************************/
/** \brief Open configuration file
*
* \return handle to configuration file or NULL in case of error
*/
EC_T_VOID* OsCfgFileOpen(const EC_T_CHAR* szCfgFileName)
{
    // pvv
//FRESULT fresult;
//FIL* psFileObject;
//
//    psFileObject = (FIL*)malloc(sizeof(FIL));
//    fresult = f_open( psFileObject, szCfgFileName, (BYTE)FA_READ );
//    if( fresult != FR_OK )
//    {
//        free( psFileObject );
//        psFileObject = EC_NULL;
//    }
//    return (EC_T_VOID*)psFileObject;
    return 0;
}

/********************************************************************************/
/** \brief Close configuration file
*
* \return if no error has occurred, OsCfgFileClose returns 0. Otherwise, it returns a nonzero value.
*/
EC_T_INT OsCfgFileClose(EC_T_VOID* pvCfgFile)
{
    // pvv
//FRESULT fresult;
//FIL* psFileObject;
//EC_T_INT nRet = 0;
//
//    if( EC_NULL != pvCfgFile )
//    {
//        psFileObject = (FIL*)pvCfgFile;
//        fresult = f_close( psFileObject );
//        if( fresult != FR_OK )
//        {
//            nRet = -1;
//        }
//        free( psFileObject );
//    }
//    return nRet;
return 0;
}

/********************************************************************************/
/** \brief Read next chunk of the configuration file
*
* \return number of bytes read
*/
// pvv static FRESULT s_fLastReadResult;
EC_T_INT   OsCfgFileRead(EC_T_VOID* pvCfgFile, EC_T_VOID* pvDst, EC_T_INT nLen)
{
    // pvv
//    FIL* psFileObject;
//EC_T_INT nRet = 0;
//unsigned short usBytesRead;
//
//    //nLen = 1;   // reduce to 1 byte reading, got error if many bytes are read!
//    if( EC_NULL != pvCfgFile )
//    {
//        psFileObject = (FIL*)pvCfgFile;
//        CacheDisable(CACHE_DCACHE);
//        s_fLastReadResult = f_read( psFileObject, (char*)pvDst, nLen, &usBytesRead );
//        CacheEnable(CACHE_DCACHE);
//        if( s_fLastReadResult != FR_OK )
//        {
//            nRet = 0;
//        }
//        else
//        {
//            nRet = (EC_T_INT)usBytesRead;
//        }
//    }
//    S_nLastReadNumBytes = nRet;
//    return nRet;
return 0;
}

/********************************************************************************/
/** \brief Determine if last OsCfgFileRead operation did cause an error
*
* \return if no error has occurred, OsCfgFileError returns 0. Otherwise, it returns a nonzero value.
*/
EC_T_INT   OsCfgFileError(EC_T_VOID* pvCfgFile)
{
    return 0;
}

/********************************************************************************/
/** \brief Determine if the end of the configuration file is reached
*
* \return Returns 0 if the current position has not reached the end of the configuration file.
*/
EC_T_INT   OsCfgFileEof(EC_T_VOID* pvCfgFile)
{
    return EC_TRUE;
}

/********************************************************************************/
/** \brief 
*
* \return
*/
EC_T_VOID OsDbgMsg(const EC_T_CHAR* szFormat, ...)
{
    EC_T_VALIST vaList;

	EC_VASTART(vaList, szFormat);
	if( S_pfOsDbgMsgHook != EC_NULL )
	{
		(*S_pfOsDbgMsgHook)(szFormat, vaList);
	}
	else
	{
	    OsVprintf(szFormat, vaList);
	}
	EC_VAEND(vaList);
}

/***************************************************************************************************/
/**
\brief  Get Curretn DBG Message Hook.
\return Dbg Message Hook.
*/
EC_PF_OSDBGMSGHK OsGetDbgMsgHook()
{
    return S_pfOsDbgMsgHook;
}

/********************************************************************************/
/** \brief Print a trace message
*
* \return N/A
*/
#ifdef OsTrcMsg
#undef OsTrcMsg
#endif
EC_T_VOID OsTrcMsg(const EC_T_CHAR* szFormat, ...)
{
va_list vaArgs;
#define MAX_TRACE_MSGLEN 255
char achTrcMsg[MAX_TRACE_MSGLEN];

    va_start(vaArgs, szFormat);
    vsnprintf(achTrcMsg, MAX_TRACE_MSGLEN, szFormat, vaArgs);
    va_end(vaArgs);
}

/********************************************************************************/
/** \brief 
*
* \return
*/
EC_T_VOID OsDbgAssertFunc(EC_T_BOOL bAssertCondition, EC_T_CHAR* szFile, EC_T_DWORD dwLine)
{
    if( !bAssertCondition )
    {
        xdc_runtime_Assert_raise__I(0, szFile, dwLine, 0);
    }
}


/********************************************************************************/
/** \brief Create a synchronization mutual exclusion object
*
* \return handle to the mutex object.
*/
EC_T_VOID* OsCreateLock(EC_T_VOID)
{
    return OsCreateLockTyped(eLockType_DEFAULT);
}

/********************************************************************************/
/** \brief Create a synchronization mutual exclusion object
*
* \return handle to the mutex object.
*/
EC_T_VOID* OsCreateLockTyped(EC_T_OS_LOCK_TYPE   eLockType)
{
    OS_LOCK_DESC* pvLockDesc = (OS_LOCK_DESC*)OsMalloc(sizeof(OS_LOCK_DESC));

    if( pvLockDesc != EC_NULL )
    {
        GateMutex_Params gateParams;
        Error_Block eb;

        pvLockDesc->nLockCnt = 0;
        pvLockDesc->eLockType = eLockType;

        switch(pvLockDesc->eLockType)
        {
        case eLockType_DEFAULT:     /*< Default mutex           */
        case eLockType_INTERFACE:   /*< only interface */
        case eLockType_SPIN:        /*< only jobs --> spin lock */
            break;
        default:
            OsDbgAssert( EC_FALSE );
        }

        Error_init(&eb);
        GateMutex_Params_init(&gateParams);
        pvLockDesc->hGate = GateMutex_create(&gateParams, &eb);
    }

    OsDbgAssert(EC_NULL != pvLockDesc);
    
    return pvLockDesc;
}


/********************************************************************************/
/** \brief Delete a mutex object
*
* \return N/A
*/
EC_T_VOID  OsDeleteLock(EC_T_VOID* pvLockHandle)
{
    OS_LOCK_DESC* pvLockDesc = (OS_LOCK_DESC*)pvLockHandle;

    if (pvLockDesc != EC_NULL)
    {
        OsDbgAssert( pvLockDesc->nLockCnt == 0 );

        GateMutex_delete(&pvLockDesc->hGate);

        OsFree( pvLockDesc );
    }
}


/********************************************************************************/
EC_T_VOID  OsLock(EC_T_VOID* pvLockHandle)
{
    OS_LOCK_DESC* pvLockDesc = (OS_LOCK_DESC*)pvLockHandle;

    OsDbgAssert( pvLockDesc != EC_NULL );
    if (pvLockDesc != NULL)
    {
        pvLockDesc->key = GateMutex_enter(pvLockDesc->hGate);

        pvLockDesc->nLockCnt++;
        G_dwOsLockCounter++;

#ifdef DEBUG
        if( pvLockDesc->nLockCnt >= 10 )
        {
            /* that much nesting levels? */
            OsDbgMsg( "FATAL Error in OsLock(): pvLockDesc = 0x%x, nLockCnt = 0x%x\n", pvLockDesc, pvLockDesc->nLockCnt );
            OsDbgAssert( EC_FALSE );
        }
#endif
    }
}


/********************************************************************************/
EC_T_VOID  OsUnlock(EC_T_VOID* pvLockHandle)
{
    OS_LOCK_DESC* pvLockDesc = (OS_LOCK_DESC*)pvLockHandle;

    OsDbgAssert( pvLockDesc != EC_NULL );
    if (pvLockDesc != NULL)
    {
        OsDbgAssert( pvLockDesc->nLockCnt > 0 );
        G_dwOsUnLockCounter++;
        pvLockDesc->nLockCnt--;
        GateMutex_leave(pvLockDesc->hGate, pvLockDesc->key);

    }
}


/********************************************************************************/
/** \brief Create an event
*
* \return event object to be referenced in further calls or EC_NULL in case of errors.
*/
EC_T_VOID* OsCreateEvent(EC_T_VOID)
{
    Error_Block eb;
    Error_init(&eb);
    Event_Handle myEvent = Event_create(NULL, &eb);

    return myEvent;
}

/********************************************************************************/
/** \brief  Delete an event object
*
* \return N/A.
*/
EC_T_VOID OsDeleteEvent(EC_T_VOID* pvEvent)
{
    Event_Handle myEvent = (Event_Handle)pvEvent;
    Event_delete(&myEvent);
}

/********************************************************************************/
/** \brief  Sets event to the active state
*
*/
EC_T_VOID EcOsSetEvent(EC_T_VOID* pvEvent)
{
    Event_Handle myEvent = (Event_Handle)pvEvent;
    Event_post(myEvent, Event_Id_00);
}

/********************************************************************************/
/** \brief  Wait for an event object
*
* \return EC_E_NOERROR if event was set for the timeout, or error code in case of errors.
*/
EC_T_DWORD OsWaitForEvent(EC_T_VOID* pvEvent, EC_T_DWORD dwTimeout)
{
    Event_Handle myEvent = (Event_Handle)pvEvent;

    UInt events = Event_pend(myEvent, Event_Id_NONE, Event_Id_00, BIOS_WAIT_FOREVER);
    if ( events != 0 )
        return EC_E_NOERROR;
    else
        return EC_E_ERROR;
}

/********************************************************************************/
/** \brief Create thread.
*
* \return thread object or EC_NULL in case of an error.
*/
EC_T_VOID* OsCreateThread(
    EC_T_CHAR* /*szThreadName  - ignored by BIOS*/,
    EC_PF_THREADENTRY pfThreadEntry,
    EC_T_DWORD dwPrio,
    EC_T_DWORD dwStackSize,
    EC_T_VOID* pvParams
)
{
    if ( dwPrio == 0 || dwPrio >= Task_numPriorities )
    {
        OsDbgMsg("An invalid task priority was used %d \n", dwPrio);
        return EC_NULL;
    }
    Error_Block eb;
    Error_init(&eb);

    Task_Params taskParams;
    Task_Params_init(&taskParams);
    taskParams.arg0 = (xdc_UArg)pvParams;
    taskParams.priority = dwPrio;
    taskParams.stackSize = dwStackSize;

    Task_Handle hTask = Task_create((Task_FuncPtr)pfThreadEntry, &taskParams, &eb);

    EC_T_VOID* pvThreadHandle = hTask;
    return pvThreadHandle;
}

/***************************************************************************************************/
/**
\brief  Delete a thread Handle return by OsCreateThread.
*/
EC_T_VOID   OsDeleteThreadHandle(
    EC_T_VOID* pvThreadObject       /**< [in]   Previously allocated Thread Handle */
                                )
{
    Task_Handle hTask = (Task_Handle)pvThreadObject;
    Task_delete(&hTask);
}

/********************************************************************************/
/** \brief  Demo Application termination check.
*
* \return  EC_TRUE if application shall terminate
*/
EC_T_BOOL OsTerminateAppRequest(void)
{
EC_T_BOOL bTerminate = EC_FALSE;
// pvv
//unsigned char ucChar;
//    if(UARTCharsAvail(SOC_UART_0_REGS))
//    {
//        /*
//        ** Read the next character from the UART.
//        */
//        ucChar = UARTGetc();
//        if( ucChar == 0x1b || ucChar == 0x03 )
//        {
//            bTerminate = EC_TRUE;
//        }
//    }
    return bTerminate;
}


static EC_T_DWORD s_dw1MHzFrequency = 0;
static inline EC_T_UINT64 GetTicksInNs()
{
    if ( 0 == s_dw1MHzFrequency )
    {
        s_dw1MHzFrequency = OsMeasGet100kHzFrequency() / 10;
    }

    EC_T_UINT64 result = OsMeasGetCounterTicks();
    /* convert to 1 usec value */
    result /= s_dw1MHzFrequency;
    /* convert to 1 nsec value */
    result *= 1000;

    return result;
}

/***************************************************************************************************/
/**
@brief  OsSystemTimeGet

    Definition of the System Time
    � Beginning on January, 1st 2000 at 0:00h
    � Base unit is 1 ns
    � 64 bit value (enough for more than 500 years)
 NOTE: this platform do not have clock, so it returns number of MS after startup

@return the current system time value [EC_T_UINT64]
*/
EC_T_DWORD OsSystemTimeGet(EC_T_UINT64* pqwSystemTime)
{
    EC_T_DWORD dwRetVal = EC_E_ERROR;

    /* check parameters */
    if( EC_NULL == pqwSystemTime )
    {
        return EC_E_INVALIDPARM;
    }
    /* get system time */
    if( EC_NULL != S_pfSystemTimeGet )
    {
        /* from configured function */
        dwRetVal = S_pfSystemTimeGet(pqwSystemTime);
    }
    else
    {
        EC_T_UINT64 result = GetTicksInNs();
        EC_SETQWORD(pqwSystemTime, result);

        dwRetVal = EC_E_NOERROR;
    }

    return dwRetVal;
}

/***************************************************************************************************/
/**
@brief  OsMeasGetCounterTicks

@return the current system counter value [EC_T_UINT64]
*/
EC_T_UINT64 OsMeasGetCounterTicks( EC_T_VOID )
{
    xdc_runtime_Types_Timestamp64 stamp;
    xdc_runtime_Timestamp_get64(&stamp);

    EC_T_UINT64 result = stamp.lo + ((UInt64)(stamp.hi) << 32);
    return result;
}

/***************************************************************************************************/
/**
@brief  OsMeasCalibrate

Measure the TSC frequency for timing measurements.
This function must be called before using one of the following functions:
*/
EC_T_VOID OsMeasCalibrate(EC_T_UINT64 dwlFreqSet)
{
    if( !S_bCalibrated )
    {
        xdc_runtime_Types_FreqHz freq;
        xdc_runtime_Timestamp_getFreq(&freq);
        EC_T_UINT64 freqU64 = freq.lo + ((EC_T_UINT64)freq.hi << 32);

        S_dw100kHzFrequency = static_cast<EC_T_DWORD>(freqU64 / 100000);

        if( dwlFreqSet != 0 )
        {
            /* two different frequencies should not happen */
            OsDbgAssert( dwlFreqSet == freqU64 );
        }
        S_bCalibrated = EC_TRUE;
    }
}

/***************************************************************************************************/
/**
@brief  OsMeasGet100kHzFrequency

@return the frequency of the TSC in 100kHz multiples [EC_T_DWORD]
*/
EC_T_DWORD OsMeasGet100kHzFrequency(EC_T_VOID)
{
    if( !S_bCalibrated )
    {
        OsDbgMsg("TscGet100kHzFrequency() Tsc not calibrated\n");
    }

    return S_dw100kHzFrequency;
}


EC_T_BOOL OsSetThreadAffinity( EC_T_VOID* , EC_T_CPUSET )
{
    return EC_TRUE;
}

/*-END OF SOURCE FILE--------------------------------------------------------*/
