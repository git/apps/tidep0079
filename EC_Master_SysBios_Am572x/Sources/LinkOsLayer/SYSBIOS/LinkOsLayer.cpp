/*-----------------------------------------------------------------------------
 * LinkOsLayer.cpp
 * Copyright                acontis technologies GmbH, Weingarten, Germany
 * Response                 Vladimir Pustovalov
 * Description              SYS/BIOS platform layer for EtherCAT link layer
 *---------------------------------------------------------------------------*/

/*-INCLUDES------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "LinkOsLayer.h"
#include "EcLink.h"

// sys/bios specific includes
#include <xdc/std.h>

#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Timestamp.h>

#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>



static EC_PF_LINKOSDBGMSGHK S_pfLinkOsDbgMsgHook    = EC_NULL;


/*******************************************************************************
*
* LinkOsPlatformInit - one time platform initialization
*
*/
static EC_T_DWORD G_dwTicksIn1Usec = 0;

EC_T_VOID LinkOsPlatformInit()
{
    // Init performance counter
    xdc_runtime_Types_FreqHz freq;
    xdc_runtime_Timestamp_getFreq(&freq);
    EC_T_UINT64 freqU64 = freq.lo + ((EC_T_UINT64)freq.hi << 32);

    G_dwTicksIn1Usec = (EC_T_DWORD)(freqU64 / 1000000);

}

/*******************************************************************************
*
* LinkOsSysDelay - delay in MicroSeconds
*
*/
EC_T_VOID LinkOsSysDelay(EC_T_DWORD dwTimeUsec)
{
    LinkOsDbgAssert(G_dwTicksIn1Usec != 0);

    EC_T_UINT64 startTicks = OsMeasGetCounterTicks();

    // try to use system delay in msec if possible
    if ( dwTimeUsec > 1000 )
    {
        EC_T_DWORD dwDelayInMs = dwTimeUsec / 1000;

        NoOsSleep(dwDelayInMs);
    }

    EC_T_UINT64 delayInTicks = (EC_T_UINT64)dwTimeUsec * G_dwTicksIn1Usec;
    EC_T_UINT64 ticksPassed = OsMeasGetCounterTicks() - startTicks;

    while (ticksPassed < delayInTicks)
    {
        ticksPassed = OsMeasGetCounterTicks() - startTicks;
    }
}


/*******************************************************************************
*
* SysAllocDmaBuffer - allocate DMA buffer
*
*/
EC_T_VOID LinkOsAllocDmaBuffer(
    EC_T_VOID*  pvDmaObject
   ,EC_T_BYTE** ppbyVirtAddrCached
   ,EC_T_BYTE** ppbyVirtAddrUncached
   ,EC_T_BYTE** ppbyPhysAddr
   ,EC_T_DWORD  dwSize           
                                 )
{
    ECLINKOS_UNREFPARM(pvDmaObject);
    EC_T_DWORD           dwAddr;
    
    dwAddr = (EC_T_DWORD) malloc(dwSize);
    *ppbyPhysAddr = (EC_T_BYTE*)dwAddr;
    *ppbyVirtAddrCached    = (EC_T_BYTE*)dwAddr;
    *ppbyVirtAddrUncached  = (EC_T_BYTE*)dwAddr;
    
    /* printf("DMA alloc phys 0x%08x, virt 0x%08x\n", *ppbyPhysAddr, *ppbyVirtAddrCached); */
}


/*******************************************************************************
*
* SysFreeDmaBuffer - free DMA buffer
*
*/
EC_T_VOID LinkOsFreeDmaBuffer(
     EC_T_VOID*  pvDmaObject
    ,EC_T_BYTE*  pbyPhysAddr
    ,EC_T_BYTE*  pbyVirtAddrCached
    ,EC_T_BYTE*  pbyVirtAddrUncached
    ,EC_T_DWORD  dwSize         
                             )
{
    ECLINKOS_UNREFPARM(pvDmaObject);
    ECLINKOS_UNREFPARM(pbyPhysAddr);
    ECLINKOS_UNREFPARM(dwSize);
	ECLINKOS_UNREFPARM(pbyVirtAddrUncached);
    
    free(((EC_T_VOID*)pbyVirtAddrCached));
}


EC_T_BYTE* LinkOsMapMemory(EC_T_BYTE* pbyPhysical, EC_T_DWORD dwLen)
{
    ECLINKOS_UNREFPARM( dwLen );
    return pbyPhysical;
}

EC_T_VOID LinkOsUnmapMemory(EC_T_BYTE* pbyVirtual, EC_T_DWORD dwLen)
{
    ECLINKOS_UNREFPARM( dwLen );
    ECLINKOS_UNREFPARM( pbyVirtual );
}


#ifdef ASSERT_SUSPEND
/********************************************************************************/
/** \brief 
*
* \return
*/
EC_T_VOID LinkOsDbgAssertFunc(EC_T_BOOL bAssertCondition, EC_T_CHAR* szFile, EC_T_DWORD dwLine)
{
    if( !bAssertCondition )
    {
        LinkOsDbgMsg( "ASSERTION in file %s, line %d\n", (int)szFile, (int)dwLine, 3,4,5,6 );
    }
}
#endif

/********************************************************************************/
/** \brief Add OS Layer Debug Message hook
*
* \return N/A
*/
EC_T_VOID LinkOsAddDbgMsgHook(EC_PF_LINKOSDBGMSGHK pfOsDbgMsgHook)
{
    S_pfLinkOsDbgMsgHook = pfOsDbgMsgHook;
}

extern "C" int UARTVprintf(const char* pStr, va_list vaArgs);

/********************************************************************************/
/** \brief Puts string to the system output and UART
*
*/
int LinkOsVprintf(const char *szFormat, va_list vaArgs)
{
	int res = xdc_runtime_System_printf_va__E(szFormat, vaArgs);

    res = UARTVprintf(const_cast<char *>(szFormat), vaArgs);

    return res;
}

/********************************************************************************/
/** \brief 
*
* \return
*/
EC_T_VOID LinkOsDbgMsg(const EC_T_CHAR* szFormat, ...)
{
    EC_T_BOOL bPrintMsg = EC_TRUE;
    va_list vaArgs;
    
    va_start(vaArgs, szFormat);
    if( S_pfLinkOsDbgMsgHook != EC_NULL )
    {
        bPrintMsg = (*S_pfLinkOsDbgMsgHook)(szFormat, vaArgs);
    }
    if( bPrintMsg )
    {
        LinkOsVprintf(szFormat, vaArgs);
    }
    va_end(vaArgs);
}

/*******************************************************************************
*
* SysInterruptInitialize - init and enable isr and irq
*
*/
EC_T_BOOL LinkOsInterruptInitialize
(   EC_T_LINKOS_IRQ_PARM* pInterruptParm )
{
    return EC_FALSE;
}

/*******************************************************************************
*
* SysInterruptDisable - disable irq
*
*/
EC_T_VOID LinkOsInterruptDisable(EC_T_LINKOS_IRQ_PARM* pInterruptParm)
{
    ECLINKOS_UNREFPARM(pInterruptParm);
}

void NoOsSleep(EC_T_DWORD dwMsec)
{
    /* convert Msec to ticks */
    EC_T_DWORD nTicks = (dwMsec * 1000) / Clock_tickPeriod;

    if ( nTicks == 0 )
        nTicks = 1;

    Task_sleep(nTicks);
}

/*-END OF SOURCE FILE--------------------------------------------------------*/
