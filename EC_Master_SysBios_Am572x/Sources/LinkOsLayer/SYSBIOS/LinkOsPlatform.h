/*-----------------------------------------------------------------------------
 * LinkOsPlatform.h
 * Copyright                acontis technologies GmbH, Weingarten, Germany
 * Response                 Vladimir Pustovalov
 * Description              SYS/BIOS platform layer for EtherCAT link layer
 *---------------------------------------------------------------------------*/

#ifndef INC_LINK_OS_PLATFORM
#define INC_LINK_OS_PLATFORM

/*-INCLUDE-------------------------------------------------------------------*/
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <EcOs.h>

#include <ti/sysbios/hal/Cache.h>


/*-DEFINES-------------------------------------------------------------------*/

/* use uncached memory for DMA and adapter descriptor */
//#define COPY_FRAME

#define ECLINKOS_INLINEATTRIBUTE

#define MAX_DEVICE_WINDOWS       6
#define CACHELINE_SIZE          64

/* virtual/physical access to DMA area */
#define VIRT_TO_PHYS(pAdapter,dwAddr) (dwAddr)
#define PHYS_TO_VIRT(pAdapter,dwAddr) (dwAddr)

#if 0
#define LinkOsCacheDataSync(pdwData, unused, dwSize)
#define LinkOsCacheDataInvalidateBuff(pdwData, unused, dwSize)
#else
#define LinkOsCacheDataSync(pdwData, unused, dwSize) Cache_wbInv((Ptr)(pdwData), (SizeT)(dwSize), Cache_Type_ALL, TRUE)
#define LinkOsCacheDataInvalidateBuff(pdwData, unused, dwSize) Cache_inv((Ptr)(pdwData), (SizeT)(dwSize), Cache_Type_ALL, TRUE)
#endif

#define LinkOsMalloc(nSize)                 malloc((size_t)(nSize))
#define LinkOsRealloc(pMem,nSize)           realloc((void*)(pMem),(size_t)(nSize))
#define LinkOsFree(pMem)                    free((void*)(pMem))
#define LinkOsMemset(pDest,nVal,nSize)      memset((void*)pDest,(int)nVal,(size_t)(nSize))
#define LinkOsMemcpy(pDest,pSrc,nSize)      memcpy((void*)(pDest),(const void*)(pSrc),(size_t)(nSize))
#define LinkOsMemcmp(pBuf1,pBuf2,nSize)     memcmp((void*)(pBuf1),(const void*)(pBuf2),(size_t)(nSize))
#define LinkOsMemmove(pDest,pSrc,nSize)     memmove((void*)(pDest),(const void*)(pSrc),(size_t)(nSize))
#define LinkOsStrlen(szString)              strlen((const char*)(szString))
#define LinkOsStrcmp(szStr1,szStr2)         strcmp((const char*)szStr1,(const char*)szStr2)

#define LinkOsSetEvent(semBEvent)           semGive((SEM_ID)(semBEvent))
#define LinkOsResetEvent(semBEvent)         semTake((SEM_ID)(semBEvent),NO_WAIT)
#define LinkOsSleep(dwMsec)                 OsSleep(dwMsec)

#ifdef __cplusplus
extern "C"
{
#endif


#if (defined DEBUG) || (defined ASSERT_SUSPEND)
#  ifdef ASSERT_SUSPEND
#    define LinkOsDbgAssert(bCond)                      LinkOsDbgAssertFunc((bCond),(EC_T_CHAR*)__FILE__,__LINE__)
     EC_T_VOID LinkOsDbgAssertFunc(EC_T_BOOL bAssertCondition, EC_T_CHAR* szFile, EC_T_DWORD dwLine);
#  else
#    define LinkOsDbgAssert                             assert
#  endif
#endif
#ifndef LinkOsDbgAssert
#define LinkOsDbgAssert(x)
#endif

#define LinkOsVsnprintf(szMsg,nLen,szFormat,vaArgs) vsnprintf(szMsg,nLen,szFormat,vaArgs)
#define LinkOsSnprintf                              snprintf


extern int LinkOsVprintf(const char *_format, va_list _ap);

#define vfprintf(_fp, _format, _ap) LinkOsVprintf(_format, _ap)

#define LinkOsIntLock                       intLock
#define LinkOsIntUnlock                     intUnlock

inline void LinkOsMemoryBarrier()
{
    asm (" dsb"); /* ARM v7 instruction (e.g. Cortex) */
}

/*-TYPEDEFS/ENUMS------------------------------------------------------------*/
typedef EC_T_DWORD PHYSICAL_ADDRESS;

/*NO PCI support*/
typedef EC_T_DWORD T_PCIINFO;

typedef va_list             EC_T_LINKVALIST;

/*-MACROS/INLINES------------------------------------------------------------*/

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* INC_LINK_OS_PLATFORM */


/*-END OF SOURCE FILE--------------------------------------------------------*/
