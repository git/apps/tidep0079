/*-----------------------------------------------------------------------------
 * EcOsPlatform.h           
 * Copyright                acontis technologies GmbH, Weingarten, Germany
 * Response                 Vladimir Pustovalov
 * Description              EC-Master OS-Layer header file for SYS/BIOS
 *---------------------------------------------------------------------------*/

#ifndef INC_ECOSPLATFORM
#define INC_ECOSPLATFORM

/*-SUPPORT-SELECTION---------------------------------------------------------*/

/*-INCLUDES------------------------------------------------------------------*/
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include <EcType.h>

/*-DEFINES-------------------------------------------------------------------*/
/* Platform identification */
#define ATECAT_PLATFORMSTR "SYSBIOS"
#define EC_VERSION_SYSBIOS 1

/* Structure pack */
#define EC_PACKED_INCLUDESTART(Bytes)   <EcOsPlatform.h>
#define EC_PACKED_INCLUDESTOP           <EcOsPlatform.h>
#define EC_PACKED(Bytes)                __attribute__((aligned(Bytes),packed))

#define ATECAT_API
#  define WITHALIGNMENT

/* using the ASSERT_SUSPEND definition: the asserting task will be suspended */
#define ASSERT_SUSPEND
#define FILESYS_8_3 /* 8.3 filenames */

/*-TYPEDEFS------------------------------------------------------------------*/
typedef va_list             EC_T_VALIST;
typedef unsigned long long  EC_T_UINT64;
typedef signed long long    EC_T_INT64;

/*-MACROS--------------------------------------------------------------------*/
/* define here if tracing shall be enabled */
/*#define DEBUGTRACE*/

#define EC_INLINEATTRIBUTE
#define EC_INLINE(funcdecl) static inline funcdecl

#ifdef __cplusplus
extern "C"
{
#endif

/* resolve namespace conflict with Profinet stack */
#define OsSetEvent     EcOsSetEvent
#define OsCreateEvent  EcOsCreateEvent
#define OsWaitForEvent EcOsWaitForEvent
ATECAT_API EC_T_DWORD  EcOsWaitForEvent(EC_T_VOID* pvEvent, EC_T_DWORD dwTimeout);
#define OsCreateThread EcOsCreateThread
typedef EC_T_VOID (*EC_PF_THREADENTRY_SYSBIOS)(EC_T_VOID* pvParams);
ATECAT_API EC_T_VOID*  EcOsCreateThread(EC_T_CHAR* szThreadName, EC_PF_THREADENTRY_SYSBIOS pfThreadEntry, EC_T_DWORD dwPrio, EC_T_DWORD dwStackSize, EC_T_VOID* pvParams);
#define OsInit         EcOsInit

/* SYSBIOS specific functions */

static inline FILE* OsFopen(EC_T_VOID) { return EC_NULL; }
static inline int OsReturns0(EC_T_VOID) { return 0; }

#define OsFopen(szFilename, szOpenType) OsFopen()
#define OsFclose(fpFile) OsReturns0()
#define OsFwrite(ptr, size, count, fpFile) OsReturns0()
#define OsFread(pDstBuf,dwElemSize,dwCnt,hFile) OsReturns0()
#define OsFflush(hFile) OsReturns0()
#define OsGetFileSize OsReturns0()

EC_T_VOID SysBiosVprintf(const EC_T_CHAR* szFormat, EC_T_VALIST vaArgs);
EC_T_DWORD SysBiosPrintf(const char *_format, ...);

#define OsVprintf SysBiosVprintf
#define OsPrintf SysBiosPrintf

/* use macros for the most important OS layer routines */
#define OsSetLastError(dwError)			dwError

void NoOsSleep(EC_T_DWORD dwMsec);
#define OsSleep(dwMsec)                 NoOsSleep(dwMsec)

#if (defined DEBUG)
#  ifdef ASSERT_SUSPEND
#    define OsDbgAssert(bCond)                      OsDbgAssertFunc((bCond),(EC_T_CHAR*)__FILE__,__LINE__)
#  else
#    define OsDbgAssert                             assert
#  endif
#endif

#ifdef ASSERT_SUSPEND
EC_T_VOID OsDbgAssertFunc(EC_T_BOOL bAssertCondition, EC_T_CHAR* szFile, EC_T_DWORD dwLine);
#endif

#ifndef OsDbgAssert
#  define OsDbgAssert(x)
#endif

EC_T_VOID EcOsSetEvent(EC_T_VOID* pvEvent);
#define OsSetEvent EcOsSetEvent

#define OsMemoryBarrier OsMemoryBarrier
static inline void OsMemoryBarrier()
{
    asm (" dsb"); /* ARM v7 instruction (e.g. Cortex) */
}

#define OsStricmp  OsStrcmp

#define OsAuxClkInit OsAuxClkInit
#define OsAuxClkDeinit OsAuxClkDeinit
EC_T_DWORD OsAuxClkInit( EC_T_DWORD dwCpuIndex, EC_T_DWORD dwFrequencyHz, EC_T_VOID* pvOsEvent );
EC_T_VOID OsAuxClkDeinit(EC_T_VOID);

#define OsTerminateAppRequest OsTerminateAppRequest
EC_T_BOOL OsTerminateAppRequest(EC_T_VOID);


/* optional: redirect trace messages into OS specific function, e.g. to store trace log into file 
 * default: print trace log as debug message
 */
#ifdef DEBUGTRACE
#  define OsTrcMsg OsTrcMsg
   ATECAT_API  EC_T_VOID   OsTrcMsg(const EC_T_CHAR* szFormat, ...);
#endif

#define EC_NEW(x) new x

#define EC_VASTART              va_start
#define EC_VAEND                va_end
#define EC_VAARG                va_arg

/* CPU set not really used, just implement something to avoid warnings */
#define EC_CPUSET_DEFINED
typedef int EC_T_CPUSET;
#define     EC_CPUSET_ZERO(CpuSet) {CpuSet=0;}
#define     EC_CPUSET_SET(CpuSet,nCpuIndex) {CpuSet=nCpuIndex;}
#define     EC_CPUSET_SETALL(CpuSet) {CpuSet=CpuSet;}

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* INC_ECOSPLATFORM */

/*-END OF SOURCE FILE--------------------------------------------------------*/

