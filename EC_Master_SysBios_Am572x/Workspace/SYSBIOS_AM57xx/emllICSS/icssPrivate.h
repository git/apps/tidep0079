/*-----------------------------------------------------------------------------
 * icssPrivate.h            Header file
 * Copyright                acontis technologies GmbH, Weingarten, Germany
 * Response                 Kai Olbrich
 * Description              Register Description for Texas Instruments CPSW
 *---------------------------------------------------------------------------*/

#ifndef INC_ICSSPRIVATE_H
#define INC_ICSSPRIVATE_H

//
// Common defines
//

//PC-- from CPSWPrivate.h
#define BUFFER_MAGIC_RX        0xBEEFBABE
#define BUFFER_MAGIC_TX        0xDEADBEEF
#define ICSS_RXDESCNT          MAX_NUM_BUF                    // Receive descriptor buffer count (!!must be power of 2, max 256!!)
#define ICSS_RXBUFLEN          BUF_SIZE                   // RX buffer length (!!should be power of 2!!)
#define ICSS_TXDESCNT          MAX_NUM_BUF                    // Transmit descriptor buffer count (!!must be power of 2, max 256!!)
#define ICSS_TXBUFLEN          BUF_SIZE                   // TX buffer length (!!should be power of 2!!)

typedef struct _ICSS_SLIVER_REGS
{
  volatile EC_T_DWORD   SL_IDVER;				// 0x00 CPGMAC_SL ID/Version
  volatile EC_T_DWORD   SL_MacControl;			// 0x04 CPGMAC_SL Mac Control
  volatile EC_T_DWORD   SL_MacStatus;			// 0x08 CPGMAC_SL Mac Status
  volatile EC_T_DWORD   SL_Soft_Reset;			// 0x0c CPGMAC_SL Soft Reset
  volatile EC_T_DWORD   SL_Rx_Maxlen;			// 0x10 CPGMAC_SL Receive Maximum Length
  volatile EC_T_DWORD   SL_BoffTest;			// 0x14 CPGMAC_SL Backoff Test Register
  volatile EC_T_DWORD   SL_Rx_Pause;			// 0x18 CPGMAC_SL Receive Pause Timer Register
  volatile EC_T_DWORD   SL_Tx_Pause;	 		// 0x1c CPGMAC_SL Transmit Pause Timer Register
  volatile EC_T_DWORD   SL_EMControl;			// 0x20 CPGMAC_SL Emulation Control
  volatile EC_T_DWORD   SL_Rx_Pri_Map; 		// 0x24 CPGMAC_SL Rx Pkt Pri to Header Pri Mapping
} ICSS_SLIVER_REGS, *PICSS_SLIVER_REGS;

typedef struct _ICSS_SLAVE_REGS
{
  volatile EC_T_DWORD   Max_Blks;			// CPSW_3G Port Maximum FIFO blocks Register
  volatile EC_T_DWORD   BLK_CNT;			   // CPSW_3G Port FIFO Block Usage Count (read only)
  volatile EC_T_DWORD   Tx_In_Ctl;			// CPSW_3G Port Transmit FIFO Control
  volatile EC_T_DWORD   Port_VLAN;			// CPSW_3G Port VLAN Register
  volatile EC_T_DWORD   Tx_Pri_Map;			// CPSW_3G Port Tx Header Pri to Switch Pri Mapping Register
  volatile EC_T_DWORD   TS_CTL;				// CPSW_3G Port Time Sync Control Register
  volatile EC_T_DWORD   TS_SEQ_LTYPE;		// CPSW_3G Port Time Sync LTYPE (and SEQ_ID_OFFSET)
  volatile EC_T_DWORD   TS_VLAN;			   // CPSW_3G Port Time Sync VLAN2 and VLAN2 Register
  volatile EC_T_DWORD   SL_SA_LO;	 		// CPSW_3G CPGMAC_SL Source Address Low Register
  volatile EC_T_DWORD   SL_SA_HI;  			// CPSW_3G CPGMAC_SL Source Address High Register
  volatile EC_T_DWORD   Send_Percent;		// CPSW_3G Port Transmit Queue Send Percentages
} ICSS_SLAVE_REGS, *PICSS_SLAVE_REGS;


#endif /* INC_ICSSPRIVATE_H */
