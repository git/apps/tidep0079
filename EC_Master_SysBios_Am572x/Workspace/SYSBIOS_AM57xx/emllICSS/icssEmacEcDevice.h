/*-----------------------------------------------------------------------------
 * iccs_emac_ec_device.h    header file
 * Description              Internal used types and structures for ICSS EMAC
 *                          Ethercat LinkLayer
 *---------------------------------------------------------------------------*/

#ifndef INC_ICCSEMACECDEVICE_H
#define INC_ICCSEMACECDEVICE_H

//Comment this to use in non-TTS mode.
#ifndef	TTS
#define	TTS
#endif

/*-INCLUDE-------------------------------------------------------------------*/
#include <ti/drv/icss_emac/icss_emacDrv.h>
#ifndef __cplusplus

#include "LinkOsLayer.h"
#include "icssPrivate.h"
#include <ti/sysbios/timers/dmtimer/Timer.h>

/*-DEFINES-------------------------------------------------------------------*/

/*-TYPEDEFS/ENUMS------------------------------------------------------------*/
typedef struct _T_ICSS_EC_DESC
{
   EC_T_DWORD dwNext;   // Next Descriptor Pointer
   EC_T_DWORD dwBuffer; // Buffer Pointer
   EC_T_DWORD dwLen;    // Buffer Offset | Buffer Length
   EC_T_DWORD dwMode;   // Flags | Packet Length
} T_ICSS_EC_DESC, *PT_ICSS_EC_DESC;

// CPSW hardware DMA descriptor structure
typedef struct _T_CPSW_DESC
{
   EC_T_DWORD dwNext;   // Next Descriptor Pointer
   EC_T_DWORD dwBuffer; // Buffer Pointer
   EC_T_DWORD dwLen;    // Buffer Offset | Buffer Length
   EC_T_DWORD dwMode;   // Flags | Packet Length
} T_CPSW_DESC, *PT_CPSW_DESC;

typedef enum
{
  BS_FREE = 0,
  BS_ALLOC,
  BS_DMA_STARTED,
} T_CPSW_BUFSTATE;

// Buffer management entry. Size is 16 byte in order to make debugging more comfortable.
typedef struct _T_CPSW_BUFENTRY
{
   EC_T_DWORD         dwMagicKey;    // Magic key for sanity checks.
   T_CPSW_BUFSTATE    eBufState;
   EC_T_DWORD         dwDescIdx;     // Assigned T_CPSW_DESC descriptor index (set during send operation)
   EC_T_DWORD         dwFrameLen;    // Debugging: This info is also stored in the DMA descriptor
} T_CPSW_BUFENTRY, *PT_CPSW_BUFENTRY;
#define CPSW_BUFENTRY_SIZE 16 // Constant expression, because VS preprocessor doesn't understand sizeof()

// Helper acccess structure (used for casting raw memory)
#if (CACHELINE_SIZE > CPSW_BUFENTRY_SIZE)
#  define CPSW_BUFENTRY_FRAME_OFFSET CACHELINE_SIZE
#else
#  define CPSW_BUFENTRY_FRAME_OFFSET CPSW_BUFENTRY_SIZE
#endif
typedef struct _T_CPSW_FRAMEBUFENTRY
{
   T_CPSW_BUFENTRY bufentry;
#if (CPSW_BUFENTRY_FRAME_OFFSET != CPSW_BUFENTRY_SIZE)
   EC_T_BYTE       abyCacheAlignmentBuffer[CPSW_BUFENTRY_FRAME_OFFSET - CPSW_BUFENTRY_SIZE];
#endif
   EC_T_BYTE       pFrame[1];     // Framebuffer
} T_CPSW_FRAMEBUFENTRY, *PT_CPSW_FRAMEBUFENTRY;

typedef struct _T_ICSS_EC_INTERNAL *PT_ICSS_EC_INTERNAL;
typedef struct _ICSS_EC_REGS *ICSS_EC_REGS;
typedef struct _ICSS_EC_HOST_REGS *ICSS_EC_HOST_REGS;
typedef struct _ICSS_EC_ALE_REGS *ICSS_EC_ALE_REGS;
typedef struct _ICSS_EC_CPDMA_REGS *ICSS_EC_CPDMA_REGS;
typedef struct _ICSS_EC_STATERAM_REGS *ICSS_EC_STATERAM_REGS;
typedef struct _ICSS_EC_SS_REGS *ICSS_EC_SS_REGS;
typedef struct _ICSS_EC_MDIO_REGS *ICSS_EC_MDIO_REGS;
typedef struct _ICSS_EC_SLAVE_REGS *ICSS_EC_SLAVE_REGS;
typedef struct _ICSS_EC_SLIVER_REGS *ICSS_EC_SLIVER_REGS;

typedef struct _ICSS_EC_REGSET
{
	   //PC-- For the moment I used same Reg as in ICSS_EmacBaseAddressHandle_T
	   EC_T_DWORD dwPrussMiiMdioRegs;   /*! MDIO Base register */
	   EC_T_DWORD dwDataRam0BaseAddr;   /*! DataRam 0 Base Address */
	   EC_T_DWORD dwDataRam1BaseAddr;   /*! DataRam 0 Base Address */
	   EC_T_DWORD dwL3OcmcBaseAddr;     /*! L3 OCMC Base Address */
	   EC_T_DWORD dwSharedDataRamBaseAddrs;    /*! PRUSS Shared RAM Base Address */
	   EC_T_DWORD dwPrussIntcRegs;      /*! Pruss INTC Register Base Address */
	   EC_T_DWORD dwPrussPru0CtrlRegs;  /*! PRU0 Control register Base Address */
	   EC_T_DWORD dwPrussPru1CtrlRegs;  /*! PRU1 Control register Base Address */
	   EC_T_DWORD dwPrussIepRegs;       /*! PRUSS IEP register Base Address */
	   EC_T_DWORD dwPrussCfgRegs;       /*! Pruss CFG register Base Address */
	   EC_T_DWORD dwPrussMiiRtCfgRegsBaseAddr;  /*! MII RT Config register Base Address */

} ICSS_EC_REGSET, *PICSS_EC_REGSET;

typedef struct _T_ICSS_EC_INTERNAL
{
   // Misc
   EC_T_DWORD                  dwSignature;
   EC_T_LINK_PARMS_ICSS        oInitParms;
   EC_T_RECEIVEFRAMECALLBACK   pfReceiveFrameCallback; //PC-- TODO: check if this is required or not due to demo working on RX polling mode only
   EC_T_VOID*                  pvCallbackContext;
   ICSS_EC_REGSET              regs; // Register set
   T_PCIINFO                   oCardInfo; // PCI info
   EC_T_DWORD                  dwPhyFeatures;
   EC_T_DWORD                  oAdapterObject;
   EC_T_DWORD                  dwIrqEvent; // storage for interrupt events
   EC_T_LINKOS_IRQ_PARM        oIrqParms; // IRQ handling
   EC_T_DWORD                  dwLosalHandle;
   EC_T_DWORD                  dwLinkStatus;

   // RX
   volatile T_ICSS_EC_DESC*       pRxDesc; // Descriptor base address (len: dwRxDescSize byte)
   volatile T_ICSS_EC_DESC*       pRxTailDesc; // Point to last dtor (nextp == NULL)
   EC_T_DWORD                  dwRxDescSize; // Size of all RX descriptors (ICSS_EC_RXDESCNT * sizeof(T_ICSS_EC_DESC)) in byte
   volatile EC_T_BYTE*         pRxBuffer; // Buffer base address (len: dwRxBufferSize byte)
   EC_T_DWORD                  dwRxBufferSize; // Size of all RX buffers (ICSS_EC_RXDMABUFCNT * ICSS_EC_RXBUFLEN) in byte
   EC_T_DWORD                  dwRxDescIdx; // Ringbuffer index (RX T_ICSS_EC_BUFENTRY)
   EC_T_DWORD                  dwRxBufIdx; // Ringbuffer index (RX buffers)
#if defined COPY_FRAME
   EC_T_BYTE*                  pRxCachedBuffer; // Cached buffer base address (len: dwRxCachedBufferSize byte)
   EC_T_DWORD                  dwRxCachedBufferSize; // Size of all cached RX buffers (ICSS_EC_RXCACHEDBUFCNT * ICSS_EC_RXBUFLEN) in byte
   EC_T_BYTE*                  pRxCachedBufferOriginal; // buffer address which was returned from malloc, but which can be not alligned
#endif

   // TX
   volatile T_ICSS_EC_DESC*       pTxDesc; // Pointer to dtor list
   volatile T_ICSS_EC_DESC*       pTxTailDesc; // Last processed dtor
   EC_T_DWORD                  dwTxDescSize; // Size of all TX descriptors (ICSS_EC_TXDESCNT * sizeof(T_ICSS_EC_DESC)) in byte
   volatile EC_T_BYTE*         pTxBuffer; // Buffer base address (len: dwTxBufferSize byte)
   EC_T_DWORD                  dwTxBufferSize; // Size of all TX buffers (ICSS_EC_TXDMABUFCNT * ICSS_EC_TXBUFLEN) in byte
   EC_T_DWORD                  dwTxDescIdx; // Next free TX Ringbuffer index (TX T_ICSS_EC_BUFENTRY)
   EC_T_DWORD                  dwTxBufIdx; // Ringbuffer index (TX buffers)
#if defined COPY_FRAME
   EC_T_BYTE*                  pTxCachedBuffer; // Cached buffer base address (len: dwTxCachedBufferSize byte)
   EC_T_DWORD                  dwTxCachedBufferSize; // Size of all cached TX buffers (ICSS_EC_TXCACHEDBUFCNT * ICSS_EC_TXBUFLEN) in byte
   EC_T_BYTE*                  pTxCachedBufferOriginal; // buffer address which was returned from malloc, but which can be not alligned
#endif

   // Instance Mgt
   PT_ICSS_EC_INTERNAL            pPrev;
   PT_ICSS_EC_INTERNAL            pNext;

} T_ICSS_EC_INTERNAL, *PT_ICSS_EC_INTERNAL;

//PC-- Below Added for a RX buffer Queue
/* Queue structure - RX circular buffer*/
#define MAX_NUM_BUF 16
#define BUF_SIZE 1024
#define ERROR_QUEUE   2
#define ERROR_MEMORY  3

typedef struct _BufferQueue
{
	EC_T_BYTE* const buffer[MAX_NUM_BUF*BUF_SIZE];
	EC_T_INT NumWriteBuf;
    EC_T_INT NumReadBuf;
	EC_T_INT PktLength[MAX_NUM_BUF];
    EC_T_INT head;
    EC_T_INT NumAllocBuf;

} BufferQueue, *PT_BUF_QUEUE;

/*
 * Function: QueueCreate
 * A new empty queue is created and returned.
 */
PT_BUF_QUEUE QueueBufCreate(void);

/* Function: QueueDestroy
 * Function frees all memory associated with the queue.
 */
void QueueBufDestroy(PT_BUF_QUEUE BufQueue);

/*
 * Functions: QueueBufIsEmpty, QueueBufIsFull
 * Return a true/false value based on whether the queue is empty or full, respectively.
 */
EC_T_INT QueueBufIsEmpty(PT_BUF_QUEUE BufQueue);
EC_T_INT QueueBufIsFull(PT_BUF_QUEUE BufQueue);

/*
 * Functions: QueueFreeBuf
 * Free a Buffer in FIFO Queue. Return if Buffer was "read" by upperlayer
 */
EC_T_INT QueueFreeBuf(PT_BUF_QUEUE BufQueue);

/*
 * Functions: QueueAllocBuf
 * Return a pointer to a free Buffer in FIFO Queue.
 */
EC_T_BYTE* QueueAllocBuf(PT_BUF_QUEUE BufQueue);

PT_CPSW_FRAMEBUFENTRY FrameBufEntryCreate(void);
#endif	//__cplusplus

#ifdef __cplusplus
extern "C"
{
#endif
#ifdef TTS
//Required for TTS.
extern ICSS_EmacHandle emachandle2;
extern ICSS_EmacHandle emachandle3;
//extern int8_t TTSSemCreate();
extern SemaphoreP_Handle ttsP1TxSem;
extern SemaphoreP_Handle ttsP2TxSem;
uint32_t deInitTTS(ICSS_EmacHandle icssEmacHandle);
EC_T_BYTE initTTS(ICSS_EmacHandle icssEmacHandle, uint32_t cyclePeriod);
#endif
#ifdef __cplusplus
} /* extern "C"*/
#endif
//PC-- BOARD_ICSS_EMAC_PORTX_PHY_ADDR are defined <PDK_INSTALL_PATH>\packages\ti\board\src\idkAM572x\include\board_cfg.h, but no sure why I am getting undeclare error. Adding them here
/* ICSS EMAC  PHY address definitions */
#define BOARD_ICSS_EMAC_PORT0_PHY_ADDR       (0U)
#define BOARD_ICSS_EMAC_PORT1_PHY_ADDR       (1U)

#define ICSS_EMAC_TEST_TIMER_ID                    7 /* GPTIMER8 */
#define ICSS_EMAC_TEST_TIMER_FREQUENCY      19200000
#define ICSS_EMAC_TEST_TIMER_PERIOD    100


#endif // INC_ICCSEMACECDEVICE_H
