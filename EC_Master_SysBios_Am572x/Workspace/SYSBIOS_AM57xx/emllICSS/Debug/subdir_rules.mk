################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
build-694325103:
	@$(MAKE) -Onone -f subdir_rules.mk build-694325103-inproc

build-694325103-inproc: C:/GIT_TIdesign/tidep0079/EC_Master_SysBios_Am572x/Workspace/SYSBIOS_AM57xx/EcMasterDemo/EcMaster.cfg
	@echo 'Building file: $<'
	@echo 'Invoking: XDCtools'
	"C:/TI/xdctools_3_50_03_33_core/xs" --xdcpath="C:/TI/bios_6_52_00_12/packages;C:/TI/pdk_am57xx_1_0_10/packages;C:/TI/ccsv7/ccs_base;" xdc.tools.configuro -o configPkg -t gnu.targets.arm.A15F -p ti.platforms.idkAM572X -r debug -b "C:/TI/pdk_am57xx_1_0_10/packages/ti/build/am572x/config_am572x_a15.bld" -c "C:/TI/gcc-arm-none-eabi-6-2017-q1-update" -DBOARD=idkAM572x "$<"
	@echo 'Finished building: $<'
	@echo ' '

configPkg/linker.cmd: C:/TI/pdk_am57xx_1_0_10/packages/ti/build/am572x/config_am572x_a15.bld
configPkg/linker.cmd: build-694325103 C:/GIT_TIdesign/tidep0079/EC_Master_SysBios_Am572x/Workspace/SYSBIOS_AM57xx/EcMasterDemo/EcMaster.cfg C:/TI/pdk_am57xx_1_0_10/packages/ti/build/am572x/config_am572x_a15.bld
configPkg/compiler.opt: build-694325103 C:/TI/pdk_am57xx_1_0_10/packages/ti/build/am572x/config_am572x_a15.bld
configPkg/: build-694325103 C:/TI/pdk_am57xx_1_0_10/packages/ti/build/am572x/config_am572x_a15.bld

TimerP_tirtos.o: C:/TI/pdk_am57xx_1_0_10/packages/ti/osal/src/tirtos/TimerP_tirtos.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: GNU Compiler'
	"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/bin/arm-none-eabi-gcc.exe" -c -mcpu=cortex-a15 -mtune=cortex-a15 -marm -mfloat-abi=hard -DSOC_AM572x -DAM5XX_FAMILY_BUILD -DMEM_BARRIER_DISABLE -I"C:/TI/pdk_am57xx_1_0_10/packages" -I"C:/TI/pdk_am57xx_1_0_10/packages/ti/drv/icss_emac/src" -I"C:/TI/pdk_am57xx_1_0_10/packages/ti/drv/icss_emac" -I"../../../../SDK/INC/SYSBIOS" -I"../../../../SDK/INC" -I"../../../../Sources/LinkOsLayer/sysbios" -I"../../../../Sources/LinkOsLayer" -I"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/arm-none-eabi/include" -I"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/arm-none-eabi/include/newlib-nano" -O2 -g -gdwarf-3 -gstrict-dwarf -Wall -finstrument-functions -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o"$@" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

UART_soc.o: C:/TI/pdk_am57xx_1_0_10/packages/ti/drv/uart/soc/am572x/UART_soc.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: GNU Compiler'
	"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/bin/arm-none-eabi-gcc.exe" -c -mcpu=cortex-a15 -mtune=cortex-a15 -marm -mfloat-abi=hard -DSOC_AM572x -DAM5XX_FAMILY_BUILD -DMEM_BARRIER_DISABLE -I"C:/TI/pdk_am57xx_1_0_10/packages" -I"C:/TI/pdk_am57xx_1_0_10/packages/ti/drv/icss_emac/src" -I"C:/TI/pdk_am57xx_1_0_10/packages/ti/drv/icss_emac" -I"../../../../SDK/INC/SYSBIOS" -I"../../../../SDK/INC" -I"../../../../Sources/LinkOsLayer/sysbios" -I"../../../../Sources/LinkOsLayer" -I"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/arm-none-eabi/include" -I"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/arm-none-eabi/include/newlib-nano" -O2 -g -gdwarf-3 -gstrict-dwarf -Wall -finstrument-functions -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o"$@" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

icss_emacFwConfig.o: C:/TI/pdk_am57xx_1_0_10/packages/ti/drv/icss_emac/firmware/icss_dualemac/config/icss_emacFwConfig.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: GNU Compiler'
	"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/bin/arm-none-eabi-gcc.exe" -c -mcpu=cortex-a15 -mtune=cortex-a15 -marm -mfloat-abi=hard -DSOC_AM572x -DAM5XX_FAMILY_BUILD -DMEM_BARRIER_DISABLE -I"C:/TI/pdk_am57xx_1_0_10/packages" -I"C:/TI/pdk_am57xx_1_0_10/packages/ti/drv/icss_emac/src" -I"C:/TI/pdk_am57xx_1_0_10/packages/ti/drv/icss_emac" -I"../../../../SDK/INC/SYSBIOS" -I"../../../../SDK/INC" -I"../../../../Sources/LinkOsLayer/sysbios" -I"../../../../Sources/LinkOsLayer" -I"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/arm-none-eabi/include" -I"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/arm-none-eabi/include/newlib-nano" -O2 -g -gdwarf-3 -gstrict-dwarf -Wall -finstrument-functions -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o"$@" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

icss_emacSoc.o: C:/TI/pdk_am57xx_1_0_10/packages/ti/drv/icss_emac/soc/am572x/icss_emacSoc.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: GNU Compiler'
	"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/bin/arm-none-eabi-gcc.exe" -c -mcpu=cortex-a15 -mtune=cortex-a15 -marm -mfloat-abi=hard -DSOC_AM572x -DAM5XX_FAMILY_BUILD -DMEM_BARRIER_DISABLE -I"C:/TI/pdk_am57xx_1_0_10/packages" -I"C:/TI/pdk_am57xx_1_0_10/packages/ti/drv/icss_emac/src" -I"C:/TI/pdk_am57xx_1_0_10/packages/ti/drv/icss_emac" -I"../../../../SDK/INC/SYSBIOS" -I"../../../../SDK/INC" -I"../../../../Sources/LinkOsLayer/sysbios" -I"../../../../Sources/LinkOsLayer" -I"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/arm-none-eabi/include" -I"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/arm-none-eabi/include/newlib-nano" -O2 -g -gdwarf-3 -gstrict-dwarf -Wall -finstrument-functions -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o"$@" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

%.o: ../%.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: GNU Compiler'
	"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/bin/arm-none-eabi-gcc.exe" -c -mcpu=cortex-a15 -mtune=cortex-a15 -marm -mfloat-abi=hard -DSOC_AM572x -DAM5XX_FAMILY_BUILD -DMEM_BARRIER_DISABLE -I"C:/TI/pdk_am57xx_1_0_10/packages" -I"C:/TI/pdk_am57xx_1_0_10/packages/ti/drv/icss_emac/src" -I"C:/TI/pdk_am57xx_1_0_10/packages/ti/drv/icss_emac" -I"../../../../SDK/INC/SYSBIOS" -I"../../../../SDK/INC" -I"../../../../Sources/LinkOsLayer/sysbios" -I"../../../../Sources/LinkOsLayer" -I"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/arm-none-eabi/include" -I"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/arm-none-eabi/include/newlib-nano" -O2 -g -gdwarf-3 -gstrict-dwarf -Wall -finstrument-functions -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o"$@" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


