################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
eni/%.o: ../eni/%.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: GNU Compiler'
	"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/bin/arm-none-eabi-gcc.exe" -c -mcpu=cortex-a15 -mtune=cortex-a15 -marm -mfloat-abi=hard -DLINKLAYER_ICSS -Dam5728 -DSOC_AM572x -DAM5XX_FAMILY_BUILD -DMEM_BARRIER_DISABLE -Dcore0 -I"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/arm-none-eabi/include" -I"/\"/\"/packages/ti/drv/icss_emac/\"" -I"../../../../SDK/INC" -I"../../../../SDK/INC/SYSBIOS" -I"../../../../Sources/LinkOsLayer" -I"../../../../Sources/LinkOsLayer/sysbios" -I"../../../../Sources/Common" -O2 -Wall -finstrument-functions -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o"$@" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


