################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
eni/%.o: ../eni/%.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: GNU Compiler'
	"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/bin/arm-none-eabi-gcc.exe" -c -mcpu=cortex-a15 -mtune=cortex-a15 -marm -mfloat-abi=hard -DLINKLAYER_ICSS -DDEBUG -Dam5728 -DSOC_AM572x -DAM5XX_FAMILY_BUILD -DMEM_BARRIER_DISABLE -Dcore0 -DidkAM572x -I"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/arm-none-eabi/include" -I"C:/TI/gcc-arm-none-eabi-6-2017-q1-update/arm-none-eabi/include/newlib-nano" -I"C:/TI/pdk_am57xx_1_0_10/packages" -I"../../../../SDK/INC" -I"../../../../SDK/INC/SYSBIOS" -I"../../../../Sources/LinkOsLayer" -I"../../../../Sources/LinkOsLayer/sysbios" -I"../../../../Sources/Common" -O2 -g -gdwarf-3 -gstrict-dwarf -Wall -v -finstrument-functions -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o"$@" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


