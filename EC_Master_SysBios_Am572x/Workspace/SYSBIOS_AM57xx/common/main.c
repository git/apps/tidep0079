/*-----------------------------------------------------------------------------
 * main.c
 * Copyright                acontis technologies GmbH, Weingarten, Germany
 * Description              EtherCAT Master demo application
 *---------------------------------------------------------------------------*/


#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/timers/dmtimer/Timer.h>
#include <ti/sysbios/hal/Cache.h>

/* EcMaster includes */
#include <EcVersion.h>
#include <EcOs.h>

/* includes from demo  */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ti/sysbios/family/arm/gic/Hwi.h>

#include <ti/csl/soc.h>
#include <ti/csl/cslr_device.h> 
#include <ti/board/board.h> 


/* TI-RTOS Header files */
#include <ti/drv/i2c/I2C.h>
#include <ti/drv/i2c/soc/I2C_v1.h>

/* UART Header files */
#include <ti/drv/uart/UART.h>
#include <ti/drv/uart/UART_stdio.h>

//PC-- 03/14/2017
#include <ti/drv/icss_emac/test/src/test_common_utils.h>
//uint8_t board_type = 0;
/* either idkAM572x or idkAM571x */
uint8_t ICSS_EMAC_testEvmType = 0;

/* PG version of EVM */
uint32_t ICSS_EMAC_testPgVersion = 0;

#define SLEEP Task_sleep
#define PRINT UART_printf

/********************************************************************************/
/** \brief Puts string to the UART and replace "\n" to "\n\r"
*
* \return N/A
*/
static int UARTPutStringWithCR(char* pStr, int maxLength)
{
    int symCount = 0;
    char* pCurrent = pStr;

    while (*pCurrent != 0)
    {
        Char curChar = *pCurrent;
        if ( '\n' == curChar )
        {
        	UART_putc('\n');
        	UART_putc('\r');
        }
        else
        	UART_putc(curChar);

        symCount++;

        pCurrent++;
    }

    return symCount;
}

/********************************************************************************/
/** \brief Puts string to the UART
*
* \return printed symbols count
*/
#define MAX_TRACE_MSGLEN 255
int UARTVprintf(const char *szFormat, va_list vaArgs)
{
    char achMsg[MAX_TRACE_MSGLEN];
    vsnprintf(achMsg, MAX_TRACE_MSGLEN, szFormat, vaArgs);

    return UARTPutStringWithCR(achMsg, MAX_TRACE_MSGLEN);
}


/* Enable the below macro to have prints on the IO Console */
//#define IO_CONSOLE

#ifndef IO_CONSOLE
#define NIMU_log                UART_printf
#else
#define NIMU_log                printf
#endif

/* ========================================================================== */
/*                             Macros                                  */
/* ========================================================================== */

/**Phy address of the CPSW port 1*/
	#define CPSW_PORT0_PHY_ADDR_IDK 	0
/**Phy address of the CPSW port 1*/
	#define CPSW_PORT1_PHY_ADDR_IDK 	1

#define GMAC_SW_IRQ_RX_PULSE_INT_NUM         (92 + 32)
#define GMAC_SW_IRQ_TX_PULSE_INT_NUM         (93 + 32)
#define GMAC_SW_IRQ_RX_THRESH_PULSE_INT_NUM  (94 + 32)
#define GMAC_SW_IRQ_MISC_PULSE_INT_NUM       (95 + 32)

#define MAX_TABLE_ENTRIES   3



/**Task handle for EIP*/
Task_Handle main_task;


//NIMU_DEVICE_TABLE_ENTRY NIMUDeviceTable[MAX_TABLE_ENTRIES];

void TaskFxn(UArg a0, UArg a1);
uint32_t ClockSynthesizerSetup(void);
extern const I2C_Config I2C_config[];

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */


void CSL_xbarMpuIrqConfigure(Uint32 irqNumIdx, CSL_XbarIrq xbarIrq)
{
    CSL_control_intr_dmaRegs *ctrlCoreIntrDmaReg =
        (CSL_control_intr_dmaRegs *) CSL_MPU_IRQ_DMARQ_CROSSBAR_REGISTERS_REGS;
    CSL_control_coreRegs *ctrlCoreReg =
        (CSL_control_coreRegs *) CSL_MPU_CTRL_MODULE_CORE_CORE_REGISTERS_REGS;
    Uint32 regIdx = (irqNumIdx - 1U) / 2U;
    Uint32 regLsb = (((irqNumIdx - 1U) % 2U) * 16U);
    Uint32 regMsb = regLsb + 16U;

    if((CSL_XBAR_IRQ_MIN < xbarIrq) && (CSL_XBAR_IRQ_MAX > xbarIrq))
    {
        if ((0U < irqNumIdx) && (CSL_MPU_IRQ_XBAR_COUNT >= irqNumIdx))
        {
            ctrlCoreReg->MMR_LOCK_2 = 0xF757FDC0U;
            CSL_FINSR(ctrlCoreIntrDmaReg->MPU_IRQ[regIdx],
                regMsb, regLsb, xbarIrq);
            ctrlCoreReg->MMR_LOCK_2 = 0xFDF45530U;
        }
    }
}
#define BOARD_IDKAM571x     1
#define BOARD_IDKAM572x     2

/**
 *  \name main
 *  \brief Main Function
 *  \param none
 *  \return none
 *
 */
int main()
{
    /* Call board init functions */

    Error_Block eb;
    Task_Params taskParams;
    Board_IDInfo info;
    Board_STATUS boardInitStatus =0;

    Board_initCfg cfg = BOARD_INIT_UART_STDIO | BOARD_INIT_PINMUX_CONFIG | BOARD_INIT_MODULE_CLOCK | BOARD_INIT_ICSS_ETH_PHY | BOARD_INIT_UNLOCK_MMR;

    Error_init(&eb);
    boardInitStatus  = Board_init(cfg);

    if (boardInitStatus !=0)
    {
        printf("Board_init failure\n");
        return(0);
    }

    memset(&info, 0,sizeof(Board_IDInfo));
    Board_getIDInfo(&info);
    PRINT("boardName: %s\n", info.boardName);
    if (!(strcmp(info.boardName, "AM571IDK")))
    {
        ICSS_EMAC_testEvmType =ICSS_EMAC_TEST_BOARD_IDKAM571x;
    }
    else if(!(strcmp(info.boardName, "AM572IDK")))
    {
       ICSS_EMAC_testEvmType =ICSS_EMAC_TEST_BOARD_IDKAM572x;
    }
    else
    {
        ICSS_EMAC_testEvmType =ICSS_EMAC_TEST_BOARD_IDKAM574x;
     }


    CSL_xbarMpuIrqConfigure(CSL_XBAR_INST_MPU_IRQ_92, CSL_XBAR_GMAC_SW_IRQ_RX_PULSE);
    CSL_xbarMpuIrqConfigure(CSL_XBAR_INST_MPU_IRQ_93, CSL_XBAR_GMAC_SW_IRQ_TX_PULSE);

      /* Select RGMII 2 ports GMIIx_SEL = 2 for RGMII*/
	  CSL_FINS (((CSL_control_coreRegs *) CSL_MPU_CTRL_MODULE_CORE_CORE_REGISTERS_REGS)->CONTROL_IO_1,
	      CONTROL_CORE_CONTROL_IO_1_GMII1_SEL, 2U);
	  CSL_FINS (((CSL_control_coreRegs *) CSL_MPU_CTRL_MODULE_CORE_CORE_REGISTERS_REGS)->CONTROL_IO_1,
	      CONTROL_CORE_CONTROL_IO_1_GMII2_SEL, 2U);

      /*GMAC RESET ISOLATION Enable*/
	  CSL_FINS (((CSL_control_coreRegs *) CSL_MPU_CTRL_MODULE_CORE_CORE_REGISTERS_REGS)->CONTROL_IO_2,
	      CONTROL_CORE_CONTROL_IO_2_GMAC_RESET_ISOLATION_ENABLE, 0U);
	  CSL_FINS (((CSL_control_coreRegs *) CSL_MPU_CTRL_MODULE_CORE_CORE_REGISTERS_REGS)->CONTROL_IO_2,
	      CONTROL_CORE_CONTROL_IO_2_GMAC_RESET_ISOLATION_ENABLE, 1U);


    Task_Params_init(&taskParams);
    taskParams.priority = 1;
    taskParams.stackSize = 0x1400;
    taskParams.instance->name = "MainTask";
    main_task = Task_create (TaskFxn, &taskParams, &eb);


    BIOS_start();
    return(0);
}

/*****************************************************************************/
/** \brief Auxiliary clock timer instance.
 *
 * Used inside EcMaster code for auxiliary clock.
 * If instance is NULL, than standard clock is used (not precise).
*/
Timer_Handle g_auxClocksTimerHandle = 0;

/*****************************************************************************/
/** \brief Initializes timer instance which is used from auxiliary clock.
*/
void InitAuxClockTimer()
{
//    const UInt DMTimer4ID = 2;

//    g_auxClocksTimerHandle = Timer_getHandle(DMTimer4ID);
}

/*****************************************************************************/
/** \brief Timer ISR which do nothing.
*/
void TimerEmptyISR()
{
}



/*****************************************************************************/
/** \brief Prototype of main task defined in another file.
*
*/
extern int EcMasterDemo(void);

/**
 *  \name TaskFxn
 *  \brief Task which do EIP initialization
 *  \param a0
 *  \param a1
 *  \return none
 *
 */
void TaskFxn(UArg a0, UArg a1)
{
	NIMU_log("\n\rSYS/BIOS EcMaster Sample application\n\r");

    InitAuxClockTimer();

	/* Workarround call to cache function should exist in executable becuase linker does not want to add it */
    Cache_wbInv(&main_task, 1, Cache_Type_ALL, TRUE);
	EcMasterDemo();

}

/** \brief Macro indicating the i2c time out value. */
#define I2C_TIMEOUT_VAL           (100U)

/* Delay function */
static void delay(unsigned int delayValue);


/*
 *  ======== Delay function ========
 */
void delay(unsigned int delayValue)
{
    volatile uint32_t delay1 = delayValue*10000;
    while (delay1--) ;
}

/* I2C Instance Controlling Clock Synthesizer */
#define CLOCK_SYNTHESIZER_I2C_INST_NUM  0

/* Clock Synthesizer Device Address */
#define CLOCK_SYNTHESIZER_I2C_ADDR      0x65

#define CLOCK_SYNTHESIZER_ID_REG        0

/* Crystal load capacitor selection */
#define CLOCK_SYNTHESIZER_XCSEL         0x05

/* PLL1 Configuration Register */
#define CLOCK_SYNTHESIZER_MUX_REG       0x14

/* PDIV2 */
#define CLOCK_SYNTHESIZER_PDIV2_REG     0x16

/* PDIV3 */
#define CLOCK_SYNTHESIZER_PDIV3_REG     0x17

#define TX_LENGTH              (2U)
#define RX_LENGTH              (10U)

uint32_t ClockSynthesizerSetup(void)
{
    uint32_t status = TRUE;
	uint32_t regValue = 0U;
    I2C_Params i2cParams;
    I2C_Handle handle = NULL;
    I2C_Transaction i2cTransaction;
    char txBuf[TX_LENGTH] = {0x00, 0x01};
    char rxBuf[RX_LENGTH] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                             0x00, 0x00};

    I2C_init();

    I2C_Params_init(&i2cParams);

    ((I2C_HwAttrs *) I2C_config[CLOCK_SYNTHESIZER_I2C_INST_NUM].hwAttrs)->enableIntr = false;
    handle = I2C_open(CLOCK_SYNTHESIZER_I2C_INST_NUM, &i2cParams);

/* Read - CLOCK_SYNTHESIZER_ID_REG */

    if (FALSE != status)
	{
	    txBuf[0] = CLOCK_SYNTHESIZER_ID_REG | 0x80U;
	    i2cTransaction.slaveAddress = CLOCK_SYNTHESIZER_I2C_ADDR;
	    i2cTransaction.writeBuf = (uint8_t *)&txBuf[0];
	    i2cTransaction.writeCount = 1U;
	    i2cTransaction.readBuf = (uint8_t *)&rxBuf[0];
	    i2cTransaction.readCount = 0U;
	    status = I2C_transfer(handle, &i2cTransaction);

	    if(FALSE == status)
	    {
	        NIMU_log("\n CLOCK_SYNTHESIZER_ID_REG: Data Write failed. \n");
	    }
	}

    delay(I2C_TIMEOUT_VAL);

    if (FALSE != status)
	{
	    i2cTransaction.slaveAddress = CLOCK_SYNTHESIZER_I2C_ADDR;
	    i2cTransaction.writeBuf = (uint8_t *)&txBuf[0];
	    i2cTransaction.writeCount = 0U;
	    i2cTransaction.readBuf = (uint8_t *)&rxBuf[0];
	    i2cTransaction.readCount = 1U;
	    status = I2C_transfer(handle, &i2cTransaction);

	    if(FALSE == status)
	    {
	        NIMU_log("\n CLOCK_SYNTHESIZER_ID_REG: Data Read failed. \n");
	    }
		else
		{
			regValue = rxBuf[0];
		}
	}

    delay(I2C_TIMEOUT_VAL);

    if((regValue & 0x81U)!= 0x81U)
    {
        NIMU_log("\n Clock synthesizer: Read: Failed");
        status = FALSE;
    }

    /** CDCE913 Clock Synthesizer configuration for RMII Clock = 50 MHz
     * fout = fin/Pdiv x N/M
     * fout = 50 MHz
     * fin  = 25 MHz
     * Pdiv = 2
     * N    = 4
     * M    = 1
     */

    /* Crystal load Capacitor Selection - 18pF: 0x12h(bits 7:3) */

/* Write - CLOCK_SYNTHESIZER_XCSEL */

    if (FALSE != status)
	{
	    txBuf[0] = CLOCK_SYNTHESIZER_XCSEL | 0x80U;
	    txBuf[1] = 0x90U;
	    i2cTransaction.slaveAddress = CLOCK_SYNTHESIZER_I2C_ADDR;
	    i2cTransaction.writeBuf = (uint8_t *)&txBuf[0];
	    i2cTransaction.writeCount = 2U;
	    i2cTransaction.readBuf = (uint8_t *)&rxBuf[0];
	    i2cTransaction.readCount = 0U;
	    status = I2C_transfer(handle, &i2cTransaction);

	    if(FALSE == status)
	    {
	        NIMU_log("\n CLOCK_SYNTHESIZER_XCSEL: Data Write failed. \n");
	    }
	}

    delay(I2C_TIMEOUT_VAL);

    if (FALSE == status)
    {
        NIMU_log("\n Clock synthesizer: Write: Failed");
    }

    /* PLL1 Multiplexer b7:0 (PLL1) */

/* Write - CLOCK_SYNTHESIZER_MUX_REG */

    if (FALSE != status)
	{
	    txBuf[0] = CLOCK_SYNTHESIZER_MUX_REG | 0x80U;
	    txBuf[1] = 0x6DU;
	    i2cTransaction.slaveAddress = CLOCK_SYNTHESIZER_I2C_ADDR;
	    i2cTransaction.writeBuf = (uint8_t *)&txBuf[0];
	    i2cTransaction.writeCount = 2U;
	    i2cTransaction.readBuf = (uint8_t *)&rxBuf[0];
	    i2cTransaction.readCount = 0U;
	    status = I2C_transfer(handle, &i2cTransaction);

	    if(FALSE == status)
	    {
	        NIMU_log("\n CLOCK_SYNTHESIZER_MUX_REG: Data Write failed. \n");
	    }
	}

    delay(I2C_TIMEOUT_VAL);

    if (FALSE == status)
    {
        NIMU_log("\n Clock synthesizer: Write: Failed");
    }

    /** b7-0(PLL1 SSC down selection by default),
     *  b6:0-0x02h(7-bit Y2-Output-Divider Pdiv2)
     */

/* Write - CLOCK_SYNTHESIZER_PDIV2_REG */

    if (FALSE != status)
	{
	    txBuf[0] = CLOCK_SYNTHESIZER_PDIV2_REG | 0x80U;
	    txBuf[1] = 0x02U;
	    i2cTransaction.slaveAddress = CLOCK_SYNTHESIZER_I2C_ADDR;
	    i2cTransaction.writeBuf = (uint8_t *)&txBuf[0];
	    i2cTransaction.writeCount = 2U;
	    i2cTransaction.readBuf = (uint8_t *)&rxBuf[0];
	    i2cTransaction.readCount = 0U;
	    status = I2C_transfer(handle, &i2cTransaction);

	    if(FALSE == status)
	    {
	        NIMU_log("\n CLOCK_SYNTHESIZER_PDIV2_REG: Data Write failed. \n");
	    }
	}

    delay(I2C_TIMEOUT_VAL);

    if (FALSE == status)
    {
        NIMU_log("\n Clock synthesizer: Write: Failed");
    }

    /* b6:0-0x02h(7-bit Y3-Output-Divider Pdiv3) */

/* Write - CLOCK_SYNTHESIZER_PDIV3_REG */

    if (FALSE != status)
	{
	    txBuf[0] = CLOCK_SYNTHESIZER_PDIV3_REG | 0x80U;
	    txBuf[1] = 0x02U;
	    i2cTransaction.slaveAddress = CLOCK_SYNTHESIZER_I2C_ADDR;
	    i2cTransaction.writeBuf = (uint8_t *)&txBuf[0];
	    i2cTransaction.writeCount = 2U;
	    i2cTransaction.readBuf = (uint8_t *)&rxBuf[0];
	    i2cTransaction.readCount = 0U;
	    status = I2C_transfer(handle, &i2cTransaction);

	    if(FALSE == status)
	    {
	        NIMU_log("\n CLOCK_SYNTHESIZER_PDIV3_REG: Data Write failed. \n");
	    }
	}

    delay(I2C_TIMEOUT_VAL);

    if (FALSE == status)
    {
        NIMU_log("\n Clock synthesizer: Write: Failed");
    }

    I2C_close(handle);

    return status;
}
